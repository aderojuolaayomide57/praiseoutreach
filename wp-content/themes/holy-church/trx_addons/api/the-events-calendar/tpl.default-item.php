<?php
/**
 * The style "default" of the Events
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.2
 */

$args = get_query_var('trx_addons_args_sc_events');
if ($args['slider']) {
	?><div class="swiper-slide"><?php
} else if ($args['columns'] > 1) {
	?><div class="<?php echo esc_attr(trx_addons_get_column_class(1, $args['columns'])); ?>"><?php
}

?><div class="sc_events_item"><?php
    // Featured image
    if ( has_post_thumbnail() ) {
        set_query_var('trx_addons_args_featured', apply_filters('trx_addons_filter_args_featured', array(
            'class' => 'tribe_events_item_thumb',
            'hover' => 'zoomin',
            'thumb_size' => apply_filters('trx_addons_filter_thumb_size', trx_addons_get_thumb_size($args['columns'] > 2 ? 'medium' : 'big'), 'tribe-events-default')
        ), 'tribe-events-default'));
        if (($fdir = trx_addons_get_file_dir('templates/tpl.featured.php')) != '') { include $fdir; }
    }

    // Event's date
	$date = tribe_get_start_date(null, true, 'D, M d');
	if (empty($date)) $date = get_the_date('D, M d');
	?><div class="sc_events_item_date"><?php echo esc_html($date); ?></div><?php

    // Event's title
	?><div class="sc_events_item_title"><?php the_title(); ?></div><?php
?></div><?php

if ($args['slider'] || $args['columns'] > 1) {
	?></div><?php
}

?>