<?php
/**
 * The style "detailed" of the Events item
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.2
 */

$args = get_query_var('trx_addons_args_sc_events');

if ($args['slider']) {
	?><div class="swiper-slide"><?php
} else if ($args['columns'] > 1) {
	?><div class="<?php echo esc_attr(trx_addons_get_column_class(1, $args['columns'])); ?>"><?php
}

?><a href="<?php the_permalink(); ?>" class="sc_events_item"><?php
	// Event's date
	$date = tribe_get_start_date(null, true, 'D, m/j');
	if (empty($date)) $date = get_the_date('D, m/j');
	$time = tribe_get_start_time(null, 'g:i A');
	// Event's date - time
	?><div class="sc_events_item_date_detalied"><?php
        echo esc_html($date) . ' ' . ($time ? esc_html($time) : esc_html__('Whole day', 'holy-church')); ?></div><?php
	// Event's title
	?><div class="sc_events_item_title_detalied"><?php the_title(); ?></div><?php
?></a><?php

if ($args['slider'] || $args['columns'] > 1) {
	?></div><?php
}

?>