<?php
/**
 * The style "default" of the Team
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.2
 */

$args = get_query_var('trx_addons_args_sc_team');

$meta = get_post_meta(get_the_ID(), 'trx_addons_options', true);
$link = get_permalink();
$image ='';
if ($args['slider']) {
	?><div class="swiper-slide"><?php
} else if ($args['columns'] > 1) {
	?><div class="<?php echo esc_attr(trx_addons_get_column_class(1, $args['columns'])); ?>"><?php
}
?>
<div class="sc_team_item">	
	<?php 
		if ( has_post_thumbnail() ) {
			$image = trx_addons_get_attachment_url( get_post_thumbnail_id( get_the_ID() ), trx_addons_get_thumb_size('avatar') );
		} else {
			$image = apply_filters('trx_addons_filter_no_image', trx_addons_get_file_url('css/images/no-image.jpg'));
		}
	?>
	<div class="post_featured with_thumb sc_team_default_item_thumb" <?php if (!empty($image)) echo ' style="background-image: url('.esc_url($image).');"'; ?>>		
	</div>
	<div class="sc_team_item_info">
		<div class="sc_team_item_header">
			<h4 class="sc_team_item_title"><a href="<?php echo esc_url($link); ?>"><?php the_title(); ?></a></h4>
			<div class="sc_team_item_subtitle"><?php echo trim($meta['subtitle']);?></div>
		</div>
		<div class="sc_team_item_content"><?php the_excerpt(); ?></div>
		<?php
		if (!empty($meta['socials'])) {
			?><div class="sc_team_item_socials"><?php echo trim(trx_addons_get_socials_links_custom($meta['socials'])); ?></div><?php
		}
		?>
	</div>
</div>
<?php
if ($args['slider'] || $args['columns'] > 1) {
	?></div><?php
}
?>