<?php
/**
 * Theme storage manipulations
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('holy_church_storage_get')) {
	function holy_church_storage_get($var_name, $default='') {
		global $HOLY_CHURCH_STORAGE;
		return isset($HOLY_CHURCH_STORAGE[$var_name]) ? $HOLY_CHURCH_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('holy_church_storage_set')) {
	function holy_church_storage_set($var_name, $value) {
		global $HOLY_CHURCH_STORAGE;
		$HOLY_CHURCH_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('holy_church_storage_empty')) {
	function holy_church_storage_empty($var_name, $key='', $key2='') {
		global $HOLY_CHURCH_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($HOLY_CHURCH_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($HOLY_CHURCH_STORAGE[$var_name][$key]);
		else
			return empty($HOLY_CHURCH_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('holy_church_storage_isset')) {
	function holy_church_storage_isset($var_name, $key='', $key2='') {
		global $HOLY_CHURCH_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($HOLY_CHURCH_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($HOLY_CHURCH_STORAGE[$var_name][$key]);
		else
			return isset($HOLY_CHURCH_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('holy_church_storage_inc')) {
	function holy_church_storage_inc($var_name, $value=1) {
		global $HOLY_CHURCH_STORAGE;
		if (empty($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = 0;
		$HOLY_CHURCH_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('holy_church_storage_concat')) {
	function holy_church_storage_concat($var_name, $value) {
		global $HOLY_CHURCH_STORAGE;
		if (empty($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = '';
		$HOLY_CHURCH_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('holy_church_storage_get_array')) {
	function holy_church_storage_get_array($var_name, $key, $key2='', $default='') {
		global $HOLY_CHURCH_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($HOLY_CHURCH_STORAGE[$var_name][$key]) ? $HOLY_CHURCH_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($HOLY_CHURCH_STORAGE[$var_name][$key][$key2]) ? $HOLY_CHURCH_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('holy_church_storage_set_array')) {
	function holy_church_storage_set_array($var_name, $key, $value) {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if ($key==='')
			$HOLY_CHURCH_STORAGE[$var_name][] = $value;
		else
			$HOLY_CHURCH_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('holy_church_storage_set_array2')) {
	function holy_church_storage_set_array2($var_name, $key, $key2, $value) {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if (!isset($HOLY_CHURCH_STORAGE[$var_name][$key])) $HOLY_CHURCH_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$HOLY_CHURCH_STORAGE[$var_name][$key][] = $value;
		else
			$HOLY_CHURCH_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Merge array elements
if (!function_exists('holy_church_storage_merge_array')) {
	function holy_church_storage_merge_array($var_name, $key, $value) {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if ($key==='')
			$HOLY_CHURCH_STORAGE[$var_name] = array_merge($HOLY_CHURCH_STORAGE[$var_name], $value);
		else
			$HOLY_CHURCH_STORAGE[$var_name][$key] = array_merge($HOLY_CHURCH_STORAGE[$var_name][$key], $value);
	}
}

// Add array element after the key
if (!function_exists('holy_church_storage_set_array_after')) {
	function holy_church_storage_set_array_after($var_name, $after, $key, $value='') {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if (is_array($key))
			holy_church_array_insert_after($HOLY_CHURCH_STORAGE[$var_name], $after, $key);
		else
			holy_church_array_insert_after($HOLY_CHURCH_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('holy_church_storage_set_array_before')) {
	function holy_church_storage_set_array_before($var_name, $before, $key, $value='') {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if (is_array($key))
			holy_church_array_insert_before($HOLY_CHURCH_STORAGE[$var_name], $before, $key);
		else
			holy_church_array_insert_before($HOLY_CHURCH_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('holy_church_storage_push_array')) {
	function holy_church_storage_push_array($var_name, $key, $value) {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($HOLY_CHURCH_STORAGE[$var_name], $value);
		else {
			if (!isset($HOLY_CHURCH_STORAGE[$var_name][$key])) $HOLY_CHURCH_STORAGE[$var_name][$key] = array();
			array_push($HOLY_CHURCH_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('holy_church_storage_pop_array')) {
	function holy_church_storage_pop_array($var_name, $key='', $defa='') {
		global $HOLY_CHURCH_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($HOLY_CHURCH_STORAGE[$var_name]) && is_array($HOLY_CHURCH_STORAGE[$var_name]) && count($HOLY_CHURCH_STORAGE[$var_name]) > 0) 
				$rez = array_pop($HOLY_CHURCH_STORAGE[$var_name]);
		} else {
			if (isset($HOLY_CHURCH_STORAGE[$var_name][$key]) && is_array($HOLY_CHURCH_STORAGE[$var_name][$key]) && count($HOLY_CHURCH_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($HOLY_CHURCH_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('holy_church_storage_inc_array')) {
	function holy_church_storage_inc_array($var_name, $key, $value=1) {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if (empty($HOLY_CHURCH_STORAGE[$var_name][$key])) $HOLY_CHURCH_STORAGE[$var_name][$key] = 0;
		$HOLY_CHURCH_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('holy_church_storage_concat_array')) {
	function holy_church_storage_concat_array($var_name, $key, $value) {
		global $HOLY_CHURCH_STORAGE;
		if (!isset($HOLY_CHURCH_STORAGE[$var_name])) $HOLY_CHURCH_STORAGE[$var_name] = array();
		if (empty($HOLY_CHURCH_STORAGE[$var_name][$key])) $HOLY_CHURCH_STORAGE[$var_name][$key] = '';
		$HOLY_CHURCH_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('holy_church_storage_call_obj_method')) {
	function holy_church_storage_call_obj_method($var_name, $method, $param=null) {
		global $HOLY_CHURCH_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($HOLY_CHURCH_STORAGE[$var_name]) ? $HOLY_CHURCH_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($HOLY_CHURCH_STORAGE[$var_name]) ? $HOLY_CHURCH_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('holy_church_storage_get_obj_property')) {
	function holy_church_storage_get_obj_property($var_name, $prop, $default='') {
		global $HOLY_CHURCH_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($HOLY_CHURCH_STORAGE[$var_name]->$prop) ? $HOLY_CHURCH_STORAGE[$var_name]->$prop : $default;
	}
}
?>