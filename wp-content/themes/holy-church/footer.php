<?php
/**
 * The Footer: widgets area, logo, footer menu and socials
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

						// Widgets area inside page content
						holy_church_create_widgets_area('widgets_below_content');
						?>				
					</div><!-- </.content> -->

					<?php
					// Show main sidebar
					get_sidebar();

					// Widgets area below page content
					holy_church_create_widgets_area('widgets_below_page');

					$holy_church_body_style = holy_church_get_theme_option('body_style');
					if ($holy_church_body_style != 'fullscreen') {
						?></div><!-- </.content_wrap> --><?php
					}
					?>
			</div><!-- </.page_content_wrap> -->

			<?php
			// Footer
			$holy_church_footer_style = holy_church_get_theme_option("footer_style");
			if (strpos($holy_church_footer_style, 'footer-custom-')===0) $holy_church_footer_style = 'footer-custom';
			get_template_part( "templates/{$holy_church_footer_style}");
			?>

		</div><!-- /.page_wrap -->

	</div><!-- /.body_wrap -->

	<?php if (holy_church_is_on(holy_church_get_theme_option('debug_mode')) && holy_church_get_file_dir('images/makeup.jpg')!='') { ?>
		<img src="<?php echo esc_url(holy_church_get_file_url('images/makeup.jpg')); ?>" id="makeup">
	<?php } ?>

	<?php wp_footer(); ?>

</body>
</html>