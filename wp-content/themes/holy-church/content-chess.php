<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_blog_style = explode('_', holy_church_get_theme_option('blog_style'));
$holy_church_columns = empty($holy_church_blog_style[1]) ? 1 : max(1, $holy_church_blog_style[1]);
$holy_church_expanded = !holy_church_sidebar_present() && holy_church_is_on(holy_church_get_theme_option('expand_content'));
$holy_church_post_format = get_post_format();
$holy_church_post_format = empty($holy_church_post_format) ? 'standard' : str_replace('post-format-', '', $holy_church_post_format);
$holy_church_animation = holy_church_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_chess post_layout_chess_'.esc_attr($holy_church_columns).' post_format_'.esc_attr($holy_church_post_format) ); ?>
	<?php echo (!holy_church_is_off($holy_church_animation) ? ' data-animation="'.esc_attr(holy_church_get_animation_classes($holy_church_animation)).'"' : ''); ?>
	>

	<?php
	// Add anchor
	if ($holy_church_columns == 1 && shortcode_exists('trx_sc_anchor')) {
		echo do_shortcode('[trx_sc_anchor id="post_'.esc_attr(get_the_ID()).'" title="'.the_title_attribute( array( 'echo' => false ) ).'"]');
	}

	// Featured image
	holy_church_show_post_featured( array(
											'class' => $holy_church_columns == 1 ? 'trx-stretch-height' : '',
											'show_no_image' => true,
											'thumb_bg' => true,
											'thumb_size' => holy_church_get_thumb_size(
																	strpos(holy_church_get_theme_option('body_style'), 'full')!==false
																		? ( $holy_church_columns > 1 ? 'huge' : 'original' )
																		: (	$holy_church_columns > 2 ? 'big' : 'huge')
																	)
											) 
										);

	?><div class="post_inner"><div class="post_inner_content"><?php 

		?><div class="post_header entry-header"><?php 
			do_action('holy_church_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h3 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
			
			do_action('holy_church_action_before_post_meta'); 

			// Post meta
			$holy_church_post_meta = holy_church_show_post_meta(array(
									'categories' => true,
									'date' => true,
									'edit' => $holy_church_columns == 1,
									'seo' => false,
									'share' => false,
									'counters' => $holy_church_columns < 3 ? 'comments' : '',
									'echo' => false
									)
								);
			holy_church_show_layout($holy_church_post_meta);
		?></div><!-- .entry-header -->
	
		<div class="post_content entry-content">
			<div class="post_content_inner">
				<?php
				$holy_church_show_learn_more = !in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'));
				if (has_excerpt()) {
					the_excerpt();
				} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
					the_content( '' );
				} else if (in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'))) {
					the_content();
				} else if (substr(get_the_content(), 0, 1)!='[') {
					the_excerpt();
				}
				?>
			</div>
			<?php
			// Post meta
			if (in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'))) {
				holy_church_show_layout($holy_church_post_meta);
			}
			// More button
			if ( $holy_church_show_learn_more ) {
				?><p><a class="more-link" href="<?php the_permalink(); ?>"><?php esc_html_e('Read more', 'holy-church'); ?></a></p><?php
			}
			?>
		</div><!-- .entry-content -->

	</div></div><!-- .post_inner -->

</article>