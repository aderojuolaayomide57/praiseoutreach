<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_sidebar_position = holy_church_get_theme_option('sidebar_position');
if (holy_church_sidebar_present()) {
	ob_start();
	$holy_church_sidebar_name = holy_church_get_theme_option('sidebar_widgets');
	holy_church_storage_set('current_sidebar', 'sidebar');
	if ( is_active_sidebar($holy_church_sidebar_name) ) {
		dynamic_sidebar($holy_church_sidebar_name);
	}
	$holy_church_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($holy_church_out)) {
		?>
		<div class="sidebar <?php echo esc_attr($holy_church_sidebar_position); ?> widget_area<?php if (!holy_church_is_inherit(holy_church_get_theme_option('sidebar_scheme'))) echo ' scheme_'.esc_attr(holy_church_get_theme_option('sidebar_scheme')); ?>" role="complementary">
			<div class="sidebar_inner">
				<?php
				do_action( 'holy_church_action_before_sidebar' );
				holy_church_show_layout(preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $holy_church_out));
				do_action( 'holy_church_action_after_sidebar' );
				?>
			</div><!-- /.sidebar_inner -->
		</div><!-- /.sidebar -->
		<?php
	}
}
?>