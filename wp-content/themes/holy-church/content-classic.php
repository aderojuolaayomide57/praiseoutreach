<?php
/**
 * The Classic template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_blog_style = explode('_', holy_church_get_theme_option('blog_style'));
$holy_church_columns = empty($holy_church_blog_style[1]) ? 2 : max(2, $holy_church_blog_style[1]);
$holy_church_expanded = !holy_church_sidebar_present() && holy_church_is_on(holy_church_get_theme_option('expand_content'));
$holy_church_post_format = get_post_format();
$holy_church_post_format = empty($holy_church_post_format) ? 'standard' : str_replace('post-format-', '', $holy_church_post_format);
$holy_church_animation = holy_church_get_theme_option('blog_animation');

?><div class="<?php echo esc_attr($holy_church_blog_style[0] == 'classic' ? 'column' : 'masonry_item masonry_item'); ?>-1_<?php echo esc_attr($holy_church_columns); ?>"><article id="post-<?php the_ID(); ?>"
	<?php post_class( 'post_item post_format_'.esc_attr($holy_church_post_format)
					. ' post_layout_classic post_layout_classic_'.esc_attr($holy_church_columns)
					. ' post_layout_'.esc_attr($holy_church_blog_style[0]) 
					. ' post_layout_'.esc_attr($holy_church_blog_style[0]).'_'.esc_attr($holy_church_columns)
					); ?>
	<?php echo (!holy_church_is_off($holy_church_animation) ? ' data-animation="'.esc_attr(holy_church_get_animation_classes($holy_church_animation)).'"' : ''); ?>
	>

	<?php

	// Featured image
	holy_church_show_post_featured( array( 'thumb_size' => holy_church_get_thumb_size($holy_church_blog_style[0] == 'classic'
													? (strpos(holy_church_get_theme_option('body_style'), 'full')!==false 
															? ( $holy_church_columns > 2 ? 'big' : 'huge' )
															: (	$holy_church_columns > 2
																? ($holy_church_expanded ? 'med' : 'small')
																: ($holy_church_expanded ? 'big' : 'med')
																)
														)
													: (strpos(holy_church_get_theme_option('body_style'), 'full')!==false 
															? ( $holy_church_columns > 2 ? 'masonry-big' : 'full' )
															: (	$holy_church_columns <= 2 && $holy_church_expanded ? 'masonry-big' : 'masonry')
														)
								) ) );

	if ( !in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote')) ) {
		?>
		<div class="post_header entry-header">
			<?php 
			do_action('holy_church_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h4 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' );

			do_action('holy_church_action_before_post_meta'); 

			// Post meta
			holy_church_show_post_meta(array(
				'categories' => true,
				'date' => true,
				'edit' => $holy_church_columns < 3,
				'seo' => false,
				'share' => false,
				'counters' => 'comments',
				)
			);
			?>
		</div><!-- .entry-header -->
		<?php
	}		
	?>

	<div class="post_content entry-content">
		<div class="post_content_inner">
			<?php
			$holy_church_show_learn_more = false;
			if (has_excerpt()) {
				the_excerpt();
			} else if (strpos(get_the_content('!--more'), '!--more')!==false) {
				the_content( '' );
			} else if (in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'))) {
				the_content();
			} else if (substr(get_the_content(), 0, 1)!='[') {
				the_excerpt();
			}
			?>
		</div>
		<?php
		// Post meta
		if (in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'))) {
			holy_church_show_post_meta(array(
				'share' => false,
				'counters' => 'comments'
				)
			);
		}
		// More button
		if ( $holy_church_show_learn_more ) {
			?><p><a class="more-link" href="<?php the_permalink(); ?>"><?php esc_html_e('Read more', 'holy-church'); ?></a></p><?php
		}
		?>
	</div><!-- .entry-content -->

</article></div>