<?php
/* Theme-specific action to configure ThemeREX Addons components
------------------------------------------------------------------------------- */


/* ThemeREX Addons components
------------------------------------------------------------------------------- */

if (!function_exists('holy_church_trx_addons_theme_specific_setup1')) {
	add_action( 'after_setup_theme', 'holy_church_trx_addons_theme_specific_setup1', 1 );
	add_action( 'trx_addons_action_save_options', 'holy_church_trx_addons_theme_specific_setup1', 8 );
	function holy_church_trx_addons_theme_specific_setup1() {
		if (holy_church_exists_trx_addons()) {
			add_filter( 'trx_addons_cv_enable',				'holy_church_trx_addons_cv_enable');
			add_filter( 'trx_addons_cpt_list',				'holy_church_trx_addons_cpt_list');
			add_filter( 'trx_addons_sc_list',				'holy_church_trx_addons_sc_list');
			add_filter( 'trx_addons_widgets_list',			'holy_church_trx_addons_widgets_list');
		}
	}
}

// CV
if ( !function_exists( 'holy_church_trx_addons_cv_enable' ) ) {
	//Handler of the add_filter( 'trx_addons_cv_enable', 'holy_church_trx_addons_cv_enable');
	function holy_church_trx_addons_cv_enable($enable=false) {
		// To do: return false if theme not use CV functionality
		return false;
	}
}

// CPT
if ( !function_exists( 'holy_church_trx_addons_cpt_list' ) ) {
	//Handler of the add_filter('trx_addons_cpt_list',	'holy_church_trx_addons_cpt_list');
	function holy_church_trx_addons_cpt_list($list=array()) {
		// To do: Enable/Disable CPT via add/remove it in the list
        $list_custom = array(
            'layouts'       => true,
            'services'      => true,
            'team'          => true,
            'testimonials'  => true,
        );
        $list = array_intersect_key( $list, $list_custom  );

		return $list;
	}
}

// Shortcodes
if ( !function_exists( 'holy_church_trx_addons_sc_list' ) ) {
	//Handler of the add_filter('trx_addons_sc_list',	'holy_church_trx_addons_sc_list');
	function holy_church_trx_addons_sc_list($list=array()) {
		// To do: Add/Remove shortcodes into list
		// If you add new shortcode - in the theme's folder must exists /trx_addons/shortcodes/new_sc_name/new_sc_name.php
		return $list;
	}
}

// Widgets
if ( !function_exists( 'holy_church_trx_addons_widgets_list' ) ) {
	//Handler of the add_filter('trx_addons_widgets_list',	'holy_church_trx_addons_widgets_list');
	function holy_church_trx_addons_widgets_list($list=array()) {
		// To do: Add/Remove widgets into list
        $list_custom = array(
            'audio'         => true,
            'video'         => true,
            'slider'        => true,
            'socials'       => true,
            'recent_posts'  => true,
            'contacts'      => true,
        );
        $list = array_intersect_key( $list, $list_custom  );

		return $list;
	}
}


/* Add options in the Theme Options Customizer
------------------------------------------------------------------------------- */

// Theme init priorities:
// 3 - add/remove Theme Options elements
if (!function_exists('holy_church_trx_addons_theme_specific_setup3')) {
	add_action( 'after_setup_theme', 'holy_church_trx_addons_theme_specific_setup3', 3 );
	function holy_church_trx_addons_theme_specific_setup3() {
		
		// Section 'Courses' - settings to show 'Courses' blog archive and single posts
		if (holy_church_exists_courses()) {
		
			holy_church_storage_merge_array('options', '', array(
				'courses' => array(
					"title" => esc_html__('Courses', 'holy-church'),
					"desc" => wp_kses_data( __('Select parameters to display the courses pages', 'holy-church') ),
					"type" => "section"
					),
				'expand_content_courses' => array(
					"title" => esc_html__('Expand content', 'holy-church'),
					"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden', 'holy-church') ),
					"refresh" => false,
					"std" => 1,
					"type" => "checkbox"
					),
				'header_style_courses' => array(
					"title" => esc_html__('Header style', 'holy-church'),
					"desc" => wp_kses_data( __('Select style to display the site header on the courses pages', 'holy-church') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_position_courses' => array(
					"title" => esc_html__('Header position', 'holy-church'),
					"desc" => wp_kses_data( __('Select position to display the site header on the courses pages', 'holy-church') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_widgets_courses' => array(
					"title" => esc_html__('Header widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the header on the courses pages', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_widgets_courses' => array(
					"title" => esc_html__('Sidebar widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select sidebar to show on the courses pages', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_position_courses' => array(
					"title" => esc_html__('Sidebar position', 'holy-church'),
					"desc" => wp_kses_data( __('Select position to show sidebar on the courses pages', 'holy-church') ),
					"refresh" => false,
					"std" => 'left',
					"options" => array(),
					"type" => "select"
					),
				'hide_sidebar_on_single_courses' => array(
					"title" => esc_html__('Hide sidebar on the single course', 'holy-church'),
					"desc" => wp_kses_data( __("Hide sidebar on the single course's page", 'holy-church') ),
					"std" => 0,
					"type" => "checkbox"
					),
				'widgets_above_page_courses' => array(
					"title" => esc_html__('Widgets above the page', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_above_content_courses' => array(
					"title" => esc_html__('Widgets above the content', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_content_courses' => array(
					"title" => esc_html__('Widgets below the content', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_page_courses' => array(
					"title" => esc_html__('Widgets below the page', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'footer_scheme_courses' => array(
					"title" => esc_html__('Footer Color Scheme', 'holy-church'),
					"desc" => wp_kses_data( __('Select color scheme to decorate footer area', 'holy-church') ),
					"std" => 'dark',
					"options" => array(),
					"type" => "select"
					),
				'footer_widgets_courses' => array(
					"title" => esc_html__('Footer widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the footer', 'holy-church') ),
					"std" => 'footer_widgets',
					"options" => array(),
					"type" => "select"
					),
				'footer_columns_courses' => array(
					"title" => esc_html__('Footer columns', 'holy-church'),
					"desc" => wp_kses_data( __('Select number columns to show widgets in the footer. If 0 - autodetect by the widgets count', 'holy-church') ),
					"dependency" => array(
						'footer_widgets_courses' => array('^hide')
					),
					"std" => 0,
					"options" => holy_church_get_list_range(0,6),
					"type" => "select"
					),
				'footer_wide_courses' => array(
					"title" => esc_html__('Footer fullwide', 'holy-church'),
					"desc" => wp_kses_data( __('Do you want to stretch the footer to the entire window width?', 'holy-church') ),
					"std" => 0,
					"type" => "checkbox"
					)
				)
			);
		}
		
		// Section 'Sport' - settings to show 'Sport' blog archive and single posts
		if (holy_church_exists_sport()) {
			holy_church_storage_merge_array('options', '', array(
				'sport' => array(
					"title" => esc_html__('Sport', 'holy-church'),
					"desc" => wp_kses_data( __('Select parameters to display the sport pages', 'holy-church') ),
					"type" => "section"
					),
				'expand_content_sport' => array(
					"title" => esc_html__('Expand content', 'holy-church'),
					"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden', 'holy-church') ),
					"refresh" => false,
					"std" => 1,
					"type" => "checkbox"
					),
				'header_style_sport' => array(
					"title" => esc_html__('Header style', 'holy-church'),
					"desc" => wp_kses_data( __('Select style to display the site header on the sport pages', 'holy-church') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_position_sport' => array(
					"title" => esc_html__('Header position', 'holy-church'),
					"desc" => wp_kses_data( __('Select position to display the site header on the sport pages', 'holy-church') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_widgets_sport' => array(
					"title" => esc_html__('Header widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the header on the sport pages', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_widgets_sport' => array(
					"title" => esc_html__('Sidebar widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select sidebar to show on the sport pages', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_position_sport' => array(
					"title" => esc_html__('Sidebar position', 'holy-church'),
					"desc" => wp_kses_data( __('Select position to show sidebar on the sport pages', 'holy-church') ),
					"refresh" => false,
					"std" => 'left',
					"options" => array(),
					"type" => "select"
					),
				'hide_sidebar_on_single_sport' => array(
					"title" => esc_html__('Hide sidebar on the single course', 'holy-church'),
					"desc" => wp_kses_data( __("Hide sidebar on the single course's page", 'holy-church') ),
					"std" => 0,
					"type" => "checkbox"
					),
				'widgets_above_page_sport' => array(
					"title" => esc_html__('Widgets above the page', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_above_content_sport' => array(
					"title" => esc_html__('Widgets above the content', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_content_sport' => array(
					"title" => esc_html__('Widgets below the content', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_page_sport' => array(
					"title" => esc_html__('Widgets below the page', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'footer_scheme_sport' => array(
					"title" => esc_html__('Footer Color Scheme', 'holy-church'),
					"desc" => wp_kses_data( __('Select color scheme to decorate footer area', 'holy-church') ),
					"std" => 'dark',
					"options" => array(),
					"type" => "select"
					),
				'footer_widgets_sport' => array(
					"title" => esc_html__('Footer widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the footer', 'holy-church') ),
					"std" => 'footer_widgets',
					"options" => array(),
					"type" => "select"
					),
				'footer_columns_sport' => array(
					"title" => esc_html__('Footer columns', 'holy-church'),
					"desc" => wp_kses_data( __('Select number columns to show widgets in the footer. If 0 - autodetect by the widgets count', 'holy-church') ),
					"dependency" => array(
						'footer_widgets_sport' => array('^hide')
					),
					"std" => 0,
					"options" => holy_church_get_list_range(0,6),
					"type" => "select"
					),
				'footer_wide_sport' => array(
					"title" => esc_html__('Footer fullwide', 'holy-church'),
					"desc" => wp_kses_data( __('Do you want to stretch the footer to the entire window width?', 'holy-church') ),
					"std" => 0,
					"type" => "checkbox"
					)
				)
			);
		}
	}
}

// Add mobile menu to the plugin's cached menu list
if ( !function_exists( 'holy_church_trx_addons_menu_cache' ) ) {
	add_filter( 'trx_addons_filter_menu_cache', 'holy_church_trx_addons_menu_cache');
	function holy_church_trx_addons_menu_cache($list=array()) {
		if (in_array('#menu_main', $list)) $list[] = '#menu_mobile';
		$list[] = '.menu_mobile_inner > nav > ul';
		return $list;
	}
}

// Add vars into localize array
if (!function_exists('holy_church_trx_addons_localize_script')) {
	add_filter( 'holy_church_filter_localize_script','holy_church_trx_addons_localize_script' );
	function holy_church_trx_addons_localize_script($arr) {
		$arr['alter_link_color'] = holy_church_get_scheme_color('alter_link');
		return $arr;
	}
}


// Add theme-specific layouts to the list
if (!function_exists('holy_church_trx_addons_theme_specific_default_layouts')) {
	add_filter( 'trx_addons_filter_default_layouts',	'holy_church_trx_addons_theme_specific_default_layouts');
	function holy_church_trx_addons_theme_specific_default_layouts($default_layouts=array()) {
		require_once HOLY_CHURCH_THEME_DIR . 'theme-specific/trx_addons.layouts.php';
		return isset($layouts) && is_array($layouts) && count($layouts) > 0
						? array_merge($default_layouts, $layouts)
						: $default_layouts;
	}
}

// Disable override header image on team pages
if ( !function_exists( 'holy_church_trx_addons_allow_override_header_image' ) ) {
	add_filter( 'holy_church_filter_allow_override_header_image', 'holy_church_trx_addons_allow_override_header_image' );
	function holy_church_trx_addons_allow_override_header_image($allow) {
		return holy_church_is_team_page() || holy_church_is_portfolio_page() ? false : $allow;
	}
}

// Hide sidebar on the team pages
if ( !function_exists( 'holy_church_trx_addons_sidebar_present' ) ) {
	add_filter( 'holy_church_filter_sidebar_present', 'holy_church_trx_addons_sidebar_present' );
	function holy_church_trx_addons_sidebar_present($present) {
		return !is_single() && (holy_church_is_team_page() || holy_church_is_portfolio_page()) ? false : $present;
	}
}


// WP Editor addons
//------------------------------------------------------------------------

// Theme-specific configure of the WP Editor
if ( !function_exists( 'holy_church_trx_addons_editor_init' ) ) {
	if (is_admin()) add_filter( 'tiny_mce_before_init', 'holy_church_trx_addons_editor_init', 11);
	function holy_church_trx_addons_editor_init($opt) {
		if (holy_church_exists_trx_addons()) {
			// Add style 'Arrow' to the 'List styles'
			// Remove 'false &&' from condition below to add new style to the list
			if (false && !empty($opt['style_formats'])) {
				$style_formats = json_decode($opt['style_formats'], true);
				if (is_array($style_formats) && count($style_formats)>0 ) {
					foreach ($style_formats as $k=>$v) {
						if ( $v['title'] == esc_html__('List styles', 'holy-church') ) {
							$style_formats[$k]['items'][] = array(
										'title' => esc_html__('Arrow', 'holy-church'),
										'selector' => 'ul',
										'classes' => 'trx_addons_list trx_addons_list_arrow'
									);
						}
					}
					$opt['style_formats'] = json_encode( $style_formats );		
				}
			}
		}
		return $opt;
	}
}


// Theme-specific thumb sizes
//------------------------------------------------------------------------

// Replace thumb sizes to the theme-specific
if ( !function_exists( 'holy_church_trx_addons_add_thumb_sizes' ) ) {
	add_filter( 'trx_addons_filter_add_thumb_sizes', 'holy_church_trx_addons_add_thumb_sizes');
	function holy_church_trx_addons_add_thumb_sizes($list=array()) {
		if (is_array($list)) {
			foreach ($list as $k=>$v) {
				if (in_array($k, array(
								'trx_addons-thumb-huge',
								'trx_addons-thumb-big',
								'trx_addons-thumb-medium',
								'trx_addons-thumb-tiny',
								'trx_addons-thumb-masonry-big',
								'trx_addons-thumb-masonry',
								)
							)
						) unset($list[$k]);
			}
		}
		return $list;
	}
}

// Return theme-specific thumb size instead removed plugin's thumb size
if ( !function_exists( 'holy_church_trx_addons_get_thumb_size' ) ) {
	add_filter( 'trx_addons_filter_get_thumb_size', 'holy_church_trx_addons_get_thumb_size');
	function holy_church_trx_addons_get_thumb_size($thumb_size='') {
		return str_replace(array(
							'trx_addons-thumb-huge',
							'trx_addons-thumb-huge-@retina',
							'trx_addons-thumb-big',
							'trx_addons-thumb-big-@retina',
							'trx_addons-thumb-medium',
							'trx_addons-thumb-medium-@retina',
							'trx_addons-thumb-tiny',
							'trx_addons-thumb-tiny-@retina',
							'trx_addons-thumb-masonry-big',
							'trx_addons-thumb-masonry-big-@retina',
							'trx_addons-thumb-masonry',
							'trx_addons-thumb-masonry-@retina',
							'trx_addons-thumb-avatar',
							'trx_addons-thumb-avatar-@retina',
							),
							array(
							'holy_church-thumb-huge',
							'holy_church-thumb-huge-@retina',
							'holy_church-thumb-big',
							'holy_church-thumb-big-@retina',
							'holy_church-thumb-med',
							'holy_church-thumb-med-@retina',
							'holy_church-thumb-tiny',
							'holy_church-thumb-tiny-@retina',
							'holy_church-thumb-masonry-big',
							'holy_church-thumb-masonry-big-@retina',
							'holy_church-thumb-masonry',
							'holy_church-thumb-masonry-@retina',
                            'holy_church-thumb-avatar',
                            'holy_church-thumb-avatar-@retina',
							),
							$thumb_size);
	}
}

// Get thumb size for the team items
if ( !function_exists( 'holy_church_trx_addons_thumb_size' ) ) {
	add_filter( 'trx_addons_filter_thumb_size',	'holy_church_trx_addons_thumb_size', 10, 2);
	function holy_church_trx_addons_thumb_size($thumb_size='', $type='') {
		if ($type == 'team-default')
			$thumb_size = holy_church_get_thumb_size('med');
		return $thumb_size;
	}
}



// Shortcodes support
//------------------------------------------------------------------------

// Return tag for the item's title
if ( !function_exists( 'holy_church_trx_addons_sc_item_title_tag' ) ) {
	add_filter( 'trx_addons_filter_sc_item_title_tag', 'holy_church_trx_addons_sc_item_title_tag');
	function holy_church_trx_addons_sc_item_title_tag($tag='') {
		return $tag=='h1' ? 'h2' : $tag;
	}
}

// Return args for the item's button
if ( !function_exists( 'holy_church_trx_addons_sc_item_button_args' ) ) {
	add_filter( 'trx_addons_filter_sc_item_button_args', 'holy_church_trx_addons_sc_item_button_args');
	function holy_church_trx_addons_sc_item_button_args($args, $sc='') {
		if (false && $sc != 'sc_button') {
			$args['type'] = 'simple';
			$args['icon_type'] = 'fontawesome';
			$args['icon_fontawesome'] = 'icon-down-big';
			$args['icon_position'] = 'top';
		}
		return $args;
	}
}

// Add new types in the shortcodes
if ( !function_exists( 'holy_church_trx_addons_sc_type' ) ) {
	add_filter( 'trx_addons_sc_type', 'holy_church_trx_addons_sc_type', 10, 2);
	function holy_church_trx_addons_sc_type($list, $sc) {
		if (in_array($sc, array('trx_sc_events')))
			$list[esc_html__('Detailed 2', 'holy-church')] = 'detailed_2';
		return $list;
	}
}

// Add new styles to the Google map
if ( !function_exists( 'holy_church_trx_addons_sc_googlemap_styles' ) ) {
	add_filter( 'trx_addons_filter_sc_googlemap_styles',	'holy_church_trx_addons_sc_googlemap_styles');
	function holy_church_trx_addons_sc_googlemap_styles($list) {
		$list[esc_html__('Dark', 'holy-church')] = 'dark';
		$list[esc_html__('Gray', 'holy-church')] = 'gray';
		return $list;
	}
}
