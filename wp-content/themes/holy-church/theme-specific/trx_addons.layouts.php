<?php
//Custom Layouts
$layouts = array(
		'header_26' => array(
				'name' => 'Header Alternative',
				'template' => '<p>[vc_row content_placement=\"middle\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\"][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\"][vc_row_inner row_delimiter=\"\"][vc_column_inner width=\"1/3\" column_align=\"left\" icons_position=\"left\"][vc_column_text css=\".vc_custom_1489076055288{margin-top: 20px !important;}\" el_class=\"sc_layouts_hide_on_mobile sc_layouts_hide_on_tablet\"]<strong>Address:</strong> 123, New LenoxChicago, IL 60606[/vc_column_text][/vc_column_inner][vc_column_inner width=\"1/3\" column_align=\"center\" icons_position=\"left\"][trx_sc_layouts_logo logo=\"423\" logo_retina=\"415\"][/vc_column_inner][vc_column_inner width=\"1/3\" column_align=\"right\" icons_position=\"left\" css=\".vc_custom_1489073533474{margin-top: 13px !important;}\"][trx_sc_layouts_iconed_text icon_type=\"fontawesome\" hide_on_tablet=\"1\" hide_on_mobile=\"1\" text2=\"123-456-7890\" link=\"tel:+1234567890\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row][vc_row row_type=\"compact\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1489072300938{background-color: #dba860 !important;}\"][vc_column icons_position=\"left\"][trx_sc_content size=\"1_1\" float=\"center\" align=\"center\" number_position=\"br\" title_style=\"default\"][vc_row_inner row_delimiter=\"\"][vc_column_inner icons_position=\"left\"][trx_sc_layouts_menu location=\"menu_main\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" inverse_colors=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row]</p>
',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1489072300938{background-color: #dba860 !important;}.vc_custom_1489073533474{margin-top: 13px !important;}.vc_custom_1489076055288{margin-top: 20px !important;}'
						)
				),
		'header_25' => array(
				'name' => 'Header Default 2',
				'template' => '[vc_row scheme=\"default\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1489064973645{padding-top: 0.8em !important;padding-bottom: 1.35em !important;background-color: rgba(37,37,37,0.56) !important;*background-color: rgb(37,37,37) !important;}\"][vc_column column_align=\"left\" icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/3\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_logo logo=\"614\" logo_retina=\"414\"][/vc_column_inner][vc_column_inner width=\"2/3\" column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1489064973645{padding-top: 0.8em !important;padding-bottom: 1.35em !important;background-color: rgba(37,37,37,0.56) !important;*background-color: rgb(37,37,37) !important;}'
						)
				),
		'header_1284' => array(
				'name' => 'Header Default 3',
				'template' => '<p>[vc_row scheme=\"default\" row_type=\"normal\" row_delimiter=\"\" row_fixed=\"1\" hide_on_tablet=\"\" hide_on_mobile=\"\" hide_on_frontpage=\"\" css=\".vc_custom_1594048470508{padding-top: 0.8em !important;padding-bottom: 1.35em !important;}\"][vc_column column_align=\"left\" icons_position=\"left\" column_type=\"center\"][trx_sc_content size=\"1_1\" number_position=\"br\" title_style=\"default\" width=\"1_1\" width2=\"1_1\"][vc_row_inner equal_height=\"yes\" content_placement=\"middle\"][vc_column_inner width=\"1/3\" column_align=\"left\" icons_position=\"left\"][trx_sc_layouts_logo][/vc_column_inner][vc_column_inner width=\"2/3\" column_align=\"right\" icons_position=\"left\"][trx_sc_layouts_menu location=\"none\" menu=\"main-menu\" animation_in=\"fadeInUpSmall\" animation_out=\"fadeOutDownSmall\" mobile_button=\"1\" mobile_menu=\"1\" hide_on_mobile=\"1\" burger=\"\" mobile=\"1\" stretch=\"\" mobile_hide=\"1\"][/vc_column_inner][/vc_row_inner][/trx_sc_content][/vc_column][/vc_row]</p>
',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'header'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1594048470508{padding-top: 0.8em !important;padding-bottom: 1.35em !important;}'
						)
				),
		'custom_437' => array(
				'name' => 'Side Button Left',
				'template' => '[vc_row][vc_column icons_position=\"left\"][trx_sc_title title_style=\"default\" title=\"Contact info\"][vc_column_text]<span class=\"trx_addons_big_font\"><a href=\"tel:123-456-7890\">123-456-7890</a></span>
123, New Lenox, Chicago IL 60606
<a href=\"mailto:info@yoursitename.com\">info@yoursitename.com</a>[/vc_column_text][trx_sc_title title_style=\"default\" title=\"Gathering Times\" css=\".vc_custom_1488814111799{margin-top: 5rem !important;}\"][vc_column_text]<span class=\"trx_addons_big_font\">Saturday:</span>
5:30pm

<span class=\"trx_addons_big_font\">Sunday:</span>
8:00am, 9:15am, 11:00am[/vc_column_text][trx_widget_socials css=\".vc_custom_1488814116133{margin-top: 5rem !important;}\"][/vc_column][/vc_row]',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								),
						'_wpb_shortcodes_custom_css' => '.vc_custom_1488814111799{margin-top: 5rem !important;}.vc_custom_1488814116133{margin-top: 5rem !important;}'
						)
				),
		'custom_436' => array(
				'name' => 'Side Button Right',
				'template' => '<p>[vc_row][vc_column icons_position=\"left\"][trx_sc_events type=\"detailed\" past=\"\" cat=\"0\" orderby=\"none\" title_style=\"default\" count=\"5\" columns=\"1\"][/vc_column][/vc_row]</p>
',
				'meta' => array(
						'trx_addons_options' => array(
								'layout_type' => 'custom'
								)
						)
				)
		);
?>