<?php
/* Elegro Crypto Payment support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if ( ! function_exists( 'holy_church_elegro_payment_theme_setup9' ) ) {
	add_action( 'after_setup_theme', 'holy_church_elegro_payment_theme_setup9', 9 );
	function holy_church_elegro_payment_theme_setup9() {

		if ( is_admin() ) {
			add_filter( 'holy_church_filter_tgmpa_required_plugins', 'holy_church_elegro_payment_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'holy_church_elegro_payment_tgmpa_required_plugins' ) ) {
	function holy_church_elegro_payment_tgmpa_required_plugins($list=array()) {
		if (in_array('contact-form-7', holy_church_storage_get('required_plugins'))) {
			// elegro Crypto Payment plugin
			$list[] = array(
				'name' 		=> esc_html__('elegro Crypto Payment', 'holy-church'),
				'slug' 		=> 'elegro-payment',
				'required' 	=> false
			);
		}
		return $list;
	}
}

// Check if this plugin installed and activated
if ( ! function_exists( 'holy_church_exists_elegro_payment' ) ) {
	function holy_church_exists_elegro_payment() {
		return class_exists( 'WC_Elegro_Payment' );
	}
}

