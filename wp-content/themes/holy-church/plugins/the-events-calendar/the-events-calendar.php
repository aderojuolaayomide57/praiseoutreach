<?php
/* Tribe Events Calendar support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 1 - register filters, that add/remove lists items for the Theme Options
if (!function_exists('holy_church_tribe_events_theme_setup1')) {
	add_action( 'after_setup_theme', 'holy_church_tribe_events_theme_setup1', 1 );
	function holy_church_tribe_events_theme_setup1() {
		add_filter( 'holy_church_filter_list_sidebars', 'holy_church_tribe_events_list_sidebars' );
	}
}

// Theme init priorities:
// 3 - add/remove Theme Options elements
if (!function_exists('holy_church_tribe_events_theme_setup3')) {
	add_action( 'after_setup_theme', 'holy_church_tribe_events_theme_setup3', 3 );
	function holy_church_tribe_events_theme_setup3() {
		if (holy_church_exists_tribe_events()) {
		
			holy_church_storage_merge_array('options', '', array(
				// Section 'Tribe Events' - settings for show pages
				'events' => array(
					"title" => esc_html__('Events', 'holy-church'),
					"desc" => wp_kses_data( __('Select parameters to display the events pages', 'holy-church') ),
					"type" => "section"
					),
				'expand_content_events' => array(
					"title" => esc_html__('Expand content', 'holy-church'),
					"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden', 'holy-church') ),
					"refresh" => false,
					"std" => 1,
					"type" => "checkbox"
					),
				'header_style_events' => array(
					"title" => esc_html__('Header style', 'holy-church'),
					"desc" => wp_kses_data( __('Select style to display the site header on the events pages', 'holy-church') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_position_events' => array(
					"title" => esc_html__('Header position', 'holy-church'),
					"desc" => wp_kses_data( __('Select position to display the site header on the events pages', 'holy-church') ),
					"std" => 'inherit',
					"options" => array(),
					"type" => "select"
					),
				'header_widgets_events' => array(
					"title" => esc_html__('Header widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the header on the events pages', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_widgets_events' => array(
					"title" => esc_html__('Sidebar widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select sidebar to show on the events pages', 'holy-church') ),
					"std" => 'tribe_events_widgets',
					"options" => array(),
					"type" => "select"
					),
				'sidebar_position_events' => array(
					"title" => esc_html__('Sidebar position', 'holy-church'),
					"desc" => wp_kses_data( __('Select position to show sidebar on the events pages', 'holy-church') ),
					"refresh" => false,
					"std" => 'left',
					"options" => array(),
					"type" => "select"
					),
				'hide_sidebar_on_single_events' => array(
					"title" => esc_html__('Hide sidebar on the single event', 'holy-church'),
					"desc" => wp_kses_data( __("Hide sidebar on the single event's page", 'holy-church') ),
					"std" => 0,
					"type" => "checkbox"
					),
				'widgets_above_page_events' => array(
					"title" => esc_html__('Widgets above the page', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_above_content_events' => array(
					"title" => esc_html__('Widgets above the content', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_content_events' => array(
					"title" => esc_html__('Widgets below the content', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'widgets_below_page_events' => array(
					"title" => esc_html__('Widgets below the page', 'holy-church'),
					"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'holy-church') ),
					"std" => 'hide',
					"options" => array(),
					"type" => "select"
					),
				'footer_scheme_events' => array(
					"title" => esc_html__('Footer Color Scheme', 'holy-church'),
					"desc" => wp_kses_data( __('Select color scheme to decorate footer area', 'holy-church') ),
					"std" => 'dark',
					"options" => array(),
					"type" => "select"
					),
				'footer_widgets_events' => array(
					"title" => esc_html__('Footer widgets', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the footer', 'holy-church') ),
					"std" => 'footer_widgets',
					"options" => array(),
					"type" => "select"
					),
				'footer_columns_events' => array(
					"title" => esc_html__('Footer columns', 'holy-church'),
					"desc" => wp_kses_data( __('Select number columns to show widgets in the footer. If 0 - autodetect by the widgets count', 'holy-church') ),
					"dependency" => array(
						'footer_widgets_events' => array('^hide')
					),
					"std" => 0,
					"options" => holy_church_get_list_range(0,6),
					"type" => "select"
					),
				'footer_wide_events' => array(
					"title" => esc_html__('Footer fullwide', 'holy-church'),
					"desc" => wp_kses_data( __('Do you want to stretch the footer to the entire window width?', 'holy-church') ),
					"std" => 0,
					"type" => "checkbox"
					)
				)
			);
		}
	}
}

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('holy_church_tribe_events_theme_setup9')) {
	add_action( 'after_setup_theme', 'holy_church_tribe_events_theme_setup9', 9 );
	function holy_church_tribe_events_theme_setup9() {
		
		if (holy_church_exists_tribe_events()) {
			add_action( 'wp_enqueue_scripts', 								'holy_church_tribe_events_frontend_scripts', 1100 );
			add_filter( 'holy_church_filter_merge_styles',						'holy_church_tribe_events_merge_styles' );
			add_filter( 'holy_church_filter_get_css',							'holy_church_tribe_events_get_css', 10, 4 );
			add_filter( 'holy_church_filter_post_type_taxonomy',				'holy_church_tribe_events_post_type_taxonomy', 10, 2 );
			if (!is_admin()) {
				add_filter( 'holy_church_filter_detect_blog_mode',				'holy_church_tribe_events_detect_blog_mode' );
				add_filter( 'holy_church_filter_get_post_categories', 			'holy_church_tribe_events_get_post_categories');
				add_filter( 'holy_church_filter_get_post_date',		 			'holy_church_tribe_events_get_post_date');
			} else {
				add_action( 'admin_enqueue_scripts',						'holy_church_tribe_events_admin_scripts' );
			}
		}
		if (is_admin()) {
			add_filter( 'holy_church_filter_tgmpa_required_plugins',			'holy_church_tribe_events_tgmpa_required_plugins' );
		}

	}
}



// Check if Tribe Events is installed and activated
if ( !function_exists( 'holy_church_exists_tribe_events' ) ) {
	function holy_church_exists_tribe_events() {
		return class_exists( 'Tribe__Events__Main' );
	}
}

// Return true, if current page is any tribe_events page
if ( !function_exists( 'holy_church_is_tribe_events_page' ) ) {
	function holy_church_is_tribe_events_page() {
		$rez = false;
		if (holy_church_exists_tribe_events())
			if (!is_search()) $rez = tribe_is_event() || tribe_is_event_query() || tribe_is_event_category() || tribe_is_event_venue() || tribe_is_event_organizer();
		return $rez;
	}
}

// Detect current blog mode
if ( !function_exists( 'holy_church_tribe_events_detect_blog_mode' ) ) {
	//Handler of the add_filter( 'holy_church_filter_detect_blog_mode', 'holy_church_tribe_events_detect_blog_mode' );
	function holy_church_tribe_events_detect_blog_mode($mode='') {
		if (holy_church_is_tribe_events_page())
			$mode = 'events';
		return $mode;
	}
}

// Return taxonomy for current post type
if ( !function_exists( 'holy_church_tribe_events_post_type_taxonomy' ) ) {
	//Handler of the add_filter( 'holy_church_filter_post_type_taxonomy',	'holy_church_tribe_events_post_type_taxonomy', 10, 2 );
	function holy_church_tribe_events_post_type_taxonomy($tax='', $post_type='') {
		if (holy_church_exists_tribe_events() && $post_type == Tribe__Events__Main::POSTTYPE)
			$tax = Tribe__Events__Main::TAXONOMY;
		return $tax;
	}
}

// Show categories of the current event
if ( !function_exists( 'holy_church_tribe_events_get_post_categories' ) ) {
	//Handler of the add_filter( 'holy_church_filter_get_post_categories', 		'holy_church_tribe_events_get_post_categories');
	function holy_church_tribe_events_get_post_categories($cats='') {
		global $wp_query;
		$id = $wp_query->current_post>=0 ? get_the_ID() : $wp_query->post->ID;
		$post_type = $wp_query->current_post>=0 ? get_post_type() : $wp_query->post->post_type;
		if ($post_type==Tribe__Events__Main::POSTTYPE) {
			$cats = holy_church_get_post_terms(', ', $id, Tribe__Events__Main::TAXONOMY);
		}
		return $cats;
	}
}

// Return date of the current event
if ( !function_exists( 'holy_church_tribe_events_get_post_date' ) ) {
	//Handler of the add_filter( 'holy_church_filter_get_post_date',		 		'holy_church_tribe_events_get_post_date');
	function holy_church_tribe_events_get_post_date($dt='') {
		global $wp_query;
		$id = $wp_query->current_post>=0 ? get_the_ID() : $wp_query->post->ID;
		$post_type = $wp_query->current_post>=0 ? get_post_type() : $wp_query->post->post_type;
		if ($post_type==Tribe__Events__Main::POSTTYPE) {
			$dt = tribe_get_start_date($wp_query->current_post>=0 ? null : $id, true, 'Y-m-d');
			$dt = sprintf($dt < date('Y-m-d') 
								? esc_html__('Started on %s', 'holy-church') 
								: esc_html__('Starting %s', 'holy-church'),
								date(get_option('date_format'), strtotime($dt)));
		}
		return $dt;
	}
}
	
// Enqueue Tribe Events admin scripts and styles
if ( !function_exists( 'holy_church_tribe_events_admin_scripts' ) ) {
	//Handler of the add_action( 'admin_enqueue_scripts', 'holy_church_tribe_events_admin_scripts' );
	function holy_church_tribe_events_admin_scripts() {
	}
}

// Enqueue Tribe Events custom scripts and styles
if ( !function_exists( 'holy_church_tribe_events_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'holy_church_tribe_events_frontend_scripts', 1100 );
	function holy_church_tribe_events_frontend_scripts() {
		if (holy_church_is_tribe_events_page()) {
			if (holy_church_is_on(holy_church_get_theme_option('debug_mode')) && holy_church_get_file_dir('plugins/the-events-calendar/the-events-calendar.css')!='')
				wp_enqueue_style( 'holy-church-the-events-calendar',  holy_church_get_file_url('plugins/the-events-calendar/the-events-calendar.css'), array(), null );
				wp_enqueue_style( 'holy-church-the-events-calendar-images',  holy_church_get_file_url('css/the-events-calendar.css'), array(), null );
		}
	}
}

// Merge custom styles
if ( !function_exists( 'holy_church_tribe_events_merge_styles' ) ) {
	//Handler of the add_filter('holy_church_filter_merge_styles', 'holy_church_tribe_events_merge_styles');
	function holy_church_tribe_events_merge_styles($list) {
		$list[] = 'plugins/the-events-calendar/the-events-calendar.css';
		$list[] = 'css/the-events-calendar.css';
		return $list;
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'holy_church_tribe_events_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('holy_church_filter_tgmpa_required_plugins',	'holy_church_tribe_events_tgmpa_required_plugins');
	function holy_church_tribe_events_tgmpa_required_plugins($list=array()) {
		if (in_array('the-events-calendar', holy_church_storage_get('required_plugins')))
			$list[] = array(
					'name' 		=> esc_html__('Tribe Events Calendar', 'holy-church'),
					'slug' 		=> 'the-events-calendar',
					'required' 	=> false
				);
		return $list;
	}
}



// Add Tribe Events specific items into lists
//------------------------------------------------------------------------

// Add sidebar
if ( !function_exists( 'holy_church_tribe_events_list_sidebars' ) ) {
	//Handler of the add_filter( 'holy_church_filter_list_sidebars', 'holy_church_tribe_events_list_sidebars' );
	function holy_church_tribe_events_list_sidebars($list=array()) {
		$list['tribe_events_widgets'] = array(
											'name' => esc_html__('Tribe Events Widgets', 'holy-church'),
											'description' => esc_html__('Widgets to be shown on the Tribe Events pages', 'holy-church')
											);
		return $list;
	}
}



// Add Tribe Events specific styles into color scheme
//------------------------------------------------------------------------

// Add styles into CSS
if ( !function_exists( 'holy_church_tribe_events_get_css' ) ) {
	//Handler of the add_filter( 'holy_church_filter_get_css', 'holy_church_tribe_events_get_css', 10, 4 );
	function holy_church_tribe_events_get_css($css, $colors, $fonts, $scheme='') {
		if (isset($css['fonts']) && $fonts) {
			$css['fonts'] .= <<<CSS
			
.tribe-events-list .tribe-events-list-event-title,
.tribe-common--breakpoint-medium.tribe-common .tribe-common-h6--min-medium,
.tribe-common .tribe-common-h1, .tribe-common .tribe-common-h2,
.tribe-common .tribe-common-h3, .tribe-common .tribe-common-h4,
.tribe-common .tribe-common-h5, .tribe-common .tribe-common-h6,
.tribe-common .tribe-common-h7, .tribe-common .tribe-common-h8,
.tribe-events-calendar-month__header-column-title.tribe-common-b3,
.tribe-common .tribe-common-anchor-alt  {
	{$fonts['h3_font-family']}
}

#tribe-events .tribe-events-button,
.tribe-events-button,
.tribe-events-cal-links a,
.tribe-events-sub-nav li a {
	{$fonts['button_font-family']}
	{$fonts['button_font-size']}
	{$fonts['button_font-weight']}
	{$fonts['button_font-style']}
	{$fonts['button_line-height']}
	{$fonts['button_text-decoration']}
	{$fonts['button_text-transform']}
	{$fonts['button_letter-spacing']}
}

#tribe-bar-form button, #tribe-bar-form a,
.tribe-events-read-more,
.tribe-events-list .tribe-events-list-separator-month,
.tribe-events-c-view-selector__list-item-link,
.tribe-events-calendar thead th,
.tribe-events-schedule, .tribe-events-schedule h2,
.tribe-events .tribe-events-c-view-selector__list-item-text,
.tribe-common .tribe-common-h3 .tribe-events-c-top-bar__datepicker-desktop 
.tribe-common-h3.tribe-common-h--alt.tribe-events-c-top-bar__datepicker-button,
.tribe-common .tribe-common-c-btn.tribe-events-c-search__button,
.tribe-common .tribe-common-c-btn-border, .tribe-common a.tribe-common-c-btn-border,
.tribe-common--breakpoint-medium.tribe-events .tribe-events-c-nav__next, 
.tribe-common--breakpoint-medium.tribe-events .tribe-events-c-nav__prev,
.tribe-common .tribe-common-c-btn-border, .tribe-common a.tribe-common-c-btn-border,
.tribe-events-c-nav__list-item.tribe-events-c-nav__list-item--prev .tribe-events-c-nav__prev,
.tribe-events-c-nav__list-item.tribe-events-c-nav__list-item--next .tribe-events-c-nav__next,
.tribe-common .tribe-common-anchor-alt, .tribe-events .tribe-events-c-ical__lin,
.tribe-common--breakpoint-medium.tribe-common .tribe-common-b3--min-medium  {
	{$fonts['button_font-family']}
	{$fonts['button_letter-spacing']}
}
.tribe-common .tribe-common-b2.tribe-events-calendar-list__event-datetime-wrapper,
.tribe-events .tribe-events-calendar-month__calendar-event-tooltip-datetime  {
	{$fonts['button_font-family']}
	{$fonts['button_font-weight']}
}
.tribe-events-list .tribe-events-list-separator-month,
.tribe-events-calendar thead th,
.tribe-events-schedule, .tribe-events-schedule h2,
.tribe-events-calendar td div[id*="tribe-events-daynum-"], 
.tribe-events-calendar td div[id*="tribe-events-daynum-"] a, 
.tribe-events-calendar-day__event-title.tribe-common-h6.tribe-common-h4--min-medium,
.tribe-events-calendar-list__event-title.tribe-common-h6.tribe-common-h4--min-medium,
.tribe-events-calendar-day__event-title.tribe-common-h6.tribe-common-h4--min-medium a,
.tribe-events-calendar-list__event-title.tribe-common-h6.tribe-common-h4--min-medium a,
.tribe-events .tribe-events-calendar-list__event-date-tag-weekday {
	{$fonts['h5_font-family']}
}
#tribe-bar-form input, #tribe-events-content.tribe-events-month,
#tribe-events-content .tribe-events-calendar div[id*="tribe-events-event-"] h3.tribe-events-month-event-title,
#tribe-mobile-container .type-tribe_events,
.tribe-events-list-widget ol li .tribe-event-title,
.tribe-events .datepicker .day,
.tribe-common .tribe-common-b1,
.tribe-common .tribe-common-b2,
.tribe-common .tribe-common-c-btn, .tribe-common a.tribe-common-c-btn,
.tribe-events .tribe-events-calendar-month__calendar-event-datetime,
.tribe-common--breakpoint-medium.tribe-common .tribe-common-form-control-text__input, 
.tribe-common .tribe-common-form-control-text__input,
.tribe-common .tribe-events-calendar-month__calendar-event-tooltip-description.tribe-common-b3 {
	{$fonts['p_font-family']}
}
.tribe-events-loop .tribe-event-schedule-details,
.single-tribe_events #tribe-events-content .tribe-events-event-meta dt,
#tribe-mobile-container .type-tribe_events .tribe-event-date-start {
	{$fonts['info_font-family']};
}

CSS;

			
			$rad = holy_church_get_border_radius();
			$css['fonts'] .= <<<CSS


#tribe-bar-form .tribe-bar-submit input[type="submit"],
#tribe-bar-form button,
#tribe-bar-form a,
.tribe-common .tribe-common-c-btn.tribe-events-c-search__button,
#tribe-events .tribe-events-button,
.tribe-events-button,
.tribe-events-cal-links a,
.tribe-events-sub-nav li a {
	-webkit-border-radius: {$rad};
	    -ms-border-radius: {$rad};
			border-radius: {$rad};
}

CSS;
		}


		if (isset($css['colors']) && $colors) {
			$css['colors'] .= <<<CSS

/* Buttons */
#tribe-bar-form .tribe-bar-submit input[type="submit"],
#tribe-bar-form.tribe-bar-mini .tribe-bar-submit input[type="submit"],
#tribe-bar-form #tribe-bar-views-toggle,
#tribe-events .tribe-events-button,
.tribe-events-button,
.tribe-events-cal-links a,
.tribe-events-sub-nav li a {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}
#tribe-bar-form .tribe-bar-submit input[type="submit"]:hover,
#tribe-bar-form .tribe-bar-submit input[type="submit"]:focus,
#tribe-bar-form.tribe-bar-mini .tribe-bar-submit input[type="submit"]:focus,
#tribe-bar-form.tribe-bar-mini .tribe-bar-submit input[type="submit"]:focus,
#tribe-events .tribe-events-button:hover,
.tribe-events-button:hover,
.tribe-events-cal-links a:hover,
.tribe-events-sub-nav li a:hover {
	color: {$colors['bg_color']};
	background-color: {$colors['text_hover']};
}

/* Filters bar */
#tribe-bar-form {
	color: {$colors['text_dark']};
}
#tribe-bar-form #tribe-bar-collapse-toggle{
	color: {$colors['inverse_link']} !important;
}
#tribe-bar-form input[type="text"] {
	color: {$colors['text_dark']};
	border-color: {$colors['text_light']};
}
#tribe-bar-views li.tribe-bar-views-option a,
#tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active a {
	color: {$colors['inverse_link']};
	background: {$colors['text_link']};
}
#tribe-bar-views li.tribe-bar-views-option a:hover,
#tribe-bar-views .tribe-bar-views-list .tribe-bar-views-option.tribe-bar-active a:hover {
	color: {$colors['bg_color']};
	background: {$colors['text_dark']};
}
.datepicker thead tr:first-child th:hover, .datepicker tfoot tr th:hover {
	color: {$colors['text_link']};
	background: {$colors['text_dark']};
}

/* Content */
.tribe-events-calendar thead th {
	color: {$colors['bg_color']};
	background: {$colors['text_dark']} !important;
	border-color: {$colors['text_dark']} !important;
}
.tribe-events-calendar thead th + th:before {
	background: {$colors['bg_color']};
}
#tribe-events-content .tribe-events-calendar td {
	border-color: {$colors['bd_color']} !important;
}
.tribe-events-calendar td div[id*="tribe-events-daynum-"],
.tribe-events-calendar td div[id*="tribe-events-daynum-"] > a {
	color: {$colors['text_dark']};
}
.tribe-events-calendar td.tribe-events-othermonth {
	color: {$colors['alter_light']};
	background: {$colors['alter_bg_color']} !important;
}
.tribe-events-calendar td.tribe-events-othermonth div[id*="tribe-events-daynum-"],
.tribe-events-calendar td.tribe-events-othermonth div[id*="tribe-events-daynum-"] > a {
	color: {$colors['alter_light']};
}
.tribe-events-calendar td.tribe-events-past div[id*="tribe-events-daynum-"], .tribe-events-calendar td.tribe-events-past div[id*="tribe-events-daynum-"] > a {
	color: {$colors['text_light']};
}
.tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"],
.tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"] > a {
	color: {$colors['text_link']};
}
.tribe-events-calendar td.tribe-events-present:before {
	border-color: {$colors['text_link']};
}
.tribe-events-calendar .tribe-events-has-events:after {
	background-color: {$colors['text']};
}
.tribe-events-calendar .mobile-active.tribe-events-has-events:after {
	background-color: {$colors['bg_color']};
}
#tribe-events-content .tribe-events-calendar td,
#tribe-events-content .tribe-events-calendar div[id*="tribe-events-event-"] h3.tribe-events-month-event-title a {
	color: {$colors['text_dark']};
}
#tribe-events-content .tribe-events-calendar div[id*="tribe-events-event-"] h3.tribe-events-month-event-title a:hover {
	color: {$colors['text_link']};
}
#tribe-events-content .tribe-events-calendar td.mobile-active,
#tribe-events-content .tribe-events-calendar td.mobile-active:hover {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}
#tribe-events-content .tribe-events-calendar td.mobile-active div[id*="tribe-events-daynum-"] {
	color: {$colors['bg_color']};
	background-color: {$colors['text_dark']};
}
#tribe-events-content .tribe-events-calendar td.tribe-events-othermonth.mobile-active div[id*="tribe-events-daynum-"] a,
.tribe-events-calendar .mobile-active div[id*="tribe-events-daynum-"] a {
	background-color: transparent;
	color: {$colors['bg_color']};
}

/* Tooltip */
.recurring-info-tooltip,
.tribe-events-calendar .tribe-events-tooltip,
.tribe-events-week .tribe-events-tooltip,
.tribe-events-tooltip .tribe-events-arrow {
	color: {$colors['alter_text']};
	background: {$colors['alter_bg_color']};
}
#tribe-events-content .tribe-events-tooltip h4 { 
	color: {$colors['text_link']};
	background: {$colors['text_dark']};
}
.tribe-events-tooltip .tribe-event-duration {
	color: {$colors['text_light']};
}

/* Events list */
.tribe-events-list-separator-month {
	color: {$colors['text_dark']};
}
.tribe-events-list-separator-month:after {
	border-color: {$colors['bd_color']};
}
.tribe-events-list .type-tribe_events + .type-tribe_events {
	border-color: {$colors['bd_color']};
}
.tribe-events-list .tribe-events-event-cost span {
	color: {$colors['bg_color']};
	border-color: {$colors['text_dark']};
	background: {$colors['text_dark']};
}
.tribe-mobile .tribe-events-loop .tribe-events-event-meta {
	color: {$colors['alter_text']};
	border-color: {$colors['alter_bd_color']};
	background-color: {$colors['alter_bg_color']};
}
.tribe-mobile .tribe-events-loop .tribe-events-event-meta a {
	color: {$colors['alter_link']};
}
.tribe-mobile .tribe-events-loop .tribe-events-event-meta a:hover {
	color: {$colors['alter_hover']};
}
.tribe-mobile .tribe-events-list .tribe-events-venue-details {
	border-color: {$colors['alter_bd_color']};
}

/* Events day */
.tribe-events-day .tribe-events-day-time-slot h5 {
	color: {$colors['bg_color']};
	background: {$colors['text_dark']};
}

/* Single Event */
.single-tribe_events .tribe-events-venue-map {
	color: {$colors['alter_text']};
	border-color: {$colors['alter_bd_hover']};
	background: {$colors['alter_bg_hover']};
}
.single-tribe_events .tribe-events-schedule .tribe-events-cost {
	color: {$colors['text_dark']};
}
.single-tribe_events .type-tribe_events {
	border-color: {$colors['bd_color']};
}

.tribe-events-calendar td .tribe-events-viewmore a {
	color: {$colors['text']};
}
.tribe-events-calendar td .tribe-events-viewmore a:hover {
	color: {$colors['alter_bg_hover']};
}



/* New Design */
.tribe-common a {
	color: {$colors['text_link']};
}
.tribe-common a:hover, 
.tribe-common a:active, 
.tribe-common a:focus{
	color: {$colors['text_hover']};
}

.tribe-events-calendar-day__event-datetime,
.tribe-events-calendar-list__event-datetime,
.tribe-events .tribe-events-calendar-month__calendar-event-tooltip-datetime {
	color: {$colors['text_link']};
}

.tribe-events .tribe-events-calendar-month__day-date-link {
	color: {$colors['text_dark']};
}
.tribe-events .tribe-events-calendar-month__day-date-link:hover {
	color: {$colors['text_hover']};
}
.tribe-events-calendar-day__event-title.tribe-common-h6.tribe-common-h4--min-medium a,
.tribe-events-calendar-list__event-title .tribe-events-calendar-list__event-title-link,
.tribe-common .tribe-events-calendar-month__calendar-event-tooltip-title.tribe-common-h7 a{
	color: {$colors['text_dark']};
}
.tribe-events-calendar-day__event-title.tribe-common-h6.tribe-common-h4--min-medium a:hover,
.tribe-events-calendar-list__event-title .tribe-events-calendar-list__event-title-link:hover,
.tribe-common .tribe-events-calendar-month__calendar-event-tooltip-title.tribe-common-h7 a:hover {
	color: {$colors['text_link']};
}

.tribe-common--breakpoint-medium.tribe-events .tribe-events-c-view-selector--tabs .tribe-events-c-view-selector__list-item--active .tribe-events-c-view-selector__list-item-link:after {
	background: {$colors['text_link']};
}

.tribe-common .tribe-events-c-top-bar__datepicker-time .tribe-common-h3 {
	color: {$colors['text_link']};
}

.tribe-events .tribe-events-c-view-selector__list-item-text {
	color: {$colors['text_dark']};
}

.tribe-common .tribe-events-c-view-selector__list-item a:hover span,
.tribe-common .tribe-events-c-view-selector__list-item--active a span {
	color: {$colors['text_link']};
}

.tribe-events .datepicker .prev .tribe-common-svgicon:before,
.tribe-events .datepicker .next .tribe-common-svgicon:before {
	color: {$colors['bg_color']} ;
}

.tribe-common .tribe-events-c-nav__next, 
.tribe-common .tribe-events-c-nav__prev {
	color: {$colors['text_link']} ;
}

.tribe-common--breakpoint-medium.tribe-events .tribe-events-c-nav__next:hover, 
.tribe-common--breakpoint-medium.tribe-events .tribe-events-c-nav__prev:hover{
	color: {$colors['text_hover']} ;
}

.tribe-common .tribe-common-c-btn.tribe-events-c-search__button {
	background-color: {$colors['text_link']} ;
}

.tribe-common .tribe-common-c-btn.tribe-events-c-search__button:hover {
	background-color: {$colors['text_hover']} ;
}

.tribe-common .tribe-common-c-btn-border, .tribe-common a.tribe-common-c-btn-border {
	color: {$colors['bg_color']};
	background-color: {$colors['text_link']} ;
	border-color: {$colors['text_link']} ;
}
.tribe-common .tribe-common-c-btn-border:focus, .tribe-common .tribe-common-c-btn-border:hover, 
.tribe-common a.tribe-common-c-btn-border:focus, .tribe-common a.tribe-common-c-btn-border:hover {
	color: {$colors['bg_color']};
	background-color: {$colors['text_hover']} ;
	border-color: {$colors['text_hover']} ;
}

.tribe-common--breakpoint-medium.tribe-events .tribe-events-calendar-month__day:hover:after {
	background-color: {$colors['text_link']};
}

.tribe-common .tribe-common-anchor-alt,
.tribe-events .tribe-events-c-ical__link  {
	color: {$colors['bg_color']};
	background-color: {$colors['text_link']};
}

.tribe-common .tribe-common-anchor-alt:hover,
.tribe-events .tribe-events-c-ical__link:active, 
.tribe-events .tribe-events-c-ical__link:focus, 
.tribe-events .tribe-events-c-ical__link:hover {
	color: {$colors['bg_color']};
	background-color: {$colors['text_hover']};
}

.tribe-events .tribe-events-c-events-bar__search-button, 
.tribe-events .tribe-events-c-view-selector__button { 
	background: transparent !important;
}

.tribe-common .tribe-common-svgicon--list:before,
.tribe-events .tribe-events-c-events-bar__search-button-icon:before {
	color: {$colors['text_link']};
}

.tribe-common .tribe-common-svgicon--list:hover:before,
.tribe-events .tribe-events-c-events-bar__search-button-icon:hover:before {
	color: {$colors['text_hover']};
}

.tribe-events .tribe-events-c-events-bar__search-button:before,
.tribe-events .tribe-events-c-view-selector__button:before {
	background-color: {$colors['text_hover']};
}

.tribe-common .tribe-events-calendar-month__day-cell .tribe-events-calendar-month__day-date.tribe-common-h4{
	color: {$colors['text_light']};
}

.tribe-events .tribe-events-calendar-month__day-cell--mobile:focus, 
.tribe-events .tribe-events-calendar-month__day-cell--mobile:hover {
	background-color: {$colors['text_link']} !important;
}

.tribe-events .tribe-events-calendar-month__day-cell--mobile:focus .tribe-events-calendar-month__day-date-daynum, 
.tribe-events .tribe-events-calendar-month__day-cell--mobile:hover .tribe-events-calendar-month__day-date-daynum {
	color: {$colors['bg_color']};
}

.tribe-events .tribe-events-calendar-month__mobile-events-icon--event {
	background-color: {$colors['text_link']};
}

.tribe-events .tribe-events-calendar-month__day-cell--mobile:focus .tribe-events-calendar-month__mobile-events-icon--event, 
.tribe-events .tribe-events-calendar-month__day-cell--mobile:hover .tribe-events-calendar-month__mobile-events-icon--event {
	background-color: {$colors['bg_color']};
}

.tribe-common .tribe-common-c-loader__dot {
	background-color: {$colors['text_link']};
}

.tribe-common--breakpoint-medium.tribe-common .tribe-common-h3 {
	color: {$colors['bg_color']};
	background-color: {$colors['text_dark']};	
}
.tribe-common--breakpoint-medium.tribe-common .tribe-common-h3:hover {
	color: {$colors['bg_color']};
	background-color: {$colors['text_hover']};	
}

.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon,
.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon {
	color: {$colors['bg_color']};
	background-color: {$colors['text_link']};
	border-color: {$colors['text_link']};
}
.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon[disabled],
.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon[disabled] {
	border-color: {$colors['text_light']} !important;
}

.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon:hover,
.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon:hover {
	color: {$colors['bg_color']};
	background-color: {$colors['text_hover']};
	border-color: {$colors['text_hover']};
}

.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon:before,
.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon:after{
	color: {$colors['bg_color']};
}

.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon:hover:before,
.tribe-common .tribe-events-c-top-bar__nav-list .tribe-common-c-btn-icon:hover:after{
	color: {$colors['bg_color']};	
}

.tribe-events .tribe-events-calendar-month__multiday-event-bar-inner {
	color: {$colors['bg_color']};
	background-color: {$colors['alter_bd_color']};	
}

.tribe-events .tribe-events-c-nav__prev:disabled,
.tribe-events .tribe-events-c-nav__next:disabled {
	color: {$colors['text_light']} !important;
	background: transparent !important;
}

.tribe-events .datepicker .datepicker-switch {
	color: {$colors['bg_color']};
}

.tribe-events .table-condensed th{
	color: {$colors['bg_color']};
}

.tribe-events .table-condensed th:hover{
	background-color: {$colors['text_link']} !important;
}

.tribe-events .datepicker .day.active, .tribe-events .datepicker .day.active.focused, 
.tribe-events .datepicker .day.active:focus, .tribe-events .datepicker .day.active:hover, 
.tribe-events .datepicker .month.active, .tribe-events .datepicker .month.active.focused, 
.tribe-events .datepicker .month.active:focus, .tribe-events .datepicker .month.active:hover, 
.tribe-events .datepicker .year.active, .tribe-events .datepicker .year.active.focused, 
.tribe-events .datepicker .year.active:focus, .tribe-events .datepicker .year.active:hover {
	background: {$colors['text_link']};
}

.tribe-events .datepicker .datepicker-switch:active, .tribe-events .datepicker .datepicker-switch:focus, 
.tribe-events .datepicker .datepicker-switch:hover, .tribe-events .datepicker .next:active, 
.tribe-events .datepicker .next:focus, .tribe-events .datepicker .next:hover, 
.tribe-events .datepicker .prev:active, .tribe-events .datepicker .prev:focus, 
.tribe-events .datepicker .prev:hover {
	background: {$colors['text_hover']} !important;
}

.tribe-events .tribe-events-calendar-month__day--current .tribe-events-calendar-month__day-date, 
.tribe-events .tribe-events-calendar-month__day--current .tribe-events-calendar-month__day-date-link {
	color: {$colors['text_link']} ;
}

.tribe-common-h3.tribe-common-h--alt.tribe-events-c-top-bar__datepicker-button {
	color: {$colors['text_dark']};
	background-color: transparent;
}
.tribe-common-h3.tribe-common-h--alt.tribe-events-c-top-bar__datepicker-button:hover {
	color: {$colors['text_hover']} !important;
	background-color: transparent !important;
}

.tribe-events .tribe-events-c-small-cta__price {
	color: {$colors['text_link']} ;
}

.tribe-common .tribe-events-calendar-list__event-date-tag-daynum.tribe-common-h5 {
	color: {$colors['text_link']} ;
}

.tribe-events-sub-nav li.tribe-events-nav-previous a,
.tribe-events-sub-nav li.tribe-events-nav-next a {
	background: {$colors['text_link']};
}

.tribe-events-sub-nav li.tribe-events-nav-previous a:hover,
.tribe-events-sub-nav li.tribe-events-nav-next a:hover {
	background: {$colors['text_hover']};
}

.tribe-events-c-nav__list-item.tribe-events-c-nav__list-item--prev .tribe-events-c-nav__prev,
.tribe-events-c-nav__list-item.tribe-events-c-nav__list-item--next .tribe-events-c-nav__next {
	color: {$colors['text_link']};
}

.tribe-events-c-nav__list-item.tribe-events-c-nav__list-item--prev .tribe-events-c-nav__prev:hover,
.tribe-events-c-nav__list-item.tribe-events-c-nav__list-item--next .tribe-events-c-nav__next:hover {
	color: {$colors['text_hover']} ;
}

.tribe-common .tribe-events-calendar-month__day-cell.tribe-events-calendar-month__day-cell--mobile {
	background-color: {$colors['alter_bg_color']} ;
}

.tribe-common--breakpoint-medium.tribe-common .tribe-common-form-control-text__input{
	border-color: {$colors['input_bd_color']} ;
}
.tribe-common--breakpoint-medium.tribe-common .tribe-common-form-control-text__input:focus{
	border-color: {$colors['input_bd_hover']} ;
}

#tribe-events .tribe-events-cal-links a.tribe-events-gcal, 
#tribe-events .tribe-events-cal-links a.tribe-events-ical {
	background-color: {$colors['text_hover']} ;
}
#tribe-events .tribe-events-cal-links a.tribe-events-gcal:hover, 
#tribe-events .tribe-events-cal-links a.tribe-events-ical:hover {
	background-color: {$colors['text_link']} ;
}

.tribe-common--breakpoint-medium.tribe-common .tribe-common-b3 {
	color: {$colors['text_dark']};
}

.tribe-events-calendar-list__event-title {
	color: {$colors['text']};
}
.tribe-events-calendar-list__event-venue {
	color: {$colors['text_dark']};
}
.tribe-events-calendar-list__event-description {
	color: {$colors['text']};
}
.tribe-events-calendar-day__event-description.tribe-common-b2,
.tribe-events-calendar-month__calendar-event-tooltip-description.tribe-common-b3 {
	color: {$colors['text']};
}


CSS;
		}
		
		return $css;
	}
}
?>