<?php
/* Essential Grid support functions
------------------------------------------------------------------------------- */


// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('holy_church_essential_grid_theme_setup9')) {
	add_action( 'after_setup_theme', 'holy_church_essential_grid_theme_setup9', 9 );
	function holy_church_essential_grid_theme_setup9() {
		if (holy_church_exists_essential_grid()) {
			add_action( 'wp_enqueue_scripts', 							'holy_church_essential_grid_frontend_scripts', 1100 );
			add_filter( 'holy_church_filter_merge_styles',					'holy_church_essential_grid_merge_styles' );
		}
		if (is_admin()) {
			add_filter( 'holy_church_filter_tgmpa_required_plugins',		'holy_church_essential_grid_tgmpa_required_plugins' );
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'holy_church_exists_essential_grid' ) ) {
	function holy_church_exists_essential_grid() {
		return defined('EG_PLUGIN_PATH');
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'holy_church_essential_grid_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('holy_church_filter_tgmpa_required_plugins',	'holy_church_essential_grid_tgmpa_required_plugins');
	function holy_church_essential_grid_tgmpa_required_plugins($list=array()) {
		if (in_array('essential-grid', holy_church_storage_get('required_plugins'))) {
			$path = holy_church_get_file_dir('plugins/essential-grid/essential-grid.zip');
			$list[] = array(
						'name' 		=> esc_html__('Essential Grid', 'holy-church'),
						'slug' 		=> 'essential-grid',
                        'version'   => '3.0.7',
						'source'	=> !empty($path) ? $path : 'upload://essential-grid.zip',
						'required' 	=> false
			);
		}
		return $list;
	}
}
	
// Enqueue plugin's custom styles
if ( !function_exists( 'holy_church_essential_grid_frontend_scripts' ) ) {
	//Handler of the add_action( 'wp_enqueue_scripts', 'holy_church_essential_grid_frontend_scripts', 1100 );
	function holy_church_essential_grid_frontend_scripts() {
		if (holy_church_is_on(holy_church_get_theme_option('debug_mode')) && holy_church_get_file_dir('plugins/essential-grid/essential-grid.css')!='')
			wp_enqueue_style( 'holy-church-essential-grid',  holy_church_get_file_url('plugins/essential-grid/essential-grid.css'), array(), null );
	}
}
	
// Merge custom styles
if ( !function_exists( 'holy_church_essential_grid_merge_styles' ) ) {
	//Handler of the add_filter('holy_church_filter_merge_styles', 'holy_church_essential_grid_merge_styles');
	function holy_church_essential_grid_merge_styles($list) {
		$list[] = 'plugins/essential-grid/essential-grid.css';
		return $list;
	}
}
?>