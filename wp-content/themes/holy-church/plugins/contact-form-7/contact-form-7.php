<?php
/* Contact Form 7 support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('holy_church_cf7_theme_setup9')) {
	add_action( 'after_setup_theme', 'holy_church_cf7_theme_setup9', 9 );
	function holy_church_cf7_theme_setup9() {
		
		add_filter( 'holy_church_filter_merge_scripts',	'holy_church_cf7_merge_scripts');
		add_filter( 'holy_church_filter_merge_styles',	'holy_church_cf7_merge_styles' );

		if (holy_church_exists_cf7()) {
			add_action( 'wp_enqueue_scripts',		'holy_church_cf7_frontend_scripts', 1100 );
		}

		if (is_admin()) {
			add_filter( 'holy_church_filter_tgmpa_required_plugins',	'holy_church_cf7_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'holy_church_cf7_tgmpa_required_plugins' ) ) {
	function holy_church_cf7_tgmpa_required_plugins($list=array()) {
		if (in_array('contact-form-7', holy_church_storage_get('required_plugins'))) {
			// CF7 plugin
			$list[] = array(
				'name' 		=> esc_html__('Contact Form 7', 'holy-church'),
				'slug' 		=> 'contact-form-7',
				'required' 	=> false
			);
		}
		return $list;
	}
}


// Check if cf7 installed and activated
if ( !function_exists( 'holy_church_exists_cf7' ) ) {
	function holy_church_exists_cf7() {
		return class_exists('WPCF7');
	}
}

// Enqueue custom scripts
if ( !function_exists( 'holy_church_cf7_frontend_scripts' ) ) {
	function holy_church_cf7_frontend_scripts() {
		if (holy_church_exists_cf7()) {
			if (holy_church_is_on(holy_church_get_theme_option('debug_mode')) && ($holy_church_url = holy_church_get_file_url('plugins/contact-form-7/contact-form-7.js')) != '')
				wp_enqueue_script( 'holy-church-cf7', $holy_church_url, array('jquery'), null, true );
		}
	}
}
	
// Merge custom scripts
if ( !function_exists( 'holy_church_cf7_merge_scripts' ) ) {
	function holy_church_cf7_merge_scripts($list) {
		if (holy_church_exists_cf7()) {
			$list[] = 'plugins/contact-form-7/contact-form-7.js';
		}
		return $list;
	}
}

// Merge custom styles
if ( !function_exists( 'holy_church_cf7_merge_styles' ) ) {
	function holy_church_cf7_merge_styles($list) {
		if (holy_church_exists_cf7()) {
			$list[] = 'plugins/contact-form-7/_contact-form-7.scss';
		}
		return $list;
	}
}
?>