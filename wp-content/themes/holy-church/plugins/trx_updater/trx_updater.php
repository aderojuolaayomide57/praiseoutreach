<?php
/* TRX Updater support functions
------------------------------------------------------------------------------- */

// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('holy_church_trx_updater_theme_setup9')) {
	add_action( 'after_setup_theme', 'holy_church_trx_updater_theme_setup9', 9 );
	function holy_church_trx_updater_theme_setup9() {

		if (is_admin()) {
			add_filter( 'holy_church_filter_tgmpa_required_plugins',			'holy_church_trx_updater_tgmpa_required_plugins' );
		}
	}
}

// Filter to add in the required plugins list
if ( !function_exists( 'holy_church_trx_updater_tgmpa_required_plugins' ) ) {
	//Handler of the add_filter('holy_church_filter_tgmpa_required_plugins',	'holy_church_trx_updater_tgmpa_required_plugins');
	function holy_church_trx_updater_tgmpa_required_plugins($list=array()) {
		if (in_array('trx_updater', holy_church_storage_get('required_plugins'))) {
			$path = holy_church_get_file_dir('plugins/trx_updater/trx_updater.zip');
			// TRX Updater plugin
			$list[] = array(
				'name' 		=> esc_html__('ThemeREX Updater', 'holy-church'),
				'slug' 		=> 'trx_updater',
				'version'	=> '1.4.1',
				'source'	=> !empty($path) ? $path : 'upload://trx_updater.zip',
				'required' 	=> false
			);
		}
		return $list;
	}
}

// Check if this plugin installed and activated
if ( !function_exists( 'holy_church_exists_trx_updater' ) ) {
	function holy_church_exists_trx_updater() {
		return function_exists( 'trx_updater_load_plugin_textdomain' );
	}
}
