<?php
/* Give support functions
------------------------------------------------------------------------------- */


// Theme init priorities:
// 9 - register other filters (for installer, etc.)
if (!function_exists('holy_church_give_theme_setup9')) {
    add_action( 'after_setup_theme', 'holy_church_give_theme_setup9', 9 );
    function holy_church_give_theme_setup9() {
        if (holy_church_exists_give()) {
            add_filter( 'give_form_grid_image_size', 'holy_church_give_size');
        }
		
	        if (is_admin()) {
            add_filter( 'holy_church_filter_tgmpa_required_plugins',		'holy_church_give_tgmpa_required_plugins' );
        }
    }
}

if ( !function_exists( 'holy_church_give_size' ) ) {
    function holy_church_give_size()    {
        $image_size = holy_church_get_thumb_size('huge');
        return $image_size;
    }
}

// Filter to add in the required plugins list
if ( !function_exists( 'holy_church_give_tgmpa_required_plugins' ) ) {
    function holy_church_give_tgmpa_required_plugins($list=array()) {
        if (in_array('give', holy_church_storage_get('required_plugins')))
            $list[] = array(
                'name' 		=> esc_html__('Give', 'holy-church'),
                'slug' 		=> 'give',
                'required' 	=> false
            );
        return $list;
    }
}


// Check if plugin installed and activated
if ( !function_exists( 'holy_church_exists_give' ) ) {
    function holy_church_exists_give() {
        return function_exists('Give');
    }
}


?>