<?php
/**
 * The template for homepage posts with "Chess" style
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

holy_church_storage_set('blog_archive', true);

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$holy_church_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$holy_church_sticky_out = is_array($holy_church_stickies) && count($holy_church_stickies) > 0 && get_query_var( 'paged' ) < 1;
	if ($holy_church_sticky_out) {
		?><div class="sticky_wrap columns_wrap"><?php	
	}
	if (!$holy_church_sticky_out) {
		?><div class="chess_wrap posts_container"><?php
	}
	while ( have_posts() ) { the_post(); 
		if ($holy_church_sticky_out && !is_sticky()) {
			$holy_church_sticky_out = false;
			?></div><div class="chess_wrap posts_container"><?php
		}
		get_template_part( 'content', $holy_church_sticky_out && is_sticky() ? 'sticky' :'chess' );
	}
	
	?></div><?php

	holy_church_show_pagination();

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>