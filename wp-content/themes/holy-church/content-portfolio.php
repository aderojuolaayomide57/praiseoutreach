<?php
/**
 * The Portfolio template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_blog_style = explode('_', holy_church_get_theme_option('blog_style'));
$holy_church_columns = empty($holy_church_blog_style[1]) ? 2 : max(2, $holy_church_blog_style[1]);
$holy_church_post_format = get_post_format();
$holy_church_post_format = empty($holy_church_post_format) ? 'standard' : str_replace('post-format-', '', $holy_church_post_format);
$holy_church_animation = holy_church_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_portfolio post_layout_portfolio_'.esc_attr($holy_church_columns).' post_format_'.esc_attr($holy_church_post_format) ); ?>
	<?php echo (!holy_church_is_off($holy_church_animation) ? ' data-animation="'.esc_attr(holy_church_get_animation_classes($holy_church_animation)).'"' : ''); ?>
	>

	<?php
	$holy_church_image_hover = holy_church_get_theme_option('image_hover');
	// Featured image
	holy_church_show_post_featured(array(
		'thumb_size' => holy_church_get_thumb_size(strpos(holy_church_get_theme_option('body_style'), 'full')!==false || $holy_church_columns < 3 ? 'masonry-big' : 'masonry'),
		'show_no_image' => true,
		'class' => $holy_church_image_hover == 'dots' ? 'hover_with_info' : '',
		'post_info' => $holy_church_image_hover == 'dots' ? '<div class="post_info">'.esc_html(get_the_title()).'</div>' : ''
	));
	?>
</article>