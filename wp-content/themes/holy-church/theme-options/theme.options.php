<?php
/**
 * Default Theme Options and Internal Theme Settings
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

// Theme init priorities:
// 1 - register filters to add/remove lists items in the Theme Options
// 2 - create Theme Options
// 3 - add/remove Theme Options elements
// 5 - load Theme Options
// 9 - register other filters (for installer, etc.)
//10 - standard Theme init procedures (not ordered)

if ( !function_exists('holy_church_options_theme_setup1') ) {
	add_action( 'after_setup_theme', 'holy_church_options_theme_setup1', 1 );
	function holy_church_options_theme_setup1() {
		
		// -----------------------------------------------------------------
		// -- ONLY FOR PROGRAMMERS, NOT FOR CUSTOMER
		// -- Internal theme settings
		// -----------------------------------------------------------------
		holy_church_storage_set('settings', array(
			
			'disable_jquery_ui'			=> false,						// Prevent loading custom jQuery UI libraries in the third-party plugins
		
			'max_load_fonts'			=> 3,							// Max fonts number to load from Google fonts or from uploaded fonts
		
			'use_mediaelements'			=> true,						// Load script "Media Elements" to play video and audio
		
			'max_excerpt_length'		=> 60,							// Max words number for the excerpt in the blog style 'Excerpt'.
																		// For style 'Classic' - get half from this value
			'message_maxlength'			=> 1000							// Max length of the message from contact form

		));
		
		
		
		// -----------------------------------------------------------------
		// -- Theme fonts (Google and/or custom fonts)
		// -----------------------------------------------------------------
		
		// Fonts to load when theme start
		// It can be Google fonts or uploaded fonts, placed in the folder /css/font-face/font-name inside the theme folder
		// Attention! Font's folder must have name equal to the font's name, with spaces replaced on the dash '-'
		// For example: font name 'TeX Gyre Termes', folder 'TeX-Gyre-Termes'
		holy_church_storage_set('load_fonts', array(
			// Google font
			array(
				'name'	 => 'Playfair Display',
				'family' => 'serif',
				'styles' => '400,400italic,700,700italic'		// Parameter 'style' used only for the Google fonts
            ),
			// Google font
			array(
				'name'	 => 'Oswald',
				'family' => 'sans-serif',
				'styles' => '400,600,700'		                    // Parameter 'style' used only for the Google fonts
            ),
			// Font-face packed with theme
			array(
				'name'   => 'Montserrat',
				'family' => 'sans-serif'
            )
		));
		
		// Characters subset for the Google fonts. Available values are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese
		holy_church_storage_set('load_fonts_subset', 'latin,latin-ext');
		
		// Settings of the main tags
		holy_church_storage_set('theme_fonts', array(
			'p' => array(
				'title'				=> esc_html__('Main text', 'holy-church'),
				'description'		=> esc_html__('Font settings of the main text of the site', 'holy-church'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '1rem',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.643em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '0em',
				'margin-bottom'		=> '1.4em'
				),
			'h1' => array(
				'title'				=> esc_html__('Heading 1', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '3.929rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '0.9583em',
				'margin-bottom'		=> '0.5833em'
				),
			'h2' => array(
				'title'				=> esc_html__('Heading 2', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '3.214rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '1.0444em',
				'margin-bottom'		=> '0.76em'
				),
			'h3' => array(
				'title'				=> esc_html__('Heading 3', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '2.5rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '1.3333em',
				'margin-bottom'		=> '0.7879em'
				),
			'h4' => array(
				'title'				=> esc_html__('Heading 4', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '2.143rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '1.9565em',
				'margin-bottom'		=> '1.0435em'
				),
			'h5' => array(
				'title'				=> esc_html__('Heading 5', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '1.786rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '1.5em',
				'margin-bottom'		=> '1.3em'
				),
			'h6' => array(
				'title'				=> esc_html__('Heading 6', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '1.429rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.2em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal',
				'margin-top'		=> '2.1176em',
				'margin-bottom'		=> '0.9412em'
				),
			'logo' => array(
				'title'				=> esc_html__('Logo text', 'holy-church'),
				'description'		=> esc_html__('Font settings of the text case of the logo', 'holy-church'),
				'font-family'		=> 'Playfair Display, serif',
				'font-size' 		=> '2.143rem',
				'font-weight'		=> '700',
				'font-style'		=> 'italic',
				'line-height'		=> '1.25em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal'
				),
			'button' => array(
				'title'				=> esc_html__('Buttons', 'holy-church'),
				'font-family'		=> 'Oswald, sans-serif',
				'font-size' 		=> '0.929rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.6em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0.4em'
				),
			'input' => array(
				'title'				=> esc_html__('Input fields', 'holy-church'),
				'description'		=> esc_html__('Font settings of the input fields, dropdowns and textareas', 'holy-church'),
				'font-family'		=> 'Montserrat, sans-serif',
				'font-size' 		=> '1em',
				'font-weight'		=> '400',
				'font-style'		=> 'normal',
				'line-height'		=> '1.6em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'none',
				'letter-spacing'	=> 'normal'
				),
			'info' => array(
				'title'				=> esc_html__('Post meta', 'holy-church'),
				'description'		=> esc_html__('Font settings of the post meta: date, counters, share, etc.', 'holy-church'),
                'font-family'		=> 'Oswald, sans-serif',
                'font-size' 		=> '0.929rem',
                'font-weight'		=> '700',
                'font-style'		=> 'normal',
                'line-height'		=> '1.5em',
                'text-decoration'	=> 'none',
                'text-transform'	=> 'uppercase',
                'letter-spacing'	=> '0.2em',
				'margin-top'		=> '0.4em',
				'margin-bottom'		=> ''
				),
			'menu' => array(
				'title'				=> esc_html__('Main menu', 'holy-church'),
				'description'		=> esc_html__('Font settings of the main menu items', 'holy-church'),
				'font-family'		=> 'Oswald, sans-serif',
				'font-size' 		=> '0.929rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0.2em'
				),
			'submenu' => array(
				'title'				=> esc_html__('Dropdown menu', 'holy-church'),
				'description'		=> esc_html__('Font settings of the dropdown menu items', 'holy-church'),
				'font-family'		=> 'Oswald, sans-serif',
				'font-size' 		=> '0.929rem',
				'font-weight'		=> '700',
				'font-style'		=> 'normal',
				'line-height'		=> '1.5em',
				'text-decoration'	=> 'none',
				'text-transform'	=> 'uppercase',
				'letter-spacing'	=> '0.2em'
				)
		));
		
		
		// -----------------------------------------------------------------
		// -- Theme colors for customizer
		// -- Attention! Inner scheme must be last in the array below
		// -----------------------------------------------------------------
		holy_church_storage_set('schemes', array(
		
			// Color scheme: 'default'
			'default' => array(
				'title'	 => esc_html__('Default', 'holy-church'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'				=> '#ffffff',
					'bd_color'				=> '#ededed',
		
					// Text and links colors
					'text'					=> '#8c8c8c',
					'text_light'			=> '#bcbcbc',
					'text_dark'				=> '#464646',
					'text_link'				=> '#dba860',
					'text_hover'			=> '#ca9a57',
		
					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#f6f4f2',
					'alter_bg_hover'		=> '#464646',
					'alter_bd_color'		=> '#f2f0ec',
					'alter_bd_hover'		=> '#3a3a3a',
					'alter_text'			=> '#464646',
					'alter_light'			=> '#bcbcbc',
					'alter_dark'			=> '#252525',
					'alter_link'			=> '#f6d099',
					'alter_hover'			=> '#464646',
		
					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#f6f4f2',
					'input_bg_hover'		=> '#f6f4f2',
					'input_bd_color'		=> '#f6f4f2',
					'input_bd_hover'		=> '#dba860',
					'input_text'			=> '#8c8c8c',
					'input_light'			=> '#8c8c8c',
					'input_dark'			=> '#464646',
					
					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#ffffff',
					'inverse_light'			=> '#ffffff',
					'inverse_dark'			=> '#ffffff',
					'inverse_link'			=> '#ffffff',
					'inverse_hover'			=> '#ffffff',
				
				)
			),

			// Color scheme: 'Scheme 2'
			'scheme_2' => array(
				'title'	 => esc_html__('Scheme 2', 'holy-church'),
				'colors' => array(

					// Whole block border and background
					'bg_color'				=> '#ffffff',
					'bd_color'				=> '#ededed',

					// Text and links colors
					'text'					=> '#8c8c8c',
					'text_light'			=> '#bcbcbc',
					'text_dark'				=> '#464646',
					'text_link'				=> '#9BC3C0',
					'text_hover'			=> '#709794',

					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#f6f4f2',
					'alter_bg_hover'		=> '#464646',
					'alter_bd_color'		=> '#f2f0ec',
					'alter_bd_hover'		=> '#3a3a3a',
					'alter_text'			=> '#464646',
					'alter_light'			=> '#bcbcbc',
					'alter_dark'			=> '#252525',
					'alter_link'			=> '#f6d099',
					'alter_hover'			=> '#464646',

					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#f6f4f2',
					'input_bg_hover'		=> '#f6f4f2',
					'input_bd_color'		=> '#f6f4f2',
					'input_bd_hover'		=> '#9BC3C0',
					'input_text'			=> '#8c8c8c',
					'input_light'			=> '#8c8c8c',
					'input_dark'			=> '#464646',

					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#ffffff',
					'inverse_light'			=> '#ffffff',
					'inverse_dark'			=> '#ffffff',
					'inverse_link'			=> '#ffffff',
					'inverse_hover'			=> '#ffffff',

				)
			),

			// Color scheme: 'Scheme 3'
			'scheme_3' => array(
				'title'	 => esc_html__('Scheme 3', 'holy-church'),
				'colors' => array(

					// Whole block border and background
					'bg_color'				=> '#ffffff',
					'bd_color'				=> '#ededed',

					// Text and links colors
					'text'					=> '#8c8c8c',
					'text_light'			=> '#bcbcbc',
					'text_dark'				=> '#464646',
					'text_link'				=> '#C77966',
					'text_hover'			=> '#9C5B51',

					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#f6f4f2',
					'alter_bg_hover'		=> '#464646',
					'alter_bd_color'		=> '#f2f0ec',
					'alter_bd_hover'		=> '#3a3a3a',
					'alter_text'			=> '#464646',
					'alter_light'			=> '#bcbcbc',
					'alter_dark'			=> '#252525',
					'alter_link'			=> '#f6d099',
					'alter_hover'			=> '#464646',

					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#f6f4f2',
					'input_bg_hover'		=> '#f6f4f2',
					'input_bd_color'		=> '#f6f4f2',
					'input_bd_hover'		=> '#C77966',
					'input_text'			=> '#8c8c8c',
					'input_light'			=> '#8c8c8c',
					'input_dark'			=> '#464646',

					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#ffffff',
					'inverse_light'			=> '#ffffff',
					'inverse_dark'			=> '#ffffff',
					'inverse_link'			=> '#ffffff',
					'inverse_hover'			=> '#ffffff',

				)
			),

			// Color scheme: 'Scheme 4'
			'scheme_4' => array(
				'title'	 => esc_html__('Scheme 4', 'holy-church'),
				'colors' => array(

					// Whole block border and background
					'bg_color'				=> '#ffffff',
					'bd_color'				=> '#ededed',

					// Text and links colors
					'text'					=> '#8c8c8c',
					'text_light'			=> '#bcbcbc',
					'text_dark'				=> '#464646',
					'text_link'				=> '#93A765',
					'text_hover'			=> '#687647',

					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#f6f4f2',
					'alter_bg_hover'		=> '#464646',
					'alter_bd_color'		=> '#f2f0ec',
					'alter_bd_hover'		=> '#3a3a3a',
					'alter_text'			=> '#464646',
					'alter_light'			=> '#bcbcbc',
					'alter_dark'			=> '#252525',
					'alter_link'			=> '#f6d099',
					'alter_hover'			=> '#464646',

					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#f6f4f2',
					'input_bg_hover'		=> '#f6f4f2',
					'input_bd_color'		=> '#f6f4f2',
					'input_bd_hover'		=> '#93A765',
					'input_text'			=> '#8c8c8c',
					'input_light'			=> '#8c8c8c',
					'input_dark'			=> '#464646',

					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#ffffff',
					'inverse_light'			=> '#ffffff',
					'inverse_dark'			=> '#ffffff',
					'inverse_link'			=> '#ffffff',
					'inverse_hover'			=> '#ffffff',


				)
			),

			// Color scheme: 'dark'
			'dark' => array(
				'title'  => esc_html__('Dark', 'holy-church'),
				'colors' => array(
					
					// Whole block border and background
					'bg_color'				=> '#0e0d12',
					'bd_color'				=> '#1c1b1f',
		
					// Text and links colors
					'text'					=> '#b7b7b7',
					'text_light'			=> '#5f5f5f',
					'text_dark'				=> '#ffffff',
                    'text_link'				=> '#dba860',
                    'text_hover'			=> '#ca9a57',
		
					// Alternative blocks (submenu, buttons, tabs, etc.)
					'alter_bg_color'		=> '#1e1d22',
					'alter_bg_hover'		=> '#28272e',
					'alter_bd_color'		=> '#313131',
					'alter_bd_hover'		=> '#3d3d3d',
					'alter_text'			=> '#a6a6a6',
					'alter_light'			=> '#5f5f5f',
					'alter_dark'			=> '#ffffff',
					'alter_link'			=> '#dba860',
					'alter_hover'			=> '#fe7259',
		
					// Input fields (form's fields and textarea)
					'input_bg_color'		=> '#2e2d32',	//'rgba(62,61,66,0.5)',
					'input_bg_hover'		=> '#2e2d32',	//'rgba(62,61,66,0.5)',
					'input_bd_color'		=> '#2e2d32',	//'rgba(62,61,66,0.5)',
					'input_bd_hover'		=> '#353535',
					'input_text'			=> '#b7b7b7',
					'input_light'			=> '#5f5f5f',
					'input_dark'			=> '#ffffff',
					
					// Inverse blocks (text and links on accented bg)
					'inverse_text'			=> '#1d1d1d',
					'inverse_light'			=> '#5f5f5f',
					'inverse_dark'			=> '#000000',
					'inverse_link'			=> '#ffffff',
					'inverse_hover'			=> '#1d1d1d',

		
				)
			)
		
		));
	}
}


// -----------------------------------------------------------------
// -- Theme options for customizer
// -----------------------------------------------------------------
if (!function_exists('holy_church_options_create')) {

	function holy_church_options_create() {

		holy_church_storage_set('options', array(
		
			// Section 'Title & Tagline' - add theme options in the standard WP section
			'title_tagline' => array(
				"title" => esc_html__('Title, Tagline & Site icon', 'holy-church'),
				"desc" => wp_kses_data( __('Specify site title and tagline (if need) and upload the site icon', 'holy-church') ),
				"type" => "section"
				),
		
		
			// Section 'Header' - add theme options in the standard WP section
			'header_image' => array(
				"title" => esc_html__('Header', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload logo images, select header type and widgets set for the header', 'holy-church') )
							. '<br>'
							. wp_kses_data( __('<b>Attention!</b> Some of these options can be overridden in the following sections (Homepage, Blog archive, Shop, Events, etc.) or in the settings of individual pages', 'holy-church') ),
				"type" => "section"
				),
			'header_image_override' => array(
				"title" => esc_html__('Header image override', 'holy-church'),
				"desc" => wp_kses_data( __("Allow override the header image with the page's/post's/product's/etc. featured image", 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'header_style' => array(
				"title" => esc_html__('Header style', 'holy-church'),
				"desc" => wp_kses_data( __('Select style to display the site header', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 'header-default',
				"options" => array(),
				"type" => "select"
				),
			'header_position' => array(
				"title" => esc_html__('Header position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position to display the site header', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 'default',
				"options" => array(),
				"type" => "select"
				),
			'header_widgets' => array(
				"title" => esc_html__('Header widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the header on each page', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church'),
					"desc" => wp_kses_data( __('Select set of widgets to show in the header on this page', 'holy-church') ),
				),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'header_columns' => array(
				"title" => esc_html__('Header columns', 'holy-church'),
				"desc" => wp_kses_data( __('Select number columns to show widgets in the Header. If 0 - autodetect by the widgets count', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"dependency" => array(
					'header_style' => array('header-default'),
					'header_widgets' => array('^hide')
				),
				"std" => 0,
				"options" => holy_church_get_list_range(0,6),
				"type" => "select"
				),
			'header_scheme' => array(
				"title" => esc_html__('Header Color Scheme', 'holy-church'),
				"desc" => wp_kses_data( __('Select color scheme to decorate header area', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 'inherit',
				"options" => array(),
				"refresh" => false,
				"type" => "select"
				),
			'header_fullheight' => array(
				"title" => esc_html__('Header fullheight', 'holy-church'),
				"desc" => wp_kses_data( __("Enlarge header area to fill whole screen. Used only if header have a background image", 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'header_wide' => array(
				"title" => esc_html__('Header fullwide', 'holy-church'),
				"desc" => wp_kses_data( __('Do you want to stretch the header widgets area to the entire window width?', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"dependency" => array(
					'header_style' => array('header-default')
				),
				"std" => 1,
				"type" => "checkbox"
				),

			'menu_info' => array(
				"title" => esc_html__('Menu settings', 'holy-church'),
				"desc" => wp_kses_data( __('Select main menu style, position, color scheme and other parameters', 'holy-church') ),
				"type" => "info"
				),
			'menu_style' => array(
				"title" => esc_html__('Menu position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position of the main menu', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 'top',
				"options" => array(
					'top'	=> esc_html__('Top',	'holy-church'),
					'left'	=> esc_html__('Left',	'holy-church'),
					'right'	=> esc_html__('Right',	'holy-church')
				),
				"type" => "hidden"
				),
			'menu_scheme' => array(
				"title" => esc_html__('Menu Color Scheme', 'holy-church'),
				"desc" => wp_kses_data( __('Select color scheme to decorate main menu area', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => 'inherit',
				"options" => array(),
				"refresh" => false,
				"type" => "select"
				),
			'menu_side_stretch' => array(
				"title" => esc_html__('Stretch sidemenu', 'holy-church'),
				"desc" => wp_kses_data( __('Stretch sidemenu to window height (if menu items number >= 5)', 'holy-church') ),
				"dependency" => array(
					'menu_style' => array('left', 'right')
				),
				"std" => 1,
				"type" => "checkbox"
				),
			'menu_side_icons' => array(
				"title" => esc_html__('Iconed sidemenu', 'holy-church'),
				"desc" => wp_kses_data( __('Get icons from anchors and display it in the sidemenu or mark sidemenu items with simple dots', 'holy-church') ),
				"dependency" => array(
					'menu_style' => array('left', 'right')
				),
				"std" => 1,
				"type" => "checkbox"
				),
			'menu_mobile_fullscreen' => array(
				"title" => esc_html__('Mobile menu fullscreen', 'holy-church'),
				"desc" => wp_kses_data( __('Display mobile and side menus on full screen (if checked) or slide narrow menu from the left or from the right side (if not checked)', 'holy-church') ),
				"dependency" => array(
					'menu_style' => array('left', 'right')
				),
				"std" => 1,
				"type" => "checkbox"
				),
			'logo_info' => array(
				"title" => esc_html__('Logo settings', 'holy-church'),
				"desc" => wp_kses_data( __('Select logo images for the normal and Retina displays', 'holy-church') ),
				"type" => "info"
				),
			'logo' => array(
				"title" => esc_html__('Logo', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'logo_retina' => array(
				"title" => esc_html__('Logo for Retina', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo used on Retina displays (if empty - use default logo from the field above)', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'logo_inverse' => array(
				"title" => esc_html__('Logo inverse', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo to display it on the dark background', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'logo_inverse_retina' => array(
				"title" => esc_html__('Logo inverse for Retina', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo used on Retina displays (if empty - use default logo from the field above)', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'logo_side' => array(
				"title" => esc_html__('Logo side', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo (with vertical orientation) to display it in the side menu', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'logo_side_retina' => array(
				"title" => esc_html__('Logo side for Retina', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo (with vertical orientation) to display it in the side menu on Retina displays (if empty - use default logo from the field above)', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'logo_text' => array(
				"title" => esc_html__('Logo from Site name', 'holy-church'),
				"desc" => wp_kses_data( __('Do you want use Site name and description as Logo if images above are not selected?', 'holy-church') ),
				"std" => 1,
				"type" => "checkbox"
				),
			
		
		
			// Section 'Content'
			'content' => array(
				"title" => esc_html__('Content', 'holy-church'),
				"desc" => wp_kses_data( __('Options for the content area.', 'holy-church') )
							. '<br>'
							. wp_kses_data( __('<b>Attention!</b> Some of these options can be overridden in the following sections (Homepage, Blog archive, Shop, Events, etc.) or in the settings of individual pages', 'holy-church') ),
				"type" => "section",
				),
			'body_style' => array(
				"title" => esc_html__('Body style', 'holy-church'),
				"desc" => wp_kses_data( __('Select width of the body content', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"refresh" => false,
				"std" => 'wide',
				"options" => array(
					'boxed'		=> esc_html__('Boxed',		'holy-church'),
					'wide'		=> esc_html__('Wide',		'holy-church'),
					'fullwide'	=> esc_html__('Fullwide',	'holy-church'),
					'fullscreen'=> esc_html__('Fullscreen',	'holy-church')
				),
				"type" => "select"
				),
			'color_scheme' => array(
				"title" => esc_html__('Site Color Scheme', 'holy-church'),
				"desc" => wp_kses_data( __('Select color scheme to decorate whole site. Attention! Case "Inherit" can be used only for custom pages, not for root site content in the Appearance - Customize', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"std" => 'default',
				"options" => array(),
				"refresh" => false,
				"type" => "select"
				),
			'expand_content' => array(
				"title" => esc_html__('Expand content', 'holy-church'),
				"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Content', 'holy-church')
				),
				"refresh" => false,
				"std" => 1,
				"type" => "checkbox"
				),
			'remove_margins' => array(
				"title" => esc_html__('Remove margins', 'holy-church'),
				"desc" => wp_kses_data( __('Remove margins above and below the content area', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Content', 'holy-church')
				),
				"refresh" => false,
				"std" => 0,
				"type" => "checkbox"
				),
			'seo_snippets' => array(
				"title" => esc_html__('SEO snippets', 'holy-church'),
				"desc" => wp_kses_data( __('Add structured data markup to the single posts and pages', 'holy-church') ),
				"std" => 0,
				"type" => "checkbox"
				),
			'privacy_text' => array(
				"title" => esc_html__("Text with Privacy Policy link", 'holy-church'),
				"desc"  => wp_kses_data( __("Specify text with Privacy Policy link for the checkbox 'I agree ...'", 'holy-church') ),
				"std"   => wp_kses( __( 'I agree that my submitted data is being collected and stored.', 'holy-church'), 'holy_church_kses_content' ),
				"type"  => "text"
			),
			'border_radius' => array(
				"title" => esc_html__('Border radius', 'holy-church'),
				"desc" => wp_kses_data( __('Specify the border radius of the form fields and buttons in pixels or other valid CSS units', 'holy-church') ),
				"std" => 0,
				"type" => "text"
				),
			'boxed_bg_image' => array(
				"title" => esc_html__('Boxed bg image', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload image, used as background in the boxed body', 'holy-church') ),
				"dependency" => array(
					'body_style' => array('boxed')
				),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"std" => '',
				"type" => "image"
				),
			'no_image' => array(
				"title" => esc_html__('No image placeholder', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload image, used as placeholder for the posts without featured image', 'holy-church') ),
				"std" => '',
				"type" => "image"
				),
			'sidebar_widgets' => array(
				"title" => esc_html__('Sidebar widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select default widgets to show in the sidebar', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"std" => 'sidebar_widgets',
				"options" => array(),
				"type" => "select"
				),
			'sidebar_scheme' => array(
				"title" => esc_html__('Sidebar Color Scheme', 'holy-church'),
				"desc" => wp_kses_data( __('Select color scheme to decorate sidebar', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"std" => 'inherit',
				"options" => array(),
				"refresh" => false,
				"type" => "select"
				),
			'sidebar_position' => array(
				"title" => esc_html__('Sidebar position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position to show sidebar', 'holy-church') ),
				"override" => array(
					'mode' => 'page,give_forms,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"refresh" => false,
				"std" => 'right',
				"options" => array(),
				"type" => "select"
				),
			'hide_sidebar_on_single' => array(
				"title" => esc_html__('Hide sidebar on the single post', 'holy-church'),
				"desc" => wp_kses_data( __("Hide sidebar on the single post's pages", 'holy-church') ),
				"std" => 0,
				"type" => "checkbox"
				),
			'widgets_above_page' => array(
				"title" => esc_html__('Widgets above the page', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'widgets_above_content' => array(
				"title" => esc_html__('Widgets above the content', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'widgets_below_content' => array(
				"title" => esc_html__('Widgets below the content', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'widgets_below_page' => array(
				"title" => esc_html__('Widgets below the page', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Widgets', 'holy-church')
				),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
		
		
		
			// Section 'Footer'
			'footer' => array(
				"title" => esc_html__('Footer', 'holy-church'),
				"desc" => wp_kses_data( __('Select set of widgets and columns number for the site footer', 'holy-church') )
							. '<br>'
							. wp_kses_data( __('<b>Attention!</b> Some of these options can be overridden in the following sections (Homepage, Blog archive, Shop, Events, etc.) or in the settings of individual pages', 'holy-church') ),
				"type" => "section"
				),
			'footer_style' => array(
				"title" => esc_html__('Footer style', 'holy-church'),
				"desc" => wp_kses_data( __('Select style to display the site footer', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Footer', 'holy-church')
				),
				"std" => 'footer-default',
				"options" => array(),
				"type" => "select"
				),
			'footer_scheme' => array(
				"title" => esc_html__('Footer Color Scheme', 'holy-church'),
				"desc" => wp_kses_data( __('Select color scheme to decorate footer area', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Footer', 'holy-church')
				),
				"std" => 'inherit',
				"options" => array(),
				"refresh" => false,
				"type" => "select"
				),
			'footer_widgets' => array(
				"title" => esc_html__('Footer widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the footer', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Footer', 'holy-church')
				),
				"dependency" => array(
					'footer_style' => array('footer-default')
				),
				"std" => 'footer_widgets',
				"options" => array(),
				"type" => "select"
				),
			'footer_columns' => array(
				"title" => esc_html__('Footer columns', 'holy-church'),
				"desc" => wp_kses_data( __('Select number columns to show widgets in the footer. If 0 - autodetect by the widgets count', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Footer', 'holy-church')
				),
				"dependency" => array(
					'footer_style' => array('footer-default'),
					'footer_widgets' => array('^hide')
				),
				"std" => 3,
				"options" => holy_church_get_list_range(0,6),
				"type" => "select"
				),
			'footer_wide' => array(
				"title" => esc_html__('Footer fullwide', 'holy-church'),
				"desc" => wp_kses_data( __('Do you want to stretch the footer to the entire window width?', 'holy-church') ),
				"override" => array(
					'mode' => 'page,cpt_team,cpt_services,cpt_courses,cpt_portfolio',
					'section' => esc_html__('Footer', 'holy-church')
				),
				"dependency" => array(
					'footer_style' => array('footer-default')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'logo_in_footer' => array(
				"title" => esc_html__('Show logo', 'holy-church'),
				"desc" => wp_kses_data( __('Show logo in the footer', 'holy-church') ),
				'refresh' => false,
				"dependency" => array(
					'footer_style' => array('footer-default')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'logo_footer' => array(
				"title" => esc_html__('Logo for footer', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload site logo to display it in the footer', 'holy-church') ),
				"dependency" => array(
					'footer_style' => array('footer-default'),
					'logo_in_footer' => array('1')
				),
				"std" => '',
				"type" => "image"
				),
			'logo_footer_retina' => array(
				"title" => esc_html__('Logo for footer (Retina)', 'holy-church'),
				"desc" => wp_kses_data( __('Select or upload logo for the footer area used on Retina displays (if empty - use default logo from the field above)', 'holy-church') ),
				"dependency" => array(
					'footer_style' => array('footer-default'),
					'logo_in_footer' => array('1')
				),
				"std" => '',
				"type" => "image"
				),
			'socials_in_footer' => array(
				"title" => esc_html__('Show social icons', 'holy-church'),
				"desc" => wp_kses_data( __('Show social icons in the footer (under logo or footer widgets)', 'holy-church') ),
				"dependency" => array(
					'footer_style' => array('footer-default')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'copyright' => array(
				"title" => esc_html__('Copyright', 'holy-church'),
				"desc" => wp_kses_data( __('Copyright text in the footer. Use {Y} to insert current year and press "Enter" to create a new line', 'holy-church') ),
				"std" => esc_html__('AncoraThemes &copy; {Y}. All rights reserved. Terms of use and Privacy Policy', 'holy-church'),
				"dependency" => array(
					'footer_style' => array('footer-default')
				),
				"refresh" => false,
				"type" => "textarea"
				),
		
		
		
			// Section 'Homepage' - settings for home page
			'homepage' => array(
				"title" => esc_html__('Homepage', 'holy-church'),
				"desc" => wp_kses_data( __('Select blog style and widgets to display on the homepage', 'holy-church') ),
				"type" => "section"
				),
			'expand_content_home' => array(
				"title" => esc_html__('Expand content', 'holy-church'),
				"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden on the Homepage', 'holy-church') ),
				"refresh" => false,
				"std" => 1,
				"type" => "checkbox"
				),
			'show_page_title_home' => array(
				"title" => esc_html__('Page Title area', 'holy-church'),
				"desc" => wp_kses_data( __('Show Page Title area on the Homepage', 'holy-church') ),
				"refresh" => false,
				"std" => 0,
				"type" => "checkbox"
				),
			'blog_style_home' => array(
				"title" => esc_html__('Blog style', 'holy-church'),
				"desc" => wp_kses_data( __('Select posts style for the homepage', 'holy-church') ),
				"std" => 'excerpt',
				"options" => array(),
				"type" => "select"
				),
			'first_post_large_home' => array(
				"title" => esc_html__('First post large', 'holy-church'),
				"desc" => wp_kses_data( __('Make first post large (with Excerpt layout) on the Classic layout of the Homepage', 'holy-church') ),
				"dependency" => array(
					'blog_style_home' => array('classic')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			'header_style_home' => array(
				"title" => esc_html__('Header style', 'holy-church'),
				"desc" => wp_kses_data( __('Select style to display the site header on the homepage', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'header_position_home' => array(
				"title" => esc_html__('Header position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position to display the site header on the homepage', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'header_widgets_home' => array(
				"title" => esc_html__('Header widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the header on the homepage', 'holy-church') ),
				"std" => 'header_widgets',
				"options" => array(),
				"type" => "select"
				),
			'sidebar_widgets_home' => array(
				"title" => esc_html__('Sidebar widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select sidebar to show on the homepage', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'sidebar_position_home' => array(
				"title" => esc_html__('Sidebar position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position to show sidebar on the homepage', 'holy-church') ),
				"refresh" => false,
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'widgets_above_page_home' => array(
				"title" => esc_html__('Widgets above the page', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'holy-church') ),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'widgets_above_content_home' => array(
				"title" => esc_html__('Widgets above the content', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'holy-church') ),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'widgets_below_content_home' => array(
				"title" => esc_html__('Widgets below the content', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'holy-church') ),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			'widgets_below_page_home' => array(
				"title" => esc_html__('Widgets below the page', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'holy-church') ),
				"std" => 'hide',
				"options" => array(),
				"type" => "select"
				),
			
		
		
			// Section 'Blog archive'
			'blog' => array(
				"title" => esc_html__('Blog archive', 'holy-church'),
				"desc" => wp_kses_data( __('Options for the blog archive', 'holy-church') ),
				"type" => "section",
				),
			'expand_content_blog' => array(
				"title" => esc_html__('Expand content', 'holy-church'),
				"desc" => wp_kses_data( __('Expand the content width if the sidebar is hidden on the blog archive', 'holy-church') ),
				"refresh" => false,
				"std" => 1,
				"type" => "checkbox"
				),
			'blog_style' => array(
				"title" => esc_html__('Blog style', 'holy-church'),
				"desc" => wp_kses_data( __('Select posts style for the blog archive', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"dependency" => array(
                    '#page_template' => array( 'blog.php' ),
                    '.editor-page-attributes__template select' => array( 'blog.php' ),
				),
				"std" => 'excerpt',
				"options" => array(),
				"type" => "select"
				),
			'blog_columns' => array(
				"title" => esc_html__('Blog columns', 'holy-church'),
				"desc" => wp_kses_data( __('How many columns should be used in the blog archive (from 2 to 4)?', 'holy-church') ),
				"std" => 2,
				"options" => holy_church_get_list_range(2,4),
				"type" => "hidden"
				),
			'post_type' => array(
				"title" => esc_html__('Post type', 'holy-church'),
				"desc" => wp_kses_data( __('Select post type to show in the blog archive', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"dependency" => array(
                    '#page_template' => array( 'blog.php' ),
                    '.editor-page-attributes__template select' => array( 'blog.php' ),
				),
				"linked" => 'parent_cat',
				"refresh" => false,
				"hidden" => true,
				"std" => 'post',
				"options" => array(),
				"type" => "select"
				),
			'parent_cat' => array(
				"title" => esc_html__('Category to show', 'holy-church'),
				"desc" => wp_kses_data( __('Select category to show in the blog archive', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"dependency" => array(
                    '#page_template' => array( 'blog.php' ),
                    '.editor-page-attributes__template select' => array( 'blog.php' ),
				),
				"refresh" => false,
				"hidden" => true,
				"std" => '0',
				"options" => array(),
				"type" => "select"
				),
			'posts_per_page' => array(
				"title" => esc_html__('Posts per page', 'holy-church'),
				"desc" => wp_kses_data( __('How many posts will be displayed on this page', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"dependency" => array(
                    '#page_template' => array( 'blog.php' ),
                    '.editor-page-attributes__template select' => array( 'blog.php' ),
				),
				"hidden" => true,
				"std" => '10',
				"type" => "text"
				),
			"blog_pagination" => array( 
				"title" => esc_html__('Pagination style', 'holy-church'),
				"desc" => wp_kses_data( __('Show Older/Newest posts or Page numbers below the posts list', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"std" => "pages",
				"options" => array(
					'pages'	=> esc_html__("Page numbers", 'holy-church'),
					'links'	=> esc_html__("Older/Newest", 'holy-church'),
					'more'	=> esc_html__("Load more", 'holy-church'),
					'infinite' => esc_html__("Infinite scroll", 'holy-church')
				),
				"type" => "select"
				),
			'show_filters' => array(
				"title" => esc_html__('Show filters', 'holy-church'),
				"desc" => wp_kses_data( __('Show categories as tabs to filter posts', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"dependency" => array(
                    '#page_template' => array( 'blog.php' ),
                    '.editor-page-attributes__template select' => array( 'blog.php' ),
					'blog_style' => array('portfolio', 'gallery')
				),
				"hidden" => true,
				"std" => 0,
				"type" => "checkbox"
				),
			'first_post_large' => array(
				"title" => esc_html__('First post large', 'holy-church'),
				"desc" => wp_kses_data( __('Make first post large (with Excerpt layout) on the Classic layout of blog archive', 'holy-church') ),
				"dependency" => array(
					'blog_style' => array('classic')
				),
				"std" => 0,
				"type" => "checkbox"
				),
			"blog_content" => array( 
				"title" => esc_html__('Posts content', 'holy-church'),
				"desc" => wp_kses_data( __("Show full post's content in the blog or only post's excerpt", 'holy-church') ),
				"std" => "excerpt",
				"options" => array(
					'excerpt'	=> esc_html__('Excerpt',	'holy-church'),
					'fullpost'	=> esc_html__('Full post',	'holy-church')
				),
				"type" => "select"
				),
			'time_diff_before' => array(
				"title" => esc_html__('Time difference', 'holy-church'),
				"desc" => wp_kses_data( __("How many days show time difference instead post's date", 'holy-church') ),
				"std" => 5,
				"type" => "text"
				),
			'related_posts' => array(
				"title" => esc_html__('Related posts', 'holy-church'),
				"desc" => wp_kses_data( __('How many related posts should be displayed in the single post?', 'holy-church') ),
				"std" => 2,
				"options" => holy_church_get_list_range(2,4),
				"type" => "select"
				),
			'related_style' => array(
				"title" => esc_html__('Related posts style', 'holy-church'),
				"desc" => wp_kses_data( __('Select style of the related posts output', 'holy-church') ),
				"std" => 2,
				"options" => holy_church_get_list_styles(1,2),
				"type" => "select"
				),
			"blog_animation" => array( 
				"title" => esc_html__('Animation for the posts', 'holy-church'),
				"desc" => wp_kses_data( __('Select animation to show posts in the blog. Attention! Do not use any animation on pages with the "wheel to the anchor" behaviour (like a "Chess 2 columns")!', 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Content', 'holy-church')
				),
				"dependency" => array(
                    '#page_template' => array( 'blog.php' ),
                    '.editor-page-attributes__template select' => array( 'blog.php' ),
				),
				"std" => "none",
				"options" => array(),
				"type" => "select"
				),
			'header_style_blog' => array(
				"title" => esc_html__('Header style', 'holy-church'),
				"desc" => wp_kses_data( __('Select style to display the site header on the blog archive', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'header_position_blog' => array(
				"title" => esc_html__('Header position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position to display the site header on the blog archive', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'header_widgets_blog' => array(
				"title" => esc_html__('Header widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select set of widgets to show in the header on the blog archive', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'sidebar_widgets_blog' => array(
				"title" => esc_html__('Sidebar widgets', 'holy-church'),
				"desc" => wp_kses_data( __('Select sidebar to show on the blog archive', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'sidebar_position_blog' => array(
				"title" => esc_html__('Sidebar position', 'holy-church'),
				"desc" => wp_kses_data( __('Select position to show sidebar on the blog archive', 'holy-church') ),
				"refresh" => false,
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'hide_sidebar_on_single_blog' => array(
				"title" => esc_html__('Hide sidebar on the single post', 'holy-church'),
				"desc" => wp_kses_data( __("Hide sidebar on the single post", 'holy-church') ),
				"std" => 0,
				"type" => "checkbox"
				),
			'widgets_above_page_blog' => array(
				"title" => esc_html__('Widgets above the page', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show above page (content and sidebar)', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'widgets_above_content_blog' => array(
				"title" => esc_html__('Widgets above the content', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show at the beginning of the content area', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'widgets_below_content_blog' => array(
				"title" => esc_html__('Widgets below the content', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show at the ending of the content area', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			'widgets_below_page_blog' => array(
				"title" => esc_html__('Widgets below the page', 'holy-church'),
				"desc" => wp_kses_data( __('Select widgets to show below the page (content and sidebar)', 'holy-church') ),
				"std" => 'inherit',
				"options" => array(),
				"type" => "select"
				),
			
		
		
		
			// Section 'Colors' - choose color scheme and customize separate colors from it
			'scheme' => array(
				"title" => esc_html__('* Color scheme editor', 'holy-church'),
				"desc" => wp_kses_data( __("<b>Simple settings</b> - you can change only accented color, used for links, buttons and some accented areas.", 'holy-church') )
						. '<br>'
						. wp_kses_data( __("<b>Advanced settings</b> - change all scheme's colors and get full control over the appearance of your site!", 'holy-church') ),
				"priority" => 1000,
				"type" => "section"
				),
		
			'color_settings' => array(
				"title" => esc_html__('Color settings', 'holy-church'),
				"desc" => '',
				"std" => 'simple',
				"options" => array(
					"simple"  => esc_html__("Simple", 'holy-church'),
					"advanced" => esc_html__("Advanced", 'holy-church')
				),
				"refresh" => false,
				"type" => "switch"
				),
		
			'color_scheme_editor' => array(
				"title" => esc_html__('Color Scheme', 'holy-church'),
				"desc" => wp_kses_data( __('Select color scheme to edit colors', 'holy-church') ),
				"std" => 'default',
				"options" => array(),
				"refresh" => false,
				"type" => "select"
				),
		
			'scheme_storage' => array(
				"title" => esc_html__('Colors storage', 'holy-church'),
				"desc" => esc_html__('Hidden storage of the all color from the all color shemes (only for internal usage)', 'holy-church'),
				"std" => '',
				"refresh" => false,
				"type" => "hidden"
				),
		
			'scheme_info_single' => array(
				"title" => esc_html__('Colors for single post/page', 'holy-church'),
				"desc" => wp_kses_data( __('Specify colors for single post/page (not for alter blocks)', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
				
			'bg_color' => array(
				"title" => esc_html__('Background color', 'holy-church'),
				"desc" => wp_kses_data( __('Background color of the whole page', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'bd_color' => array(
				"title" => esc_html__('Border color', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the bordered elements, separators, etc.', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'text' => array(
				"title" => esc_html__('Text', 'holy-church'),
				"desc" => wp_kses_data( __('Plain text color on single page/post', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_light' => array(
				"title" => esc_html__('Light text', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the post meta: post date and author, comments number, etc.', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_dark' => array(
				"title" => esc_html__('Dark text', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the headers, strong text, etc.', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_link' => array(
				"title" => esc_html__('Links', 'holy-church'),
				"desc" => wp_kses_data( __('Color of links and accented areas', 'holy-church') ),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'text_hover' => array(
				"title" => esc_html__('Links hover', 'holy-church'),
				"desc" => wp_kses_data( __('Hover color for links and accented areas', 'holy-church') ),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'scheme_info_alter' => array(
				"title" => esc_html__('Colors for alternative blocks', 'holy-church'),
				"desc" => wp_kses_data( __('Specify colors for alternative blocks - rectangular blocks with its own background color (posts in homepage, blog archive, search results, widgets on sidebar, footer, etc.)', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
		
			'alter_bg_color' => array(
				"title" => esc_html__('Alter background color', 'holy-church'),
				"desc" => wp_kses_data( __('Background color of the alternative blocks', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_bg_hover' => array(
				"title" => esc_html__('Alter hovered background color', 'holy-church'),
				"desc" => wp_kses_data( __('Background color for the hovered state of the alternative blocks', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_bd_color' => array(
				"title" => esc_html__('Alternative border color', 'holy-church'),
				"desc" => wp_kses_data( __('Border color of the alternative blocks', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_bd_hover' => array(
				"title" => esc_html__('Alternative hovered border color', 'holy-church'),
				"desc" => wp_kses_data( __('Border color for the hovered state of the alter blocks', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_text' => array(
				"title" => esc_html__('Alter text', 'holy-church'),
				"desc" => wp_kses_data( __('Text color of the alternative blocks', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_light' => array(
				"title" => esc_html__('Alter light', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the info blocks inside block with alternative background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_dark' => array(
				"title" => esc_html__('Alter dark', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the headers inside block with alternative background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_link' => array(
				"title" => esc_html__('Alter link', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the links inside block with alternative background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'alter_hover' => array(
				"title" => esc_html__('Alter hover', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the hovered links inside block with alternative background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'scheme_info_input' => array(
				"title" => esc_html__('Colors for the form fields', 'holy-church'),
				"desc" => wp_kses_data( __('Specify colors for the form fields and textareas', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
		
			'input_bg_color' => array(
				"title" => esc_html__('Inactive background', 'holy-church'),
				"desc" => wp_kses_data( __('Background color of the inactive form fields', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_bg_hover' => array(
				"title" => esc_html__('Active background', 'holy-church'),
				"desc" => wp_kses_data( __('Background color of the focused form fields', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_bd_color' => array(
				"title" => esc_html__('Inactive border', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the border in the inactive form fields', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_bd_hover' => array(
				"title" => esc_html__('Active border', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the border in the focused form fields', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_text' => array(
				"title" => esc_html__('Inactive field', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the text in the inactive fields', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_light' => array(
				"title" => esc_html__('Disabled field', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the disabled field', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'input_dark' => array(
				"title" => esc_html__('Active field', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the active field', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
		
			'scheme_info_inverse' => array(
				"title" => esc_html__('Colors for inverse blocks', 'holy-church'),
				"desc" => wp_kses_data( __('Specify colors for inverse blocks, rectangular blocks with background color equal to the links color or one of accented colors (if used in the current theme)', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"type" => "info"
				),
		
			'inverse_text' => array(
				"title" => esc_html__('Inverse text', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the text inside block with accented background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_light' => array(
				"title" => esc_html__('Inverse light', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the info blocks inside block with accented background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_dark' => array(
				"title" => esc_html__('Inverse dark', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the headers inside block with accented background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_link' => array(
				"title" => esc_html__('Inverse link', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the links inside block with accented background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),
			'inverse_hover' => array(
				"title" => esc_html__('Inverse hover', 'holy-church'),
				"desc" => wp_kses_data( __('Color of the hovered links inside block with accented background', 'holy-church') ),
				"dependency" => array(
					'color_settings' => array('^simple')
				),
				"std" => '$holy_church_get_scheme_color',
				"refresh" => false,
				"type" => "color"
				),


			// Section 'Hidden'
			'media_title' => array(
				"title" => esc_html__('Media title', 'holy-church'),
				"desc" => wp_kses_data( __('Used as title for the audio and video item in this post', 'holy-church') ),
				"override" => array(
					'mode' => 'post',
					'section' => esc_html__('Title', 'holy-church')
				),
				"hidden" => true,
				"std" => '',
				"type" => "text"
				),
			'media_author' => array(
				"title" => esc_html__('Media author', 'holy-church'),
				"desc" => wp_kses_data( __('Used as author name for the audio and video item in this post', 'holy-church') ),
				"override" => array(
					'mode' => 'post',
					'section' => esc_html__('Title', 'holy-church')
				),
				"hidden" => true,
				"std" => '',
				"type" => "text"
				),


			// Internal options.
			// Attention! Don't change any options in the section below!
			'reset_options' => array(
				"title" => '',
				"desc" => '',
				"std" => '0',
				"type" => "hidden",
				),

		));


		// Prepare panel 'Fonts'
		$fonts = array(
		
			// Panel 'Fonts' - manage fonts loading and set parameters of the base theme elements
			'fonts' => array(
				"title" => esc_html__('* Fonts settings', 'holy-church'),
				"desc" => '',
				"priority" => 1500,
				"type" => "panel"
				),

			// Section 'Load_fonts'
			'load_fonts' => array(
				"title" => esc_html__('Load fonts', 'holy-church'),
				"desc" => wp_kses_data( __('Specify fonts to load when theme start. You can use them in the base theme elements: headers, text, menu, links, input fields, etc.', 'holy-church') )
						. '<br>'
						. wp_kses_data( __('<b>Attention!</b> Press "Refresh" button to reload preview area after the all fonts are changed', 'holy-church') ),
				"type" => "section"
				),
			'load_fonts_subset' => array(
				"title" => esc_html__('Google fonts subsets', 'holy-church'),
				"desc" => wp_kses_data( __('Specify comma separated list of the subsets which will be load from Google fonts', 'holy-church') )
						. '<br>'
						. wp_kses_data( __('Available subsets are: latin,latin-ext,cyrillic,cyrillic-ext,greek,greek-ext,vietnamese', 'holy-church') ),
				"refresh" => false,
				"std" => '$holy_church_get_load_fonts_subset',
				"type" => "text"
				)
		);

		for ($i=1; $i<=holy_church_get_theme_setting('max_load_fonts'); $i++) {
			$fonts["load_fonts-{$i}-info"] = array(
				"title" => esc_html(sprintf(__('Font %s', 'holy-church'), $i)),
				"desc" => '',
				"type" => "info",
				);
			$fonts["load_fonts-{$i}-name"] = array(
				"title" => esc_html__('Font name', 'holy-church'),
				"desc" => '',
				"refresh" => false,
				"std" => '$holy_church_get_load_fonts_option',
				"type" => "text"
				);
			$fonts["load_fonts-{$i}-family"] = array(
				"title" => esc_html__('Font family', 'holy-church'),
				"desc" => $i==1 
							? wp_kses_data( __('Select font family to use it if font above is not available', 'holy-church') )
							: '',
				"refresh" => false,
				"std" => '$holy_church_get_load_fonts_option',
				"options" => array(
					'inherit' => esc_html__("Inherit", 'holy-church'),
					'serif' => esc_html__('serif', 'holy-church'),
					'sans-serif' => esc_html__('sans-serif', 'holy-church'),
					'monospace' => esc_html__('monospace', 'holy-church'),
					'cursive' => esc_html__('cursive', 'holy-church'),
					'fantasy' => esc_html__('fantasy', 'holy-church')
				),
				"type" => "select"
				);
			$fonts["load_fonts-{$i}-styles"] = array(
				"title" => esc_html__('Font styles', 'holy-church'),
				"desc" => $i==1 
							? wp_kses_data( __('Font styles used only for the Google fonts. This is a comma separated list of the font weight and styles. For example: 400,400italic,700', 'holy-church') )
											. '<br>'
								. wp_kses_data( __('<b>Attention!</b> Each weight and style increase download size! Specify only used weights and styles.', 'holy-church') )
							: '',
				"refresh" => false,
				"std" => '$holy_church_get_load_fonts_option',
				"type" => "text"
				);
		}
		$fonts['load_fonts_end'] = array(
			"type" => "section_end"
			);

		// Sections with font's attributes for each theme element
		$theme_fonts = holy_church_get_theme_fonts();
		foreach ($theme_fonts as $tag=>$v) {
			$fonts["{$tag}_section"] = array(
				"title" => !empty($v['title']) 
								? $v['title'] 
								: esc_html(sprintf(__('%s settings', 'holy-church'), $tag)),
				"desc" => !empty($v['description']) 
								? $v['description'] 
								: wp_kses_post( sprintf(__('Font settings of the "%s" tag.', 'holy-church'), $tag) ),
				"type" => "section",
				);
	
			foreach ($v as $css_prop=>$css_value) {
				if (in_array($css_prop, array('title', 'description'))) continue;
				$options = '';
				$type = 'text';
				$title = ucfirst(str_replace('-', ' ', $css_prop));
				if ($css_prop == 'font-family') {
					$type = 'select';
					$options = array();
				} else if ($css_prop == 'font-weight') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'holy-church'),
						'100' => esc_html__('100 (Light)', 'holy-church'), 
						'200' => esc_html__('200 (Light)', 'holy-church'), 
						'300' => esc_html__('300 (Thin)',  'holy-church'),
						'400' => esc_html__('400 (Normal)', 'holy-church'),
						'500' => esc_html__('500 (Semibold)', 'holy-church'),
						'600' => esc_html__('600 (Semibold)', 'holy-church'),
						'700' => esc_html__('700 (Bold)', 'holy-church'),
						'800' => esc_html__('800 (Black)', 'holy-church'),
						'900' => esc_html__('900 (Black)', 'holy-church')
					);
				} else if ($css_prop == 'font-style') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'holy-church'),
						'normal' => esc_html__('Normal', 'holy-church'), 
						'italic' => esc_html__('Italic', 'holy-church')
					);
				} else if ($css_prop == 'text-decoration') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'holy-church'),
						'none' => esc_html__('None', 'holy-church'), 
						'underline' => esc_html__('Underline', 'holy-church'),
						'overline' => esc_html__('Overline', 'holy-church'),
						'line-through' => esc_html__('Line-through', 'holy-church')
					);
				} else if ($css_prop == 'text-transform') {
					$type = 'select';
					$options = array(
						'inherit' => esc_html__("Inherit", 'holy-church'),
						'none' => esc_html__('None', 'holy-church'), 
						'uppercase' => esc_html__('Uppercase', 'holy-church'),
						'lowercase' => esc_html__('Lowercase', 'holy-church'),
						'capitalize' => esc_html__('Capitalize', 'holy-church')
					);
				}
				$fonts["{$tag}_{$css_prop}"] = array(
					"title" => $title,
					"desc" => '',
					"refresh" => false,
					"std" => '$holy_church_get_theme_fonts_option',
					"options" => $options,
					"type" => $type
				);
			}
			
			$fonts["{$tag}_section_end"] = array(
				"type" => "section_end"
				);
		}

		$fonts['fonts_end'] = array(
			"type" => "panel_end"
			);

		// Add fonts parameters into Theme Options
		holy_church_storage_merge_array('options', '', $fonts);

		// Add Header Video if WP version < 4.7
		if (!function_exists('get_header_video_url')) {
			holy_church_storage_set_array_after('options', 'header_image_override', 'header_video', array(
				"title" => esc_html__('Header video', 'holy-church'),
				"desc" => wp_kses_data( __("Select video to use it as background for the header", 'holy-church') ),
				"override" => array(
					'mode' => 'page',
					'section' => esc_html__('Header', 'holy-church')
				),
				"std" => '',
				"type" => "video"
				)
			);
		}
	}
}


// Return lists with choises when its need in the admin mode
if (!function_exists('holy_church_options_get_list_choises')) {
	add_filter('holy_church_filter_options_get_list_choises', 'holy_church_options_get_list_choises', 10, 2);
	function holy_church_options_get_list_choises($list, $id) {
		if (is_array($list) && count($list)==0) {
			if (strpos($id, 'header_style')===0)
				$list = holy_church_get_list_header_styles(strpos($id, 'header_style_')===0);
			else if (strpos($id, 'header_position')===0)
				$list = holy_church_get_list_header_positions(strpos($id, 'header_position_')===0);
			else if (strpos($id, 'header_widgets')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'header_widgets_')===0, true);
			else if (strpos($id, 'header_scheme')===0 
					|| strpos($id, 'menu_scheme')===0
					|| strpos($id, 'color_scheme')===0
					|| strpos($id, 'sidebar_scheme')===0
					|| strpos($id, 'footer_scheme')===0)
				$list = holy_church_get_list_schemes(true);
			else if (strpos($id, 'sidebar_widgets')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'sidebar_widgets_')===0, true);
			else if (strpos($id, 'sidebar_position')===0)
				$list = holy_church_get_list_sidebars_positions(strpos($id, 'sidebar_position_')===0);
			else if (strpos($id, 'widgets_above_page')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'widgets_above_page_')===0, true);
			else if (strpos($id, 'widgets_above_content')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'widgets_above_content_')===0, true);
			else if (strpos($id, 'widgets_below_page')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'widgets_below_page_')===0, true);
			else if (strpos($id, 'widgets_below_content')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'widgets_below_content_')===0, true);
			else if (strpos($id, 'footer_style')===0)
				$list = holy_church_get_list_footer_styles(strpos($id, 'footer_style_')===0);
			else if (strpos($id, 'footer_widgets')===0)
				$list = holy_church_get_list_sidebars(strpos($id, 'footer_widgets_')===0, true);
			else if (strpos($id, 'blog_style')===0)
				$list = holy_church_get_list_blog_styles(strpos($id, 'blog_style_')===0);
			else if (strpos($id, 'post_type')===0)
				$list = holy_church_get_list_posts_types();
			else if (strpos($id, 'parent_cat')===0)
				$list = holy_church_array_merge(array(0 => esc_html__('- Select category -', 'holy-church')), holy_church_get_list_categories());
			else if (strpos($id, 'blog_animation')===0)
				$list = holy_church_get_list_animations_in();
			else if ($id == 'color_scheme_editor')
				$list = holy_church_get_list_schemes();
			else if (strpos($id, '_font-family') > 0)
				$list = holy_church_get_list_load_fonts(true);
		}
		return $list;
	}
}




// -----------------------------------------------------------------
// -- Create and manage Theme Options
// -----------------------------------------------------------------

// Theme init priorities:
// 2 - create Theme Options
if (!function_exists('holy_church_options_theme_setup2')) {
	add_action( 'after_setup_theme', 'holy_church_options_theme_setup2', 2 );
	function holy_church_options_theme_setup2() {
		holy_church_options_create();
	}
}

// Step 1: Load default settings and previously saved mods
if (!function_exists('holy_church_options_theme_setup5')) {
	add_action( 'after_setup_theme', 'holy_church_options_theme_setup5', 5 );
	function holy_church_options_theme_setup5() {
		holy_church_storage_set('options_reloaded', false);
		holy_church_load_theme_options();
	}
}

// Step 2: Load current theme customization mods
if (is_customize_preview()) {
	if (!function_exists('holy_church_load_custom_options')) {
		add_action( 'wp_loaded', 'holy_church_load_custom_options' );
		function holy_church_load_custom_options() {
			if (!holy_church_storage_get('options_reloaded')) {
				holy_church_storage_set('options_reloaded', true);
				holy_church_load_theme_options();
			}
		}
	}
}

// Load current values for each customizable option
if ( ! function_exists( 'holy_church_load_theme_options' ) ) {
    function holy_church_load_theme_options() {
        $options = holy_church_storage_get( 'options' );
        $reset   = (int) get_theme_mod( 'reset_options', 0 );
        foreach ( $options as $k => $v ) {
            if ( isset( $v['std'] ) ) {
                $value = holy_church_get_theme_option_std( $k, $v['std'] );
                if ( ! $reset ) {
                    if ( isset( $_GET[ $k ] ) ) {
                        $value = wp_kses_data( wp_unslash( $_GET[ $k ] ) );
                    } else {
                        $default_value = -987654321;
                        $tmp           = get_theme_mod( $k, $default_value );
                        if ( $tmp != $default_value ) {
                            $value = $tmp;
                        }
                    }
                }
                holy_church_storage_set_array2( 'options', $k, 'val', $value );
                if ( $reset ) {
                    remove_theme_mod( $k );
                }
            }
        }
        if ( $reset ) {
            // Unset reset flag
            set_theme_mod( 'reset_options', 0 );
            // Regenerate CSS with default colors and fonts
            holy_church_customizer_save_css();
        } else {
            do_action( 'holy_church_action_load_options' );
        }
    }
}

// Override options with stored page/post meta
if ( !function_exists('holy_church_override_theme_options') ) {
	add_action( 'wp', 'holy_church_override_theme_options', 1 );
	function holy_church_override_theme_options($query=null) {
		if (is_page_template('blog.php')) {
			holy_church_storage_set('blog_archive', true);
			holy_church_storage_set('blog_template', get_the_ID());
		}
		holy_church_storage_set('blog_mode', holy_church_detect_blog_mode());
		if (is_singular()) {
			holy_church_storage_set('options_meta', get_post_meta(get_the_ID(), 'holy_church_options', true));
		}
	}
}

// Return 'std' value of the option, processed by special function (if specified)
if ( ! function_exists( 'holy_church_get_theme_option_std' ) ) {
    function holy_church_get_theme_option_std( $opt_name, $opt_std ) {
        if ( ! is_array( $opt_std ) && strpos( $opt_std, '$holy_church_' ) !== false ) {
            $func = substr( $opt_std, 1 );
            if ( function_exists( $func ) ) {
                $opt_std = $func( $opt_name );
            }
        }
        return $opt_std;
    }
}


// Return customizable option value
if (!function_exists('holy_church_get_theme_option')) {
	function holy_church_get_theme_option($name, $defa='', $strict_mode=false, $post_id=0) {
		$rez = $defa;
		$from_post_meta = false;
		if ($post_id > 0) {
			if (!holy_church_storage_isset('post_options_meta', $post_id))
				holy_church_storage_set_array('post_options_meta', $post_id, get_post_meta($post_id, 'holy_church_options', true));
			if (holy_church_storage_isset('post_options_meta', $post_id, $name)) {
				$tmp = holy_church_storage_get_array('post_options_meta', $post_id, $name);
				if (!holy_church_is_inherit($tmp)) {
					$rez = $tmp;
					$from_post_meta = true;
				}
			}
		}
		if (!$from_post_meta && holy_church_storage_isset('options')) {
			if ( !holy_church_storage_isset('options', $name) ) {
				$rez = $tmp = '_not_exists_';
				if (function_exists('trx_addons_get_option'))
					$rez = trx_addons_get_option($name, $tmp, false);
				if ($rez === $tmp) {
					if ($strict_mode) {
						$s = debug_backtrace();
						$s = array_shift($s);
						echo '<pre>' . sprintf(esc_html__('Undefined option "%s" called from:', 'holy-church'), $name);
						if (function_exists('holy_church_dco')) holy_church_dco($s);
						echo '</pre>';
                        wp_die();
					} else
						$rez = $defa;
				}
			} else {
				$blog_mode = holy_church_storage_get('blog_mode');
				// Override option from GET or POST for current blog mode
				if (!empty($blog_mode) && isset($_REQUEST[$name . '_' . $blog_mode])) {
					$rez = wp_kses_data( wp_unslash( $_REQUEST[$name . '_' . $blog_mode] ) );
				// Override option from GET
				} else if (isset($_REQUEST[$name])) {
					$rez = wp_kses_data( wp_unslash( $_REQUEST[$name] ) );
				// Override option from current page settings (if exists)
				} else if (holy_church_storage_isset('options_meta', $name) && !holy_church_is_inherit(holy_church_storage_get_array('options_meta', $name))) {
					$rez = holy_church_storage_get_array('options_meta', $name);
				// Override option from current blog mode settings: 'home', 'search', 'page', 'give_forms', 'post', 'blog', etc. (if exists)
				} else if (!empty($blog_mode) && holy_church_storage_isset('options', $name . '_' . $blog_mode, 'val') && !holy_church_is_inherit(holy_church_storage_get_array('options', $name . '_' . $blog_mode, 'val'))) {
					$rez = holy_church_storage_get_array('options', $name . '_' . $blog_mode, 'val');
				// Get saved option value
				} else if (holy_church_storage_isset('options', $name, 'val')) {
					$rez = holy_church_storage_get_array('options', $name, 'val');
				// Get ThemeREX Addons option value
				} else if (function_exists('trx_addons_get_option')) {
					$rez = trx_addons_get_option($name, $defa, false);
				}
			}
		}
		return $rez;
	}
}


// Check if customizable option exists
if (!function_exists('holy_church_check_theme_option')) {
	function holy_church_check_theme_option($name) {
		return holy_church_storage_isset('options', $name);
	}
}


// Get dependencies list from the Theme Options
if ( !function_exists('holy_church_get_theme_dependencies') ) {
	function holy_church_get_theme_dependencies() {
		$options = holy_church_storage_get('options');
		$depends = array();
		foreach ($options as $k=>$v) {
			if (isset($v['dependency'])) 
				$depends[$k] = $v['dependency'];
		}
		return $depends;
	}
}

// Return internal theme setting value
if (!function_exists('holy_church_get_theme_setting')) {
	function holy_church_get_theme_setting($name) {
		return holy_church_storage_isset('settings', $name) ? holy_church_storage_get_array('settings', $name) : false;
	}
}

// Set theme setting
if ( !function_exists( 'holy_church_set_theme_setting' ) ) {
	function holy_church_set_theme_setting($option_name, $value) {
		if (holy_church_storage_isset('settings', $option_name))
			holy_church_storage_set_array('settings', $option_name, $value);
	}
}
?>