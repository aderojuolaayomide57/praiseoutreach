<?php
/**
 * Generate custom CSS
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */


			
// Additional (calculated) theme-specific colors
// Attention! Don't forget setup custom colors also in the theme.customizer.color-scheme.js
if (!function_exists('holy_church_customizer_add_theme_colors')) {
	function holy_church_customizer_add_theme_colors($colors) {
		if (substr($colors['text'], 0, 1) == '#') {
			$colors['bg_color_0']  = holy_church_hex2rgba( $colors['bg_color'], 0 );
			$colors['bg_color_02']  = holy_church_hex2rgba( $colors['bg_color'], 0.2 );
			$colors['bg_color_04']  = holy_church_hex2rgba( $colors['bg_color'], 0.4 );
			$colors['bg_color_07']  = holy_church_hex2rgba( $colors['bg_color'], 0.7 );
			$colors['bg_color_08']  = holy_church_hex2rgba( $colors['bg_color'], 0.8 );
			$colors['bg_color_09']  = holy_church_hex2rgba( $colors['bg_color'], 0.9 );
			$colors['alter_bg_color_07']  = holy_church_hex2rgba( $colors['alter_bg_color'], 0.7 );
			$colors['alter_bg_color_04']  = holy_church_hex2rgba( $colors['alter_bg_color'], 0.4 );
			$colors['alter_bg_color_02']  = holy_church_hex2rgba( $colors['alter_bg_color'], 0.2 );
			$colors['alter_bd_color_02']  = holy_church_hex2rgba( $colors['alter_bd_color'], 0.2 );
			$colors['text_dark_07']  = holy_church_hex2rgba( $colors['text_dark'], 0.7 );
			$colors['text_link_02']  = holy_church_hex2rgba( $colors['text_link'], 0.2 );
			$colors['text_link_07']  = holy_church_hex2rgba( $colors['text_link'], 0.7 );
			$colors['text_link_blend'] = holy_church_hsb2hex(holy_church_hex2hsb( $colors['text_link'], 2, -5, 5 ));
			$colors['alter_link_blend'] = holy_church_hsb2hex(holy_church_hex2hsb( $colors['alter_link'], 2, -5, 5 ));
            $colors['alter_text_07']  = holy_church_hex2rgba( $colors['alter_text'], 0.7 );
            $colors['alter_text_04']  = holy_church_hex2rgba( $colors['alter_text'], 0.4 );
            $colors['alter_text_02']  = holy_church_hex2rgba( $colors['alter_text'], 0.2 );
		} else {
			$colors['bg_color_0'] = '{{ data.bg_color_0 }}';
			$colors['bg_color_02'] = '{{ data.bg_color_02 }}';
			$colors['bg_color_04'] = '{{ data.bg_color_04 }}';
			$colors['bg_color_07'] = '{{ data.bg_color_07 }}';
			$colors['bg_color_08'] = '{{ data.bg_color_08 }}';
			$colors['bg_color_09'] = '{{ data.bg_color_09 }}';
			$colors['alter_bg_color_07'] = '{{ data.alter_bg_color_07 }}';
			$colors['alter_bg_color_04'] = '{{ data.alter_bg_color_04 }}';
			$colors['alter_bg_color_02'] = '{{ data.alter_bg_color_02 }}';
			$colors['alter_bd_color_02'] = '{{ data.alter_bd_color_02 }}';
			$colors['text_dark_07'] = '{{ data.text_dark_07 }}';
			$colors['text_link_02'] = '{{ data.text_link_02 }}';
			$colors['text_link_07'] = '{{ data.text_link_07 }}';
			$colors['text_link_blend'] = '{{ data.text_link_blend }}';
			$colors['alter_link_blend'] = '{{ data.alter_link_blend }}';
            $colors['alter_text_07'] = '{{ data.alter_text_07 }}';
            $colors['alter_text_04'] = '{{ data.alter_text_04 }}';
            $colors['alter_text_02'] = '{{ data.alter_text_02 }}';
		}
		return $colors;
	}
}


			
// Additional theme-specific fonts rules
// Attention! Don't forget setup fonts rules also in the theme.customizer.color-scheme.js
if (!function_exists('holy_church_customizer_add_theme_fonts')) {
	function holy_church_customizer_add_theme_fonts($fonts) {
		$rez = array();	
		foreach ($fonts as $tag => $font) {

			if (substr($font['font-family'], 0, 2) != '{{') {
				$rez[$tag.'_font-family'] 		= !empty($font['font-family']) && !holy_church_is_inherit($font['font-family'])
														? 'font-family:' . trim($font['font-family']) . ';' 
														: '';
				$rez[$tag.'_font-size'] 		= !empty($font['font-size']) && !holy_church_is_inherit($font['font-size'])
														? 'font-size:' . holy_church_prepare_css_value($font['font-size']) . ";"
														: '';
				$rez[$tag.'_line-height'] 		= !empty($font['line-height']) && !holy_church_is_inherit($font['line-height'])
														? 'line-height:' . trim($font['line-height']) . ";"
														: '';
				$rez[$tag.'_font-weight'] 		= !empty($font['font-weight']) && !holy_church_is_inherit($font['font-weight'])
														? 'font-weight:' . trim($font['font-weight']) . ";"
														: '';
				$rez[$tag.'_font-style'] 		= !empty($font['font-style']) && !holy_church_is_inherit($font['font-style'])
														? 'font-style:' . trim($font['font-style']) . ";"
														: '';
				$rez[$tag.'_text-decoration'] 	= !empty($font['text-decoration']) && !holy_church_is_inherit($font['text-decoration'])
														? 'text-decoration:' . trim($font['text-decoration']) . ";"
														: '';
				$rez[$tag.'_text-transform'] 	= !empty($font['text-transform']) && !holy_church_is_inherit($font['text-transform'])
														? 'text-transform:' . trim($font['text-transform']) . ";"
														: '';
				$rez[$tag.'_letter-spacing'] 	= !empty($font['letter-spacing']) && !holy_church_is_inherit($font['letter-spacing'])
														? 'letter-spacing:' . trim($font['letter-spacing']) . ";"
														: '';
				$rez[$tag.'_margin-top'] 		= !empty($font['margin-top']) && !holy_church_is_inherit($font['margin-top'])
														? 'margin-top:' . holy_church_prepare_css_value($font['margin-top']) . ";"
														: '';
				$rez[$tag.'_margin-bottom'] 	= !empty($font['margin-bottom']) && !holy_church_is_inherit($font['margin-bottom'])
														? 'margin-bottom:' . holy_church_prepare_css_value($font['margin-bottom']) . ";"
														: '';
			} else {
				$rez[$tag.'_font-family']		= '{{ data["'.$tag.'_font-family"] }}';
				$rez[$tag.'_font-size']			= '{{ data["'.$tag.'_font-size"] }}';
				$rez[$tag.'_line-height']		= '{{ data["'.$tag.'_line-height"] }}';
				$rez[$tag.'_font-weight']		= '{{ data["'.$tag.'_font-weight"] }}';
				$rez[$tag.'_font-style']		= '{{ data["'.$tag.'_font-style"] }}';
				$rez[$tag.'_text-decoration']	= '{{ data["'.$tag.'_text-decoration"] }}';
				$rez[$tag.'_text-transform']	= '{{ data["'.$tag.'_text-transform"] }}';
				$rez[$tag.'_letter-spacing']	= '{{ data["'.$tag.'_letter-spacing"] }}';
				$rez[$tag.'_margin-top']		= '{{ data["'.$tag.'_margin-top"] }}';
				$rez[$tag.'_margin-bottom']		= '{{ data["'.$tag.'_margin-bottom"] }}';
			}
		}
		return $rez;
	}
}


// Return CSS with custom colors and fonts
if (!function_exists('holy_church_customizer_get_css')) {

	function holy_church_customizer_get_css($colors=null, $fonts=null, $remove_spaces=true, $only_scheme='') {

		$css = array(
			'fonts' => '',
			'colors' => ''
		);
		
		// Theme fonts
		//---------------------------------------------
		if ($fonts === null) {
			$fonts = holy_church_get_theme_fonts();
		}
		
		if ($fonts) {

			// Make theme-specific fonts rules
			$fonts = holy_church_customizer_add_theme_fonts($fonts);

			$rez = array();
			$rez['fonts'] = <<<CSS

body {
	{$fonts['p_font-family']}
	{$fonts['p_font-size']}
	{$fonts['p_font-weight']}
	{$fonts['p_font-style']}
	{$fonts['p_line-height']}
	{$fonts['p_text-decoration']}
	{$fonts['p_text-transform']}
	{$fonts['p_letter-spacing']}
}
p, ul, ol, dl, blockquote, address {
	{$fonts['p_margin-top']}
	{$fonts['p_margin-bottom']}
}

h1 {
	{$fonts['h1_font-family']}
	{$fonts['h1_font-size']}
	{$fonts['h1_font-weight']}
	{$fonts['h1_font-style']}
	{$fonts['h1_line-height']}
	{$fonts['h1_text-decoration']}
	{$fonts['h1_text-transform']}
	{$fonts['h1_letter-spacing']}
	{$fonts['h1_margin-top']}
	{$fonts['h1_margin-bottom']}
}
h2 {
	{$fonts['h2_font-family']}
	{$fonts['h2_font-size']}
	{$fonts['h2_font-weight']}
	{$fonts['h2_font-style']}
	{$fonts['h2_line-height']}
	{$fonts['h2_text-decoration']}
	{$fonts['h2_text-transform']}
	{$fonts['h2_letter-spacing']}
	{$fonts['h2_margin-top']}
	{$fonts['h2_margin-bottom']}
}
h3 {
	{$fonts['h3_font-family']}
	{$fonts['h3_font-size']}
	{$fonts['h3_font-weight']}
	{$fonts['h3_font-style']}
	{$fonts['h3_line-height']}
	{$fonts['h3_text-decoration']}
	{$fonts['h3_text-transform']}
	{$fonts['h3_letter-spacing']}
	{$fonts['h3_margin-top']}
	{$fonts['h3_margin-bottom']}
}
h4 {
	{$fonts['h4_font-family']}
	{$fonts['h4_font-size']}
	{$fonts['h4_font-weight']}
	{$fonts['h4_font-style']}
	{$fonts['h4_line-height']}
	{$fonts['h4_text-decoration']}
	{$fonts['h4_text-transform']}
	{$fonts['h4_letter-spacing']}
	{$fonts['h4_margin-top']}
	{$fonts['h4_margin-bottom']}
}
h5 {
	{$fonts['h5_font-family']}
	{$fonts['h5_font-size']}
	{$fonts['h5_font-weight']}
	{$fonts['h5_font-style']}
	{$fonts['h5_line-height']}
	{$fonts['h5_text-decoration']}
	{$fonts['h5_text-transform']}
	{$fonts['h5_letter-spacing']}
	{$fonts['h5_margin-top']}
	{$fonts['h5_margin-bottom']}
}
h6 {
	{$fonts['h6_font-family']}
	{$fonts['h6_font-size']}
	{$fonts['h6_font-weight']}
	{$fonts['h6_font-style']}
	{$fonts['h6_line-height']}
	{$fonts['h6_text-decoration']}
	{$fonts['h6_text-transform']}
	{$fonts['h6_letter-spacing']}
	{$fonts['h6_margin-top']}
	{$fonts['h6_margin-bottom']}
}

input[type="text"],
input[type="number"],
input[type="email"],
input[type="tel"],
input[type="search"],
input[type="password"],
textarea,
textarea.wp-editor-area,
.select_container,
select,
.select_container select {
	{$fonts['input_font-family']}
	{$fonts['input_font-size']}
	{$fonts['input_font-weight']}
	{$fonts['input_font-style']}
	{$fonts['input_line-height']}
	{$fonts['input_text-decoration']}
	{$fonts['input_text-transform']}
	{$fonts['input_letter-spacing']}
}

.sc_button.sc_button_size_large,
.sc_button.sc_button_size_small,
.sc_item_button a, .sc_form button, .sc_button, .sc_price_link, .sc_action_item_link,
button,
input[type="button"],
input[type="reset"],
input[type="submit"],
.theme_button,
.gallery_preview_show .post_readmore,
.more-link,
.holy_church_tabs .holy_church_tabs_titles li a {
	{$fonts['button_font-family']}
	{$fonts['button_font-size']}
	{$fonts['button_font-weight']}
	{$fonts['button_font-style']}
	{$fonts['button_line-height']}
	{$fonts['button_text-decoration']}
	{$fonts['button_text-transform']}
	{$fonts['button_letter-spacing']}
}

.top_panel .slider_engine_revo .slide_title {
	{$fonts['h1_font-family']}
}

blockquote,
mark, ins,
.logo_text,
.post_price.price,
.theme_scroll_down {
	{$fonts['h5_font-family']}
}
blockquote > a, blockquote > p > a{
	{$fonts['p_font-family']}
}
blockquote > cite, blockquote > p > cite,
blockquote > cite a, blockquote > p > cite a{
	{$fonts['h5_font-family']}
}

.post_meta {
	{$fonts['info_font-family']}
	{$fonts['info_font-size']}
	{$fonts['info_font-weight']}
	{$fonts['info_font-style']}
	{$fonts['info_line-height']}
	{$fonts['info_text-decoration']}
	{$fonts['info_text-transform']}
	{$fonts['info_letter-spacing']}
	{$fonts['info_margin-top']}
	{$fonts['info_margin-bottom']}
}

.post-date, .rss-date 
.post_date, .post_meta_item, .post_counters_item,
.comments_list_wrap .comment_date,
.comments_list_wrap .comment_time,
.comments_list_wrap .comment_counters,
.top_panel .slider_engine_revo .slide_subtitle,
.logo_slogan,
fieldset legend,
figure figcaption,
.wp-caption .wp-caption-text,
.wp-caption .wp-caption-dd,
.wp-caption-overlay .wp-caption .wp-caption-text,
.wp-caption-overlay .wp-caption .wp-caption-dd,
.format-audio .post_featured .post_audio_author,
.author_bio .author_link,
.comments_list_wrap .comment_posted,
.comments_list_wrap .comment_reply {
}
.wp-block-calendar table,
.widget_area .post_item .post_title, .widget .post_item .post_title,
.search_wrap .search_results .post_meta_item,
.search_wrap .search_results .post_counters_item {
	{$fonts['p_font-family']}
}

.logo_text {
	{$fonts['logo_font-family']}
	{$fonts['logo_font-size']}
	{$fonts['logo_font-weight']}
	{$fonts['logo_font-style']}
	{$fonts['logo_line-height']}
	{$fonts['logo_text-decoration']}
	{$fonts['logo_text-transform']}
	{$fonts['logo_letter-spacing']}
}
.logo_footer_text {
	{$fonts['logo_font-family']}
}

.menu_main_nav_area {
	{$fonts['menu_font-size']}
	{$fonts['menu_line-height']}
}
.menu_main_nav > li,
.menu_main_nav > li > a {
	{$fonts['menu_font-family']}
	{$fonts['menu_font-weight']}
	{$fonts['menu_font-style']}
	{$fonts['menu_text-decoration']}
	{$fonts['menu_text-transform']}
	{$fonts['menu_letter-spacing']}
}
.menu_main_nav > li ul,
.menu_main_nav > li ul > li,
.menu_main_nav > li ul > li > a {
	{$fonts['submenu_font-family']}
	{$fonts['submenu_font-size']}
	{$fonts['submenu_font-weight']}
	{$fonts['submenu_font-style']}
	{$fonts['submenu_line-height']}
	{$fonts['submenu_text-decoration']}
	{$fonts['submenu_text-transform']}
	{$fonts['submenu_letter-spacing']}
}
.menu_mobile .menu_mobile_nav_area > ul > li,
.menu_mobile .menu_mobile_nav_area > ul > li > a {
	{$fonts['menu_font-family']}
}
.menu_mobile .menu_mobile_nav_area > ul > li li,
.menu_mobile .menu_mobile_nav_area > ul > li li > a {
	{$fonts['submenu_font-family']}
}


/* Custom Headers */
.sc_layouts_row .sc_button_wrap .sc_button {
	{$fonts['button_font-family']}
	{$fonts['button_font-size']}
	{$fonts['button_font-weight']}
	{$fonts['button_font-style']}
	{$fonts['button_line-height']}
	{$fonts['button_text-decoration']}
	{$fonts['button_text-transform']}
	{$fonts['button_letter-spacing']}
}
.sc_layouts_menu_nav > li,
.sc_layouts_menu_nav > li > a {
	{$fonts['menu_font-family']}
	{$fonts['menu_font-weight']}
	{$fonts['menu_font-style']}
	{$fonts['menu_text-decoration']}
	{$fonts['menu_text-transform']}
	{$fonts['menu_letter-spacing']}
}
.sc_layouts_menu_popup .sc_layouts_menu_nav > li,
.sc_layouts_menu_popup .sc_layouts_menu_nav > li > a,
.sc_layouts_menu_nav > li ul,
.sc_layouts_menu_nav > li ul > li,
.sc_layouts_menu_nav > li ul > li > a {
	{$fonts['submenu_font-family']}
	{$fonts['submenu_font-size']}
	{$fonts['submenu_font-weight']}
	{$fonts['submenu_font-style']}
	{$fonts['submenu_line-height']}
	{$fonts['submenu_text-decoration']}
	{$fonts['submenu_text-transform']}
	{$fonts['submenu_letter-spacing']}
}

.post_audio .mejs-container *,
.mejs-container *{
	{$fonts['p_font-family']}
}
.comments_list_wrap .comment_reply,
.sc_item_subtitle{
	{$fonts['button_font-family']}
}

.sticky .label_sticky,
.widget_product_tag_cloud a, .widget_tag_cloud a, .wp-block-tag-cloud a,
.sidebar .widget .widget_title, .sidebar .widget .widgettitle{
	{$fonts['info_font-family']}
}


CSS;
			$rez = apply_filters('holy_church_filter_get_css', $rez, false, $fonts, '');
			$css['fonts'] = $rez['fonts'];

			
			// Border radius
			//--------------------------------------
			$rad = holy_church_get_border_radius();
			$rad50 = ' '.$rad != ' 0' ? '50%' : 0;
			$css['fonts'] .= <<<CSS

/* Buttons */
button,
input[type="button"],
input[type="reset"],
input[type="submit"],
.theme_button,
.post_item .more-link,
.gallery_preview_show .post_readmore,

/* Fields */
input[type="text"],
input[type="number"],
input[type="email"],
input[type="tel"],
input[type="password"],
input[type="search"],
select,
.select_container,
textarea,

/* Search fields */
.widget_search .search-field,
.woocommerce.widget_product_search .search_field,
.widget_display_search #bbp_search,
#bbpress-forums #bbp-search-form #bbp_search,

/* Comment fields */
.comments_wrap .comments_field input,
.comments_wrap .comments_field textarea,

/* Tags cloud */
.widget_product_tag_cloud a,
.widget_tag_cloud a,
.wp-block-tag-cloud a {
	-webkit-border-radius: {$rad};
	    -ms-border-radius: {$rad};
			border-radius: {$rad};
}
.select_container:before {
	-webkit-border-radius: 0 {$rad} {$rad} 0;
	    -ms-border-radius: 0 {$rad} {$rad} 0;
			border-radius: 0 {$rad} {$rad} 0;
}
textarea.wp-editor-area {
	-webkit-border-radius: 0 0 {$rad} {$rad};
	    -ms-border-radius: 0 0 {$rad} {$rad};
			border-radius: 0 0 {$rad} {$rad};
}

/* Radius 50% or 0 */
.widget li a img {
	-webkit-border-radius: {$rad50};
	    -ms-border-radius: {$rad50};
			border-radius: {$rad50};
}

CSS;
		}


		// Theme colors
		//--------------------------------------
		if ($colors !== false) {
			$schemes = empty($only_scheme) ? array_keys(holy_church_get_list_schemes()) : array($only_scheme);
	
			if (count($schemes) > 0) {
				$rez = array();
				foreach ($schemes as $scheme) {
					// Prepare colors
					if (empty($only_scheme)) $colors = holy_church_get_scheme_colors($scheme);
	
					// Make theme-specific colors and tints
					$colors = holy_church_customizer_add_theme_colors($colors);
			
					// Make styles
					$rez['colors'] = <<<CSS

/* Common tags */
h1, h2, h3, h4, h5, h6,
h1 a, h2 a, h3 a, h4 a, h5 a, h6 a{
	color: {$colors['text_dark']};
}
h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover{
	color: {$colors['text_link']};
}

b, strong, mark, ins {	
	color: {$colors['text_dark']};
}
s, strike, del {	
	color: {$colors['text_light']};
}

code {
	color: {$colors['alter_text']};
	background-color: {$colors['alter_bg_color']};
	border-color: {$colors['alter_bd_color']};
}
code a {
	color: {$colors['alter_link']};
}
code a:hover {
	color: {$colors['alter_hover']};
}

a {
	color: {$colors['text_link']};
}
a:hover {
	color: {$colors['text_hover']};
}

blockquote {
	color: {$colors['text_dark']};
}
blockquote:before {
	color: {$colors['text_dark']};
}
blockquote a {
	color: {$colors['text']};
}
blockquote a:hover {
	color: {$colors['text_link']};
}
blockquote > a, blockquote > p > a{
	color: {$colors['text']};
}
blockquote > cite, blockquote > p > cite,
blockquote > cite a, blockquote > p > cite a{
	color: {$colors['text_link']};
}
blockquote > a:hover, blockquote > p > a:hover,
blockquote > cite a:hover, blockquote > p > cite a:hover{
	color: {$colors['text_hover']};
}
blockquote > cite:before, blockquote > p > cite:before{
	background-color: {$colors['text_link']};
}
.wp-block-pullquote.has-background cite {
	color: {$colors['inverse_text']};
}
.wp-block-pullquote.has-background blockquote > cite:before {
	background-color: {$colors['inverse_text']};
}

table th, table th + th, table td + th  {
	border-color: {$colors['bg_color_02']};
}
table td, table th + td, table td + td {
	color: {$colors['text']};
	border-color: {$colors['alter_bd_color']};
}
table th,
table th a{
	background-color: {$colors['text_link']};
	color: {$colors['inverse_dark']};
}

.comment_text table > tbody > tr th {
	background-color: {$colors['text_link']};
}
table > tbody > tr th a {
	color: {$colors['inverse_link']};
}
table > tbody > tr th a:hover {
	color: {$colors['inverse_hover']};
}
table th a:hover {
	color: {$colors['bg_color']};
}
table > tbody > tr th a:hover {
	color: {$colors['text_dark']};
}

table:not(#wp-calendar) thead th, 
table:not(#wp-calendar) thead td, 
table:not(#wp-calendar) tbody th{
	{$fonts['h1_font-family']}
}

hr {
	border-color: {$colors['bd_color']};
}
figure figcaption,
.wp-caption .wp-caption-text,
.wp-caption .wp-caption-dd,
.wp-caption-overlay .wp-caption .wp-caption-text,
.wp-caption-overlay .wp-caption .wp-caption-dd {
	color: {$colors['inverse_text']};
	background-color: {$colors['alter_text_07']};
}
figure figcaption b, figure figcaption ins,
figure figcaption em, figure figcaption strong,
figure figcaption mark {
	color: {$colors['inverse_text']};
}

ul > li:before {
	color: {$colors['text_link']};
}

button[disabled],
input[type="submit"][disabled],
input[type="button"][disabled] {
    background-color: {$colors['text_light']} !important;
    color: {$colors['text']} !important;
}

/* Form fields */
fieldset {
	border-color: {$colors['bd_color']};
}
fieldset legend {
	color: {$colors['text_dark']};
	background-color: {$colors['bg_color']};
}

::-webkit-input-placeholder { color: {$colors['text_light']}; }
::-moz-placeholder          { color: {$colors['text_light']}; }
:-ms-input-placeholder      { color: {$colors['text_light']}; }

input[type="text"],
input[type="number"],
input[type="email"],
input[type="tel"],
input[type="search"],
input[type="password"],
.select_container,
.select2-container .select2-choice,
textarea,
textarea.wp-editor-area,
.select2-container.select2-container--default span.select2-choice,
.select2-container.select2-container--default span.select2-selection  {
	color: {$colors['input_text']};
	border-color: {$colors['input_bd_color']};
	background-color: {$colors['input_bg_color']};
}
input[type="text"]:focus,
input[type="number"]:focus,
input[type="email"]:focus,
input[type="tel"]:focus,
input[type="search"]:focus,
input[type="password"]:focus,
.select_container:hover,
select option:hover,
select option:focus,
.select2-container .select2-choice:hover,
textarea:focus,
textarea.wp-editor-area:focus {
	color: {$colors['input_dark']};
	border-color: {$colors['input_bd_hover']};
	background-color: {$colors['input_bg_hover']};
}
.select_container:before {
	color: {$colors['input_text']};
	background-color: {$colors['input_bg_color']};
}
.select_container:focus:before,
.select_container:hover:before {
	color: {$colors['input_dark']};
	background-color: {$colors['input_bg_hover']};
}
.select_container:after {
	color: {$colors['input_text']};
}
.select_container:focus:after,
.select_container:hover:after {
	color: {$colors['input_dark']};
}
.select_container select {
	color: {$colors['input_text']};
}
.select_container select:focus {
	color: {$colors['input_dark']};
	border-color: {$colors['input_bd_hover']};
}

.widget_search form:after,
.woocommerce.widget_product_search form:after,
.widget_display_search form:after,
#bbpress-forums #bbp-search-form:after {
	color: {$colors['input_text']};
}
.widget_search form:hover:after,
.woocommerce.widget_product_search form:hover:after,
.widget_display_search form:hover:after,
#bbpress-forums #bbp-search-form:hover:after {
	color: {$colors['input_dark']};
}
.widget_search input[type="search"],
.widget_search input[type="search"]:focus{
	background-color: {$colors['bg_color']};
	border-color: {$colors['bd_color']};
}
input[type="radio"] + label:before,
input[type="checkbox"] + label:before {
	border-color: {$colors['input_bd_color']};
	background-color: {$colors['input_bg_color']};
}
input[type="checkbox"]:checked + label:before {
	color: {$colors['text_link']};
}

button,
input[type="reset"],
input[type="submit"],
input[type="button"] {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}
input[type="submit"]:hover,
input[type="reset"]:hover,
input[type="button"]:hover,
button:hover,
input[type="submit"]:focus,
input[type="reset"]:focus,
input[type="button"]:focus,
button:focus {
	color: {$colors['inverse_hover']};
	background-color: {$colors['text_hover']};
}

.wp-editor-container input[type="button"] {
	background-color: {$colors['alter_bg_color']};
	border-color: {$colors['alter_bd_color']};
	color: {$colors['alter_dark']};
	-webkit-box-shadow: 0 1px 0 0 {$colors['alter_bd_hover']};
	    -ms-box-shadow: 0 1px 0 0 {$colors['alter_bd_hover']};
			box-shadow: 0 1px 0 0 {$colors['alter_bd_hover']};	
}
.wp-editor-container input[type="button"]:hover,
.wp-editor-container input[type="button"]:focus {
	background-color: {$colors['alter_bg_hover']};
	border-color: {$colors['alter_bd_hover']};
	color: {$colors['alter_link']};
}

.select2-results {
	color: {$colors['input_text']};
	border-color: {$colors['input_bd_hover']};
	background: {$colors['input_bg_color']};
}
.select2-results .select2-highlighted {
	color: {$colors['input_dark']};
	background: {$colors['input_bg_hover']};
}


/* WP Standard classes */
.sticky {
    background-color: {$colors['alter_bd_color']};
}
.sticky .label_sticky {
	color: {$colors['inverse_text']};
	background-color: {$colors['text_link']};
}
	

/* Page */
body {
	color: {$colors['text']};
	background-color: {$colors['bg_color']};
}
#page_preloader,
.scheme_self.header_position_under .page_content_wrap,
.page_wrap {
	background-color: {$colors['bg_color']};
}
.preloader_wrap > div {
	background-color: {$colors['text_link']};
}

/* Header */
.scheme_self.top_panel.with_bg_image:before {
	background-color: {$colors['bg_color_07']};
}
.scheme_self.top_panel .slider_engine_revo .slide_subtitle,
.top_panel .slider_engine_revo .slide_subtitle {
	color: {$colors['text_link']};
}
.top_panel_default .top_panel_title,
.scheme_self.top_panel_default .top_panel_title {
	background-color: {$colors['alter_bg_hover']};
}
.sc_layouts_title_caption,
.sc_layouts_title_caption b{
	color: {$colors['inverse_dark']};
}


/* Tabs */
.holy_church_tabs .holy_church_tabs_titles li a {
	color: {$colors['alter_dark']};
	background-color: {$colors['alter_bg_color']};
}
.holy_church_tabs .holy_church_tabs_titles li a:hover {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}
.holy_church_tabs .holy_church_tabs_titles li.ui-state-active a {
	color: {$colors['bg_color']};
	background-color: {$colors['text_dark']};
}

/* Post layouts */
.post_item {
	color: {$colors['text']};
}
.post_info .post_info_item{
	color: {$colors['text_link']};
}
.post_meta,
.post_meta_item,
.post_meta_item a,
.post_meta_item:before,
.post_meta_item:hover:before,
.post_date a,
.post_date:before,
.post_info .post_info_item a,
.post_info_counters .post_counters_item,
.post_counters .socials_share .socials_caption:before,
.post_counters .socials_share .socials_caption:hover:before {
	color: {$colors['text']};
}
.post_date a:hover,
a.post_meta_item:hover,
.post_meta_item a:hover,
.post_info .post_info_item a:hover,
.post_info_counters .post_counters_item:hover {
	color: {$colors['text_dark']};
}
.top_panel_title .post_date a:hover,
.top_panel_title a.post_meta_item:hover,
.top_panel_title .post_meta_item a:hover,
.top_panel_title .post_info .post_info_item a:hover,
.top_panel_title .post_info_counters .post_counters_item:hover {
	color: {$colors['text_link']};
}
.post_item .post_title a:hover,
.post_item .post_title a:hover em,
.post_item .post_title a:hover b {
	color: {$colors['text_link']};
}

.widget_recent_posts .post_info .post_info_item{
	color: {$colors['text']};
}
.widget_recent_posts .post_info .post_info_item a{
	color: {$colors['text_link']};
}
.widget_recent_posts .post_info .post_info_item a:hover{
	color: {$colors['text_hover']};
}

.post_meta_item.post_categories,
.post_meta_item.post_categories a {
	color: {$colors['text_link']};
}
.post_meta_item.post_categories a:hover {
	color: {$colors['text_hover']};
}

.post_meta_item .socials_share .social_items {
	background-color: {$colors['bg_color']};
}
.post_meta_item .social_items,
.post_meta_item .social_items:before {
	background-color: {$colors['bg_color']};
	border-color: {$colors['bd_color']};
	color: {$colors['text_light']};
}

.post_layout_excerpt + .post_layout_excerpt {
	border-color: {$colors['bd_color']};
}
.post_layout_classic {
	border-color: {$colors['bd_color']};
}

.scheme_self.gallery_preview:before {
	background-color: {$colors['bg_color']};
}
.scheme_self.gallery_preview {
	color: {$colors['text']};
}

/* Post Formats */
.format-audio .post_featured .post_audio_author {
	color: {$colors['text_link']};
}
.format-audio .post_featured.without_thumb .post_audio {
	border-color: {$colors['bd_color']};
}
.format-audio .post_featured.without_thumb .post_audio_title,
.without_thumb .mejs-controls .mejs-currenttime,
.without_thumb .mejs-controls .mejs-duration {
	color: {$colors['text_dark']};
}

.mejs-container,
.mejs-container .mejs-controls,
.mejs-embed,
.mejs-embed body {
	background: {$colors['text_dark_07']};
}

body .trx_addons_audio_player .mejs-container .mejs-controls .mejs-time,
.mejs-controls .mejs-button{
	color: {$colors['inverse_link']};
}
.mejs-controls .mejs-time-rail .mejs-time-current,
.mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current {
	color: {$colors['inverse_link']};
	background: {$colors['text_link']};
}
.mejs-controls .mejs-button:hover {
	color: {$colors['text_link']};
}
.mejs-controls .mejs-time-rail .mejs-time-total,
.mejs-controls .mejs-time-rail .mejs-time-loaded,
.mejs-container .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-total {
	background: {$colors['bg_color_02']};
}

.format-aside .post_content_inner {
	color: {$colors['alter_dark']};
	background-color: {$colors['alter_bg_color']};
}

.format-link .post_content_inner,
.format-status .post_content_inner {
	color: {$colors['text_dark']};
}

.format-chat p > b,
.format-chat p > strong {
	color: {$colors['text_dark']};
}

.post_layout_chess .post_content_inner:after {
	background: linear-gradient(to top, {$colors['bg_color']} 0%, {$colors['bg_color_0']} 100%) no-repeat scroll right top / 100% 100% {$colors['bg_color_0']};
}
.post_layout_chess_1 .post_meta:before {
	background-color: {$colors['bd_color']};
}

/* Pagination */
.nav-links-old {
	color: {$colors['text_dark']};
}
.nav-links-old a:hover {
	color: {$colors['text_dark']};
	border-color: {$colors['text_dark']};
}

.page_links > a,
.comments_pagination .page-numbers,
.nav-links .page-numbers {
	color: {$colors['text_dark']};
	background-color: {$colors['input_bg_color']};
}
.page_links > a:hover,
.page_links > span:not(.page_links_title),
.comments_pagination a.page-numbers:hover,
.comments_pagination .page-numbers.current,
.nav-links a.page-numbers:hover,
.nav-links .page-numbers.current {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}

/* Single post */
.post_item_single .post_header .post_date {
	color: {$colors['text_light']};
}
.post_item_single .post_header .post_categories,
.post_item_single .post_header .post_categories a {
	color: {$colors['text_link']};
}
.post_item_single .post_header .post_meta_item,
.post_item_single .post_header .post_meta_item:before,
.post_item_single .post_header .post_meta_item:hover:before,
.post_item_single .post_header .post_meta_item a,
.post_item_single .post_header .post_meta_item a:before,
.post_item_single .post_header .post_meta_item a:hover:before,
.post_item_single .post_header .post_meta_item .socials_caption,
.post_item_single .post_header .post_meta_item .socials_caption:before,
.post_item_single .post_header .post_edit a {
	color: {$colors['text_light']};
}
.post_item_single .post_meta_item:hover,
.post_item_single .post_meta_item > a:hover,
.post_item_single .post_meta_item .socials_caption:hover,
.post_item_single .post_edit a:hover {
	color: {$colors['text_hover']};
}
.post_item_single .post_content .post_meta_label,
.post_item_single .post_content .post_meta_item:hover .post_meta_label {
	color: {$colors['text']};
}
.post_item_single .post_content .post_tags,
.post_item_single .post_content .post_tags a {
	color: {$colors['text_link']};
}
.post_item_single .post_content .post_tags a:hover {
	color: {$colors['text_hover']};
}
.post_item_single .post_content .post_meta .post_share .social_item a {
	color: {$colors['inverse_link']} !important;
	background-color: {$colors['alter_hover']};
}
.post_item_single .post_content .post_meta .post_share .social_item a:hover {
	color: {$colors['inverse_hover']} !important;
	background-color: {$colors['text_link']};
}

.post-password-form input[type="submit"] {
	border-color: {$colors['text_dark']};
}
.post-password-form input[type="submit"]:hover,
.post-password-form input[type="submit"]:focus {
	color: {$colors['bg_color']};
}

/* Single post navi */
.nav-links-single .nav-links {
	border-color: {$colors['bd_color']};
}
.nav-links-single .nav-links a .meta-nav {
	color: {$colors['text_light']};
}
.nav-links-single .nav-links a .post_date {
	color: {$colors['text_light']};
}
.nav-links-single .nav-links a:hover .meta-nav,
.nav-links-single .nav-links a:hover .post_date {
	color: {$colors['text_dark']};
}
.nav-links-single .nav-links a:hover .post-title {
	color: {$colors['text_link']};
}

/* Author info */
.author_info {
	color: {$colors['text']};
	background-color: {$colors['alter_bg_color']};
}
.author_info .author_title {
	color: {$colors['text_dark']};
}
.author_info a {
	color: {$colors['text_dark']};
}
.author_info a:hover {
	color: {$colors['text_link']};
}

/* Related posts */
.related_wrap {
	border-color: {$colors['bd_color']};
}
.related_wrap .related_item_style_1 .post_header {
	background-color: {$colors['bg_color_07']};
}
.related_wrap .related_item_style_1:hover .post_header {
	background-color: {$colors['bg_color']};
}
.related_wrap .related_item_style_1 .post_date a {
	color: {$colors['text']};
}
.related_wrap .related_item_style_1:hover .post_date a {
	color: {$colors['text_light']};
}
.related_wrap .related_item_style_1:hover .post_date a:hover {
	color: {$colors['text_dark']};
}

/* Comments */
.comments_list_wrap,
.comments_list_wrap > ul {
	border-color: {$colors['bd_color']};
}
.comments_list_wrap li + li,
.comments_list_wrap li ul {
	border-color: {$colors['bd_color']};
}
.comments_list_wrap .comment_info {
	color: {$colors['text_dark']};
}
.comments_list_wrap .comment_counters a {
	color: {$colors['text_link']};
}
.comments_list_wrap .comment_counters a:before {
	color: {$colors['text_link']};
}
.comments_list_wrap .comment_counters a:hover:before,
.comments_list_wrap .comment_counters a:hover {
	color: {$colors['text_hover']};
}
.comments_list_wrap .comment_text {
	color: {$colors['text']};
}
.comments_list_wrap .comment_reply a {
	color: {$colors['text_link']};
}
.comments_list_wrap .comment_reply a:hover {
	color: {$colors['text_hover']};
}
.comments_form_wrap {
	border-color: {$colors['bd_color']};
}
.comments_wrap .comments_notes {
	color: {$colors['text_light']};
}


/* Page 404 */
.post_item_404 .page_title {
	color: {$colors['text_light']};
}
.post_item_404 .page_description {
	color: {$colors['text_link']};
}
.post_item_404 .go_home {
	border-color: {$colors['text_dark']};
}

/* Sidebar */
.scheme_self.sidebar .sidebar_inner {
	background-color: {$colors['bg_color']};
	color: {$colors['alter_text']};
}
.sidebar_inner .widget + .widget {
	border-color: {$colors['bd_color']};
}
.scheme_self.sidebar .widget + .widget {
	border-color: {$colors['alter_bd_color']};
}
.scheme_self.sidebar h1, .scheme_self.sidebar h2, .scheme_self.sidebar h3, .scheme_self.sidebar h4, .scheme_self.sidebar h5, .scheme_self.sidebar h6,
.scheme_self.sidebar h1 a, .scheme_self.sidebar h2 a, .scheme_self.sidebar h3 a, .scheme_self.sidebar h4 a, .scheme_self.sidebar h5 a, .scheme_self.sidebar h6 a {
	color: {$colors['alter_dark']};
}
.scheme_self.sidebar h1 a:hover, .scheme_self.sidebar h2 a:hover, .scheme_self.sidebar h3 a:hover, .scheme_self.sidebar h4 a:hover, .scheme_self.sidebar h5 a:hover, .scheme_self.sidebar h6 a:hover {
	color: {$colors['alter_link']};
}

.sidebar .widget .widget_title, 
.sidebar .widget .widgettitle{
    color: {$colors['text_link']};    
    background-color: {$colors['alter_bd_color']};
}
.sidebar li a{
    color: {$colors['text']};    
}
.sidebar li a:hover{
    color: {$colors['text_link']};    
}


/* Widgets */
.widget li:before {
	background-color: {$colors['text_link']};
}
.scheme_self.sidebar li:before {
	background-color: {$colors['alter_link']};
}
.scheme_self.sidebar a {
	color: {$colors['alter_link']};
}
.scheme_self.sidebar a:hover {
	color: {$colors['alter_hover']};
}
.scheme_self.sidebar li > a,
.scheme_self.sidebar .post_title > a {
	color: {$colors['alter_dark']};
}
.scheme_self.sidebar li > a:hover,
.scheme_self.sidebar .post_title > a:hover {
	color: {$colors['alter_link']};
}

/* Archive */
.scheme_self.sidebar .widget_archive li {
	color: {$colors['alter_dark']};
}

/* Calendar */
.widget_calendar caption,
.wp-block-calendar caption{
	{$fonts['button_font-family']}
}
.widget_calendar caption,
.wp-block-calendar caption{
	color: {$colors['text_dark']};
}
.widget_calendar tbody td a, .widget_calendar th,
.wp-block-calendar tbody td a, .wp-block-calendar th {
	color: {$colors['text']};
}
.scheme_self.sidebar .widget_calendar caption,
.scheme_self.sidebar .widget_calendar tbody td a,
.scheme_self.sidebar .widget_calendar th,
.scheme_self.sidebar .wp-block-calendar caption,
.scheme_self.sidebar .wp-block-calendar tbody td a,
.scheme_self.sidebar .wp-block-calendar th {
	color: {$colors['alter_dark']};
}
.widget_calendar tbody td, .wp-block-calendar tbody td {
	color: {$colors['text']} !important;
}
.scheme_self.sidebar .widget_calendar tbody td,
.scheme_self.sidebar .wp-block-calendar tbody td {
	color: {$colors['alter_text']} !important;
}
.widget_calendar tbody td a:hover,
.wp-block-calendar tbody td a:hover {
	color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_calendar tbody td a:hover,
.scheme_self.sidebar .wp-block-calendar tbody td a:hover {
	color: {$colors['alter_link']};
}
.widget_calendar tbody td a:after,
.wp-block-calendar tbody td a:after {
	background-color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_calendar tbody td a:after,
.scheme_self.sidebar .wp-block-calendar tbody td a:after {
	background-color: {$colors['alter_link']};
}
.widget_calendar td#today, .wp-block-calendar td#today {
	color: {$colors['inverse_text']} !important;
}
.widget_calendar td#today a, .wp-block-calendar td#today a {
	color: {$colors['inverse_link']};
}
.widget_calendar td#today a:hover, .wp-block-calendar td#today a:hover {
	color: {$colors['inverse_hover']};
}
.widget_calendar td#today:before, .wp-block-calendar td#today:before {
	background-color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_calendar td#today:before,
.scheme_self.sidebar .wp-block-calendar td#today:before {
	background-color: {$colors['alter_link']};
}
.widget_calendar td#today a:after, .wp-block-calendar td#today a:after {
	background-color: {$colors['inverse_link']};
}
.widget_calendar td#today a:hover:after, .wp-block-calendar td#today a:hover:after {
	background-color: {$colors['inverse_hover']};
}
.widget_calendar #prev a, .widget_calendar #next a,
.wp-block-calendar #prev a, .wp-block-calendar #next a,
.widget_calendar .wp-calendar-nav-prev a, .widget_calendar .wp-calendar-nav-next a,
.wp-block-calendar .wp-calendar-nav-prev a, .wp-block-calendar .wp-calendar-nav-next a {
	color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_calendar #prev a, .scheme_self.sidebar .widget_calendar #next a,
.scheme_self.sidebar .wp-block-calendar #prev a, .scheme_self.sidebar .wp-block-calendar #next a,
.scheme_self.sidebar .widget_calendar .wp-calendar-nav-prev a, .scheme_self.sidebar .widget_calendar .wp-calendar-nav-next a,
.scheme_self.sidebar .wp-block-calendar .wp-calendar-nav-prev a, .scheme_self.sidebar .wp-block-calendar .wp-calendar-nav-next a {
	color: {$colors['alter_link']};
}
.widget_calendar #prev a:hover, .widget_calendar #next a:hover,
.wp-block-calendar #prev a:hover, .wp-block-calendar #next a:hover,
.widget_calendar .wp-calendar-nav-prev a:hover, .widget_calendar .wp-calendar-nav-next a:hover,
.wp-block-calendar .wp-calendar-nav-prev a:hover, .wp-block-calendar .wp-calendar-nav-next a:hover {
	color: {$colors['text_hover']};
}
.scheme_self.sidebar .widget_calendar #prev a:hover, .scheme_self.sidebar .widget_calendar #next a:hover,
.scheme_self.sidebar .wp-block-calendar #prev a:hover, .scheme_self.sidebar .wp-block-calendar #next a:hover,
.scheme_self.sidebar .widget_calendar .wp-calendar-nav-prev a:hover, .scheme_self.sidebar .widget_calendar .wp-calendar-nav-next a:hover,
.scheme_self.sidebar .wp-block-calendar .wp-calendar-nav-prev a:hover, .scheme_self.sidebar .wp-block-calendar .wp-calendar-nav-next a:hover {
	color: {$colors['alter_hover']};
}
.widget_calendar td#prev a:before, .widget_calendar td#next a:before,
.wp-block-calendar td#prev a:before, .wp-block-calendar td#next a:before,
.widget_calendar .wp-calendar-nav-prev a:before, .widget_calendar .wp-calendar-nav-next a:before,
.wp-block-calendar .wp-calendar-nav-prev a:before, .wp-block-calendar .wp-calendar-nav-next a:before {
	background-color: {$colors['bg_color']};
}
.scheme_self.sidebar .widget_calendar td#prev a:before, .scheme_self.sidebar .widget_calendar td#next a:before,
.scheme_self.sidebar .wp-block-calendar td#prev a:before, .scheme_self.sidebar .wp-block-calendar td#next a:before,
.scheme_self.sidebar .widget_calendar .wp-calendar-nav-prev a:before, .scheme_self.sidebar .widget_calendar .wp-calendar-nav-next a:before,
.scheme_self.sidebar .wp-block-calendar .wp-calendar-nav-prev a:before, .scheme_self.sidebar .wp-block-calendar .wp-calendar-nav-next a:before,
.scheme_self.footer_wrap .widget_calendar td#prev a:before, .scheme_self.footer_wrap .widget_calendar td#next a:before,
.scheme_self.footer_wrap .wp-block-calendar td#prev a:before, .scheme_self.footer_wrap .wp-block-calendar td#next a:before,
.scheme_self.footer_wrap .widget_calendar .wp-calendar-nav-prev a:before, .scheme_self.footer_wrap .widget_calendar .wp-calendar-nav-next a:before,
.scheme_self.footer_wrap .wp-block-calendar .wp-calendar-nav-prev a:before, .scheme_self.footer_wrap .wp-block-calendar .wp-calendar-nav-next a:before {
	background-color: {$colors['alter_bg_color']};
}

.sc_table table tr:first-child th,
.sc_table table tr:first-child td {
    background-color: {$colors['text_link']};
}

/* Categories */
.widget_archive li,
.widget_categories li {
	color: {$colors['text_dark']};
}
.widget_archive li a,
.widget_categories li a{
	color: {$colors['text']};
}
.widget_archive li a:hover,
.widget_categories li a:hover{
	color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_categories li {
	color: {$colors['alter_dark']};
}

/* Tag cloud */
.widget_product_tag_cloud a,
.widget_tag_cloud a,
.wp-block-tag-cloud a {
	color: {$colors['inverse_text']};
	background-color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_product_tag_cloud a,
.scheme_self.sidebar .widget_tag_cloud a,
.scheme_self.sidebar .wp-block-tag-cloud a,
.scheme_self.footer_wrap .widget_product_tag_cloud a,
.scheme_self.footer_wrap .widget_tag_cloud a,
.scheme_self.footer_wrap .wp-block-tag-cloud a {
	color: {$colors['inverse_text']};
	background-color: {$colors['text_link']};
}
.widget_product_tag_cloud a:hover,
.widget_tag_cloud a:hover,
.wp-block-tag-cloud a:hover {
	color: {$colors['text']} !important;
	background-color: {$colors['bg_color']};
}
.scheme_self.sidebar .widget_product_tag_cloud a:hover,
.scheme_self.sidebar .widget_tag_cloud a:hover,
.scheme_self.sidebar .wp-block-tag-cloud a:hover,
.scheme_self.footer_wrap .widget_product_tag_cloud a:hover,
.scheme_self.footer_wrap .widget_tag_cloud a:hover,
.scheme_self.footer_wrap .wp-block-tag-cloud a:hover {
	color: {$colors['text']} !important;
	background-color: {$colors['bg_color']};
}

/* RSS */
.widget_rss .widget_title a{
	color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_rss .widget_title a:first-child {
	color: {$colors['alter_link']};
}
.widget_rss .widget_title a:hover {
	color: {$colors['text_hover']};
}
.scheme_self.sidebar .widget_rss .widget_title a:first-child:hover {
	color: {$colors['alter_hover']};
}
.widget_rss .rss-date {
	color: {$colors['text_link']};
}
.scheme_self.sidebar .widget_rss .rss-date {
	color: {$colors['alter_light']};
}
.footer_wrap .widget_rss .widget_title,
.footer_wrap .widget_rss .widget_title a {
	color: {$colors['alter_dark']};
}
.footer_wrap .widget_rss .widget_title a:hover {
	color: {$colors['text_hover']};
}
/* Footer */
.scheme_self.footer_wrap,
.footer_wrap .scheme_self.vc_row {
	background-color: {$colors['alter_bg_color']};
	color: {$colors['alter_text']};
}
.scheme_self.footer_wrap .widget,
.scheme_self.footer_wrap .sc_content .wpb_column,
.footer_wrap .scheme_self.vc_row .widget,
.footer_wrap .scheme_self.vc_row .sc_content .wpb_column {
	border-color: {$colors['alter_bd_color']};
}
.scheme_self.footer_wrap h1, .scheme_self.footer_wrap h2, .scheme_self.footer_wrap h3,
.scheme_self.footer_wrap h4, .scheme_self.footer_wrap h5, .scheme_self.footer_wrap h6,
.scheme_self.footer_wrap h1 a, .scheme_self.footer_wrap h2 a, .scheme_self.footer_wrap h3 a,
.scheme_self.footer_wrap h4 a, .scheme_self.footer_wrap h5 a, .scheme_self.footer_wrap h6 a,
.footer_wrap .scheme_self.vc_row h1, .footer_wrap .scheme_self.vc_row h2, .footer_wrap .scheme_self.vc_row h3,
.footer_wrap .scheme_self.vc_row h4, .footer_wrap .scheme_self.vc_row h5, .footer_wrap .scheme_self.vc_row h6,
.footer_wrap .scheme_self.vc_row h1 a, .footer_wrap .scheme_self.vc_row h2 a, .footer_wrap .scheme_self.vc_row h3 a,
.footer_wrap .scheme_self.vc_row h4 a, .footer_wrap .scheme_self.vc_row h5 a, .footer_wrap .scheme_self.vc_row h6 a {
	color: {$colors['alter_dark']};
}
.scheme_self.footer_wrap h1 a:hover, .scheme_self.footer_wrap h2 a:hover, .scheme_self.footer_wrap h3 a:hover,
.scheme_self.footer_wrap h4 a:hover, .scheme_self.footer_wrap h5 a:hover, .scheme_self.footer_wrap h6 a:hover,
.footer_wrap .scheme_self.vc_row h1 a:hover, .footer_wrap .scheme_self.vc_row h2 a:hover, .footer_wrap .scheme_self.vc_row h3 a:hover,
.footer_wrap .scheme_self.vc_row h4 a:hover, .footer_wrap .scheme_self.vc_row h5 a:hover, .footer_wrap .scheme_self.vc_row h6 a:hover {
	color: {$colors['alter_link']};
}
.scheme_self.footer_wrap .widget li:before,
.footer_wrap .scheme_self.vc_row .widget li:before {
	background-color: {$colors['alter_link']};
}
.scheme_self.footer_wrap a,
.footer_wrap .scheme_self.vc_row a {
	color: {$colors['text']};
}
.scheme_self.footer_wrap a:hover,
.footer_wrap .scheme_self.vc_row a:hover {
	color: {$colors['text_link']};
}

.footer_logo_inner {
	border-color: {$colors['alter_bd_color']};
}
.footer_logo_inner:after {
	background-color: {$colors['alter_text']};
}
.footer_socials_inner .social_item .social_icons {
	border-color: {$colors['alter_text']};
	color: {$colors['alter_text']};
}
.footer_socials_inner .social_item .social_icons:hover {
	border-color: {$colors['alter_dark']};
	color: {$colors['alter_dark']};
}
.menu_footer_nav_area ul li a {
	color: {$colors['alter_dark']};
}
.menu_footer_nav_area ul li a:hover {
	color: {$colors['alter_link']};
}
.menu_footer_nav_area ul li+li:before {
	border-color: {$colors['alter_light']};
}

.footer_copyright_inner {
	background-color: {$colors['alter_bg_color']};
	border-color: {$colors['bd_color']};
	color: {$colors['text']};
}
.footer_copyright_inner a {
	color: {$colors['text']};
}
.footer_copyright_inner a:hover {
	color: {$colors['text_light']};
}
.footer_copyright_inner .copyright_text {
	color: {$colors['text']};
}

/* Buttons */
.theme_button {
	color: {$colors['inverse_link']} !important;
	background-color: {$colors['text_link']} !important;
}
.theme_button:hover,
.theme_button:focus {
	color: {$colors['inverse_hover']} !important;
	background-color: {$colors['text_link_blend']} !important;
}
.more-link,
.socials_share:not(.socials_type_drop) .social_icons,
.comments_wrap .form-submit input[type="submit"] {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}
.more-link:hover,
.socials_share:not(.socials_type_drop) .social_icons:hover,
.comments_wrap .form-submit input[type="submit"]:hover,
.comments_wrap .form-submit input[type="submit"]:focus {
	color: {$colors['inverse_hover']};
	background-color: {$colors['text_link_blend']};
}

.format-video .post_featured.with_thumb .post_video_hover {
	color: {$colors['text_link']};
}
.format-video .post_featured.with_thumb .post_video_hover:hover {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}

.theme_scroll_down:hover {
	color: {$colors['text_link']};
}

/* Third-party plugins */

.mfp-bg {
	background-color: {$colors['bg_color_07']};
}
.mfp-image-holder .mfp-close,
.mfp-iframe-holder .mfp-close,
.mfp-close-btn-in .mfp-close {
	color: {$colors['text_dark']};
	background-color: transparent;
}
.mfp-image-holder .mfp-close:hover,
.mfp-iframe-holder .mfp-close:hover,
.mfp-close-btn-in .mfp-close:hover {
	color: {$colors['text_link']};
}

.sidebar .sidebar_inner{
	background-color: {$colors['bg_color']};
}
.widget_recent_comments a:hover,
.widget_recent_comments .comment-author-link,
.widget_recent_comments .comment-author-link a{
	color: {$colors['text_link']};
}
.widget_recent_comments .comment-author-link a:hover{
	color: {$colors['text_hover']};
}
.widget_recent_comments a{
	color: {$colors['text_dark']};
}


.widget_area .post_item .post_title, .widget .post_item .post_title,
.widget_area .post_item .post_title a, .widget .post_item .post_title a{
	color: {$colors['text']};
}
.widget_area .post_item .post_title a:hover, .widget .post_item .post_title a:hover{
	color: {$colors['text_dark']};
}


/*Accordion*/
.vc_tta.vc_tta-accordion .vc_tta-panel-heading .vc_tta-controls-icon-position-right.vc_tta-panel-title > a{
	color: {$colors['text_dark']};
	background-color: {$colors['alter_bg_color']};
}
body .vc_tta.vc_general.vc_tta-accordion .vc_tta-panels .vc_tta-panel-body{
	background-color: {$colors['alter_bg_color']} !important;
}
body .vc_tta.vc_tta-accordion .vc_tta-panel-title .vc_tta-title-text {
	{$fonts['h5_font-family']}
}


/*Tabs*/
body .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tabs-list .vc_tta-tab>a:hover, 
body .vc_tta-color-grey.vc_tta-style-classic .vc_tta-tabs-list .vc_tta-tab.vc_active>a{
	color: {$colors['inverse_dark']};
	background-color: {$colors['text_link']} !important;
}
body .vc_tta.vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab > a{
	color: {$colors['text_dark']};
}
body .vc_tta.vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab > a{
	{$fonts['button_font-family']}
}


/*Progress bars*/
body .vc_progress_bar.vc_progress_bar_narrow.vc_progress-bar-color-bar_orange .vc_single_bar .vc_label, 
body .vc_progress_bar.vc_progress_bar_narrow.vc_progress-bar-color-bar_orange .vc_single_bar .vc_label .vc_label_units{
	color: {$colors['inverse_dark']};
}
body .vc_progress_bar.vc_progress_bar_narrow.vc_progress-bar-color-bar_orange  .vc_single_bar{
	background-color: {$colors['bg_color_02']};
}
body .vc_progress_bar.vc_progress_bar_narrow .vc_single_bar .vc_label, 
body .vc_progress_bar.vc_progress_bar_narrow .vc_single_bar .vc_label .vc_label_units{
    color: {$colors['text_dark']};
}
body .vc_progress_bar.vc_progress_bar_narrow .vc_single_bar .vc_label, 
body .vc_progress_bar.vc_progress_bar_narrow .vc_single_bar .vc_label .vc_label_units{
	{$fonts['button_font-family']}
}


/*Notice*/
.vc_message_box p{
	{$fonts['h1_font-family']}
}
.vc_message_box p, .vc_message_box .vc_message_box-icon i{
    color: {$colors['text_dark']};
}
.vc_message_box.vc_color-info .vc_message_box-icon i, .vc_message_box.vc_color-info p, .vc_message_box.vc_color-warning .vc_message_box-icon i, .vc_message_box.vc_color-warning p,  .vc_message_box.vc_color-success .vc_message_box-icon i,  .vc_message_box.vc_color-success p{
    color: {$colors['inverse_dark']};
}
.vc_message_box.vc_color-grey {
    background-color: {$colors['alter_bg_color']};
}
.vc_message_box.vc_color-info {
    background-color: #dba860;
}
.vc_message_box.vc_color-warning {
    background-color: #d16a52;
}
.vc_message_box.vc_color-success {
    background-color: #43ba86;
}

.post_item_none_search .search_wrap .search_submit, .post_item_none_archive .search_wrap .search_submit{
    color: {$colors['text_dark']};
}
.post_item_none_search .search_wrap .search_submit:hover, .post_item_none_archive .search_wrap .search_submit:hover{
    color: {$colors['text_link']};
}

.sc_layouts_logo .logo_slogan{
    color: {$colors['text_dark']};
}


/*Excerpt*/
.post_item_single .post_meta.post_meta_date .post_date .post_date_month,
.post_layout_excerpt .post_meta.post_meta_date .post_date .post_date_month{
    color: {$colors['inverse_text']};
    background-color: {$colors['text_link']};
}
.post_item_single .post_meta.post_meta_date .post_date,
.post_item_single .post_meta.post_meta_date .post_date .post_date_day,
.post_layout_excerpt .post_meta.post_meta_date .post_date,
.post_layout_excerpt .post_meta.post_meta_date .post_date .post_date_day{
    color: {$colors['text_dark']};
    background-color: {$colors['alter_bd_color']};
}
.post_item_single .post_meta.post_meta_date .post_counters_item:before,
.post_layout_excerpt .post_meta.post_meta_date .post_counters_item:before {
    color: {$colors['text_link']};
}

.post_layout_sticky .post_meta_item.post_categories,
.post_item_single .post_meta_item.post_categories,
.post_layout_excerpt .post_meta_item.post_categories{
    color: {$colors['text']};    
}
.post_layout_sticky .post_meta:not(.post_meta_single):not(.post_meta_date) .post_meta_item a,
.post_item_single .post_meta:not(.post_meta_single):not(.post_meta_date) .post_meta_item a,
.post_item_single .post_meta:not(.post_meta_single):not(.post_meta_date) .post_meta_item span:not(.post_counters_number),
.post_layout_excerpt .post_meta_item a,
.post_layout_excerpt .post_meta_item span:not(.post_counters_number){
    color: {$colors['text_link']};    
}
.post_layout_sticky .post_meta:not(.post_meta_single):not(.post_meta_date) .post_meta_item a:hover,
.post_item_single .post_meta:not(.post_meta_single):not(.post_meta_date) .post_meta_item a:hover,
.post_item_single .post_meta:not(.post_meta_single):not(.post_meta_date) .post_meta_item:hover span:not(.post_counters_number),
.post_layout_excerpt .post_meta_item a:hover,
.post_layout_excerpt .post_meta_item:hover span:not(.post_counters_number){
    color: {$colors['text_hover']};    
}
.post_layout_sticky .post_meta_item.post_tags,
.post_item_single .post_meta_item.post_tags,
.post_layout_excerpt .post_meta_item.post_tags {
    color: {$colors['text_dark']};    
}
.post_layout_sticky .post_meta_item.post_tags a,
.post_item_single .post_meta_item.post_tags a,
.post_layout_excerpt .post_meta_item.post_tags a{
    color: {$colors['text']};    
    background-color: {$colors['alter_bd_color']};
}
.post_layout_sticky .post_meta_item.post_tags a:hover,  
.post_item_single .post_meta_item.post_tags a:hover,  
.post_layout_excerpt .post_meta_item.post_tags a:hover{  
    color: {$colors['inverse_text']};
    background-color: {$colors['text_link']};
}

.post_layout_sticky .post_meta_item.post_tags a{
    color: {$colors['text']} !important;
    background-color: {$colors['bg_color']};
}
.post_layout_sticky .post_meta_item.post_tags a:hover{
   color: {$colors['inverse_text']} !important;
   background-color: {$colors['text_link']};
}

.side_button_wrapper .sc_layouts .sc_events_detailed .sc_events_item:hover,
.side_button_wrapper .sc_layouts .sc_events_detailed .sc_events_item:hover .sc_events_item_date_detalied,
.side_button_wrapper .sc_layouts .sc_events_detailed .sc_events_item:hover .sc_events_item_title_detalied {
   color: {$colors['text_dark']} !important;
}

/* MailChimp */
.inverse_colors .mc4wp-form .terms_agree {
	color: {$colors['inverse_text']};
}
.mc4wp-form .mc4wp-response a {
    color: {$colors['text_dark']};
}
.mc4wp-form .mc4wp-response a:hover {
    color: {$colors['inverse_text']};
}

/* Give */
.give-wrap {
	color: {$colors['text']};
}
.give-wrap .give-card {
	background-color: {$colors['bg_color']};
}
.ua_edge .give-card .give-card__title{
	color: {$colors['text_dark']} !important;
}			
.give-card:hover .give-card__title {
	color: {$colors['text_hover']};
}		
.ua_edge .give-card:hover .give-card__title {
	color: {$colors['text_hover']} !important;
}
.give-goal-progress .goal-text,
.give-goal-progress .goal-text:hover{
	color: {$colors['text_dark']};
}
.give-goal-progress .income {
	color: {$colors['text_link']} !important;
}			
.give-form input[type="radio"] + label:before {
	background-color: {$colors['bg_color']};
	border-color: {$colors['bd_color']} !important;
}
form.give-form .form-row input[type="text"],
form.give-form .form-row input[type="email"] {
	background-color: {$colors['bg_color']};
	border-color: {$colors['bd_color']};
}
form[id*="give-form"] .give-donation-amount #give-amount, 
form[id*="give-form"] .give-donation-amount #give-amount-text	{
	color: {$colors['input_text']};
	border-color: {$colors['bd_color']};
}	
form[id*="give-form"] .give-donation-amount .give-currency-symbol {
	color: {$colors['inverse_text']};
	background-color: {$colors['text_link']};
	border-color: {$colors['text_link']};
}
form[id*="give-form"] #give-final-total-wrap .give-donation-total-label {
	color: {$colors['inverse_link']};
	background-color: {$colors['text_link']};
}
form[id*="give-form"] #give-final-total-wrap .give-final-total-amount {
	color: {$colors['text']};
}		
[id*="give-form"].give-fl-form .give-fl-is-required:before	{
	color: {$colors['text_link']};
}	
.give-wrap .give-card__text,
.give-wrap .give-card .give-card__text,
.give-card:hover .give-wrap .give-card__text {
	color: {$colors['text']};
	border-color: {$colors['bd_color']};
}	
.give_success {
	border-color: {$colors['text_hover']};
}		
.give_success:before {
	background-color: {$colors['text_hover']};
}
.give_error {
	border-color: {$colors['text_link']};
}		
.give_error:before {
	background-color: {$colors['text_link']};
}
.give-card .give-goal-progress .raised {
	color: {$colors['text']};
}		
.give-card:hover .give-goal-progress .raised {
	color: {$colors['text_hover']} !important;
}

/* Gutenberg */
.wp-block-cover-text a {
	color: {$colors['inverse_link']} ;
}
.wp-block-cover-text a:hover {
	color: {$colors['text_hover']} ;
}



CSS;
				
					$rez = apply_filters('holy_church_filter_get_css', $rez, $colors, false, $scheme);
					$css['colors'] .= $rez['colors'];
				}
			}
		}
				
		$css_str = (!empty($css['fonts']) ? $css['fonts'] : '')
				. (!empty($css['colors']) ? $css['colors'] : '');
		return apply_filters( 'holy_church_filter_prepare_css', $css_str, $remove_spaces );
	}
}
?>