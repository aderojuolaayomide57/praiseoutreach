<?php
/**
 * The template to display the background video in the header
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.14
 */
$holy_church_header_video = holy_church_get_header_video();
if (!empty($holy_church_header_video) && !holy_church_is_from_uploads($holy_church_header_video)) {
	global $wp_embed;
	if (is_object($wp_embed))
		$holy_church_embed_video = do_shortcode($wp_embed->run_shortcode( '[embed]' . trim($holy_church_header_video) . '[/embed]' ));
	$holy_church_embed_video = holy_church_make_video_autoplay($holy_church_embed_video);
	?><div id="background_video"><?php holy_church_show_layout($holy_church_embed_video); ?></div><?php
}
?>