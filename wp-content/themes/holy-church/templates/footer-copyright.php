<?php
/**
 * The template to display the copyright info in the footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */

// Copyright area
$holy_church_footer_scheme =  holy_church_is_inherit(holy_church_get_theme_option('footer_scheme')) ? holy_church_get_theme_option('color_scheme') : holy_church_get_theme_option('footer_scheme');
$holy_church_copyright_scheme = holy_church_is_inherit(holy_church_get_theme_option('copyright_scheme')) ? $holy_church_footer_scheme : holy_church_get_theme_option('copyright_scheme');
?> 
<div class="footer_copyright_wrap scheme_<?php echo esc_attr($holy_church_copyright_scheme); ?>">
	<div class="footer_copyright_inner">
		<div class="content_wrap">
			<div class="copyright_text"><?php
				// Replace {{...}} and [[...]] on the <i>...</i> and <b>...</b>
				$holy_church_copyright = holy_church_prepare_macros(holy_church_get_theme_option('copyright'));
				if (!empty($holy_church_copyright)) {
					holy_church_show_layout(do_shortcode(str_replace(array('{{Y}}', '{Y}'), date('Y'), holy_church_get_theme_option('copyright'))));
				}
			?></div>
		</div>
	</div>
</div>
