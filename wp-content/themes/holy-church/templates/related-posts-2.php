<?php
/**
 * The template 'Style 2' to displaying related posts
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_link = get_permalink();
$holy_church_post_format = get_post_format();
$holy_church_post_format = empty($holy_church_post_format) ? 'standard' : str_replace('post-format-', '', $holy_church_post_format);
?><div id="post-<?php the_ID(); ?>" 
	<?php post_class( 'related_item related_item_style_2 post_format_'.esc_attr($holy_church_post_format) ); ?>><?php
	holy_church_show_post_featured(array(
		'thumb_size' => holy_church_get_thumb_size( 'big' ),
		'show_no_image' => true,
		'singular' => false
		)
	);
	?><div class="post_header entry-header"><?php
		if ( in_array(get_post_type(), array( 'post', 'attachment' ) ) ) {
			?><span class="post_date"><a href="<?php echo esc_url($holy_church_link); ?>"><?php echo holy_church_get_date(); ?></a></span><?php
		}
		?>
		<h6 class="post_title entry-title"><a href="<?php echo esc_url($holy_church_link); ?>"><?php echo the_title(); ?></a></h6>
	</div>
</div>