<?php
/**
 * The template to display the page title and breadcrumbs
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

// Page (category, tag, archive, author) title


if ( holy_church_need_page_title() ) {
	holy_church_sc_layouts_showed('title', true);
	holy_church_sc_layouts_showed('postmeta', true);

    $holy_church_header_css = $holy_church_header_image = '';
    $holy_church_header_video = holy_church_get_header_video();
    if (true || empty($holy_church_header_video)) {
        $holy_church_header_image = get_header_image();
        if (holy_church_is_on(holy_church_get_theme_option('header_image_override')) && apply_filters('holy_church_filter_allow_override_header_image', true)) {
            if (is_category()) {
                if (($holy_church_cat_img = holy_church_get_category_image()) != '')
                    $holy_church_header_image = $holy_church_cat_img;
            } else if (is_singular() || holy_church_storage_isset('blog_archive')) {
                if (has_post_thumbnail()) {
                    $holy_church_header_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
                    if (is_array($holy_church_header_image)) $holy_church_header_image = $holy_church_header_image[0];
                } else
                    $holy_church_header_image = '';
            }
        }
    }
	?>
	<div class="top_panel_title sc_layouts_row sc_layouts_row_type_normal<?php
                echo !empty($holy_church_header_image) || !empty($holy_church_header_video) ? ' with_bg_image' : ' without_bg_image';
                if ($holy_church_header_video!='') echo ' with_bg_video';
                if ($holy_church_header_image!='') echo ' '.esc_attr(holy_church_add_inline_css_class('background-image: url('.esc_url($holy_church_header_image).');'));
                if (is_single() && has_post_thumbnail()) echo ' with_featured_image';
                ?>">
		<div class="content_wrap">
			<div class="sc_layouts_column <?php
                echo (( is_category() || is_tag() || is_tax() || is_single() ) ? ' sc_layouts_column_align_center' : ' sc_layouts_column_align_flex')
                ?>">
				<div class="sc_layouts_item">
					<div class="sc_layouts_title">
						<?php
						
						// Blog/Post title
						?><div class="sc_layouts_title_title"><?php
							$holy_church_blog_title = holy_church_get_blog_title();
							$holy_church_blog_title_text = $holy_church_blog_title_class = $holy_church_blog_title_link = $holy_church_blog_title_link_text = '';
							if (is_array($holy_church_blog_title)) {
								$holy_church_blog_title_text = $holy_church_blog_title['text'];
								$holy_church_blog_title_class = !empty($holy_church_blog_title['class']) ? ' '.$holy_church_blog_title['class'] : '';
								$holy_church_blog_title_link = !empty($holy_church_blog_title['link']) ? $holy_church_blog_title['link'] : '';
								$holy_church_blog_title_link_text = !empty($holy_church_blog_title['link_text']) ? $holy_church_blog_title['link_text'] : '';
							} else
								$holy_church_blog_title_text = $holy_church_blog_title;
							?>
							<h1 class="sc_layouts_title_caption<?php echo esc_attr($holy_church_blog_title_class); ?>"><?php
								$holy_church_top_icon = holy_church_get_category_icon();
								if (!empty($holy_church_top_icon)) {
									$holy_church_attr = holy_church_getimagesize($holy_church_top_icon);
									?><img src="<?php echo esc_url($holy_church_top_icon); ?>" alt="'.esc_attr__('top_icon', 'holy-church').'" <?php if (!empty($holy_church_attr[3])) holy_church_show_layout($holy_church_attr[3]);?>><?php
								}
								echo wp_kses_post($holy_church_blog_title_text);
							?></h1>
							<?php
							if (!empty($holy_church_blog_title_link) && !empty($holy_church_blog_title_link_text)) {
								?><a href="<?php echo esc_url($holy_church_blog_title_link); ?>" class="theme_button theme_button_small sc_layouts_title_link"><?php echo esc_html($holy_church_blog_title_link_text); ?></a><?php
							}
							
							// Category/Tag description
							if ( is_category() || is_tag() || is_tax() ) 
								the_archive_description( '<div class="sc_layouts_title_description">', '</div>' );
		
						?></div><?php
	
						// Breadcrumbs
						?><div class="sc_layouts_title_breadcrumbs"><?php
							do_action( 'holy_church_action_breadcrumbs');
						?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>