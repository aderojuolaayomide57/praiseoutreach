<?php
/**
 * The template to display Admin notices
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.1
 */
?>
<div class="update-nag" id="holy_church_admin_notice">
	<h3 class="holy_church_notice_title"><?php echo sprintf(esc_html__('Welcome to %s', 'holy-church'), wp_get_theme()->name); ?></h3>
	<?php
	if (!holy_church_exists_trx_addons()) {
		?><p><?php echo wp_kses_data(__('<b>Attention!</b> Plugin "ThemeREX Addons is required! Please, install and activate it!', 'holy-church')); ?></p><?php
	}
	?><p><?php
		if (holy_church_get_value_gp('page')!='tgmpa-install-plugins') {
			?>
			<a href="<?php echo esc_url(admin_url().'themes.php?page=tgmpa-install-plugins'); ?>" class="button-primary"><i class="dashicons dashicons-admin-plugins"></i> <?php esc_html_e('Install plugins', 'holy-church'); ?></a>
			<?php
		}
		if (function_exists('holy_church_exists_trx_addons') && holy_church_exists_trx_addons()) {
			?>
			<a href="<?php echo esc_url(admin_url().'themes.php?page=trx_importer'); ?>" class="button-primary"><i class="dashicons dashicons-download"></i> <?php esc_html_e('One Click Demo Data', 'holy-church'); ?></a>
			<?php
		}
		?>
        <a href="<?php echo esc_url(admin_url().'customize.php'); ?>" class="button-primary"><i class="dashicons dashicons-admin-appearance"></i> <?php esc_html_e('Theme Customizer', 'holy-church'); ?></a>
        <a href="#" class="button holy_church_hide_notice"><i class="dashicons dashicons-dismiss"></i> <?php esc_html_e('Hide Notice', 'holy-church'); ?></a>
	</p>
</div>