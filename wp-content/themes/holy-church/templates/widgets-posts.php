<?php
/**
 * The template to display posts in widgets and/or in the search results
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_post_id    = get_the_ID();
$holy_church_post_date  = holy_church_get_date();
$holy_church_post_title = get_the_title();
$holy_church_post_link  = get_permalink();
$holy_church_post_author_id   = get_the_author_meta('ID');
$holy_church_post_author_name = get_the_author_meta('display_name');
$holy_church_post_author_url  = get_author_posts_url($holy_church_post_author_id, '');

$holy_church_args = get_query_var('holy_church_args_widgets_posts');
$holy_church_show_date = isset($holy_church_args['show_date']) ? (int) $holy_church_args['show_date'] : 1;
$holy_church_show_image = isset($holy_church_args['show_image']) ? (int) $holy_church_args['show_image'] : 1;
$holy_church_show_author = isset($holy_church_args['show_author']) ? (int) $holy_church_args['show_author'] : 1;
$holy_church_show_counters = isset($holy_church_args['show_counters']) ? (int) $holy_church_args['show_counters'] : 1;
$holy_church_show_categories = isset($holy_church_args['show_categories']) ? (int) $holy_church_args['show_categories'] : 1;

$holy_church_output = holy_church_storage_get('holy_church_output_widgets_posts');

$holy_church_post_counters_output = '';
if ( $holy_church_show_counters ) {
	$holy_church_post_counters_output = '<span class="post_info_item post_info_counters">'
								. holy_church_get_post_counters('comments')
							. '</span>';
}


$holy_church_output .= '<article class="post_item with_thumb">';

if ($holy_church_show_image) {
	$holy_church_post_thumb = get_the_post_thumbnail($holy_church_post_id, holy_church_get_thumb_size('tiny'), array(
		'alt' => the_title_attribute( array( 'echo' => false ) )
	));
	if ($holy_church_post_thumb) $holy_church_output .= '<div class="post_thumb">' . ($holy_church_post_link ? '<a href="' . esc_url($holy_church_post_link) . '">' : '') . ($holy_church_post_thumb) . ($holy_church_post_link ? '</a>' : '') . '</div>';
}

$holy_church_output .= '<div class="post_content">'
			. ($holy_church_show_categories 
					? '<div class="post_categories">'
						. holy_church_get_post_categories()
						. $holy_church_post_counters_output
						. '</div>' 
					: '')
			. '<h6 class="post_title">' . ($holy_church_post_link ? '<a href="' . esc_url($holy_church_post_link) . '">' : '') . ($holy_church_post_title) . ($holy_church_post_link ? '</a>' : '') . '</h6>'
			. apply_filters('holy_church_filter_get_post_info', 
								'<div class="post_info">'
									. ($holy_church_show_date 
										? '<span class="post_info_item post_info_posted">'
											. ($holy_church_post_link ? '<a href="' . esc_url($holy_church_post_link) . '" class="post_info_date">' : '') 
											. esc_html($holy_church_post_date) 
											. ($holy_church_post_link ? '</a>' : '')
											. '</span>'
										: '')
									. ($holy_church_show_author 
										? '<span class="post_info_item post_info_posted_by">' 
											. esc_html__('by', 'holy-church') . ' ' 
											. ($holy_church_post_link ? '<a href="' . esc_url($holy_church_post_author_url) . '" class="post_info_author">' : '') 
											. esc_html($holy_church_post_author_name) 
											. ($holy_church_post_link ? '</a>' : '') 
											. '</span>'
										: '')
									. (!$holy_church_show_categories && $holy_church_post_counters_output
										? $holy_church_post_counters_output
										: '')
								. '</div>')
		. '</div>'
	. '</article>';
holy_church_storage_set('holy_church_output_widgets_posts', $holy_church_output);
?>