<?php
/**
 * The template to display the widgets area in the header
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

// Header sidebar
$holy_church_header_name = holy_church_get_theme_option('header_widgets');
$holy_church_header_present = !holy_church_is_off($holy_church_header_name) && is_active_sidebar($holy_church_header_name);
if ($holy_church_header_present) { 
	holy_church_storage_set('current_sidebar', 'header');
	$holy_church_header_wide = holy_church_get_theme_option('header_wide');
	ob_start();
	if ( is_active_sidebar($holy_church_header_name) ) {
		dynamic_sidebar($holy_church_header_name);
	}
	$holy_church_widgets_output = ob_get_contents();
	ob_end_clean();
	if (!empty($holy_church_widgets_output)) {
		$holy_church_widgets_output = preg_replace("/<\/aside>[\r\n\s]*<aside/", "</aside><aside", $holy_church_widgets_output);
		$holy_church_need_columns = strpos($holy_church_widgets_output, 'columns_wrap')===false;
		if ($holy_church_need_columns) {
			$holy_church_columns = max(0, (int) holy_church_get_theme_option('header_columns'));
			if ($holy_church_columns == 0) $holy_church_columns = min(6, max(1, substr_count($holy_church_widgets_output, '<aside ')));
			if ($holy_church_columns > 1)
				$holy_church_widgets_output = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($holy_church_columns).' widget ', $holy_church_widgets_output);
			else
				$holy_church_need_columns = false;
		}
		?>
		<div class="header_widgets_wrap widget_area<?php echo !empty($holy_church_header_wide) ? ' header_fullwidth' : ' header_boxed'; ?>">
			<div class="header_widgets_inner widget_area_inner">
				<?php 
				if (!$holy_church_header_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($holy_church_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'holy_church_action_before_sidebar' );
				holy_church_show_layout($holy_church_widgets_output);
				do_action( 'holy_church_action_after_sidebar' );
				if ($holy_church_need_columns) {
					?></div>	<!-- /.columns_wrap --><?php
				}
				if (!$holy_church_header_wide) {
					?></div>	<!-- /.content_wrap --><?php
				}
				?>
			</div>	<!-- /.header_widgets_inner -->
		</div>	<!-- /.header_widgets_wrap -->
		<?php
	}
}
?>