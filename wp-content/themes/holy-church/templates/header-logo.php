<?php
/**
 * The template to display the logo or the site name and the slogan in the Header
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_args = get_query_var('holy_church_logo_args');

// Site logo
$holy_church_logo_image  = holy_church_get_logo_image(isset($holy_church_args['type']) ? $holy_church_args['type'] : '');
$holy_church_logo_text   = holy_church_is_on(holy_church_get_theme_option('logo_text')) ? get_bloginfo( 'name' ) : '';
$holy_church_logo_slogan = get_bloginfo( 'description', 'display' );
if (!empty($holy_church_logo_image) || !empty($holy_church_logo_text)) {
	?><a class="sc_layouts_logo" href="<?php echo esc_url(home_url('/')); ?>"><?php
		if (!empty($holy_church_logo_image)) {
			$holy_church_attr = holy_church_getimagesize($holy_church_logo_image);
			echo '<img src="'.esc_url($holy_church_logo_image).'" alt="'.esc_attr__('logo_header_image', 'holy-church').'"'.(!empty($holy_church_attr[3]) ? sprintf(' %s', $holy_church_attr[3]) : '').'>' ;
		} else {
			holy_church_show_layout(holy_church_prepare_macros($holy_church_logo_text), '<span class="logo_text">', '</span>');
			holy_church_show_layout(holy_church_prepare_macros($holy_church_logo_slogan), '<span class="logo_slogan">', '</span>');
		}
	?></a><?php
}
?>