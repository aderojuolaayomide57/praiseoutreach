<?php
/**
 * The template to display menu in the footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */

// Footer menu
$holy_church_menu_footer = holy_church_get_nav_menu('menu_footer');
if (!empty($holy_church_menu_footer)) {
	?>
	<div class="footer_menu_wrap">
		<div class="footer_menu_inner">
			<?php holy_church_show_layout($holy_church_menu_footer); ?>
		</div>
	</div>
	<?php
}
?>