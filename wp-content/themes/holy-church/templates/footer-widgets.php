<?php
/**
 * The template to display the widgets area in the footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */

// Footer sidebar
$holy_church_footer_name = holy_church_get_theme_option('footer_widgets');
$holy_church_footer_present = !holy_church_is_off($holy_church_footer_name) && is_active_sidebar($holy_church_footer_name);
if ($holy_church_footer_present) { 
	holy_church_storage_set('current_sidebar', 'footer');
	$holy_church_footer_wide = holy_church_get_theme_option('footer_wide');
	ob_start();
	if ( is_active_sidebar($holy_church_footer_name) ) {
		dynamic_sidebar($holy_church_footer_name);
	}
	$holy_church_out = trim(ob_get_contents());
	ob_end_clean();
	if (!empty($holy_church_out)) {
		$holy_church_out = preg_replace("/<\\/aside>[\r\n\s]*<aside/", "</aside><aside", $holy_church_out);
		$holy_church_need_columns = true;
		if ($holy_church_need_columns) {
			$holy_church_columns = max(0, (int) holy_church_get_theme_option('footer_columns'));
			if ($holy_church_columns == 0) $holy_church_columns = min(6, max(1, substr_count($holy_church_out, '<aside ')));
			if ($holy_church_columns > 1)
				$holy_church_out = preg_replace("/class=\"widget /", "class=\"column-1_".esc_attr($holy_church_columns).' widget ', $holy_church_out);
			else
				$holy_church_need_columns = false;
		}
		?>
		<div class="footer_widgets_wrap widget_area<?php echo !empty($holy_church_footer_wide) ? ' footer_fullwidth' : ''; ?>">
			<div class="footer_widgets_inner widget_area_inner">
				<?php 
				if (!$holy_church_footer_wide) { 
					?><div class="content_wrap"><?php
				}
				if ($holy_church_need_columns) {
					?><div class="columns_wrap"><?php
				}
				do_action( 'holy_church_action_before_sidebar' );
				holy_church_show_layout($holy_church_out);
				do_action( 'holy_church_action_after_sidebar' );
				if ($holy_church_need_columns) {
					?></div><!-- /.columns_wrap --><?php
				}
				if (!$holy_church_footer_wide) {
					?></div><!-- /.content_wrap --><?php
				}
				?>
			</div><!-- /.footer_widgets_inner -->
		</div><!-- /.footer_widgets_wrap -->
		<?php
	}
}
?>