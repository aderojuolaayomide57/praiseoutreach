<?php
/**
 * The template to show mobile menu
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */
?>
<div class="menu_mobile_overlay"></div>
<div class="menu_mobile menu_mobile_<?php echo esc_attr(holy_church_get_theme_option('menu_mobile_fullscreen') > 0 ? 'fullscreen' : 'narrow'); ?> scheme_dark">
	<div class="menu_mobile_inner">
		<a class="menu_mobile_close icon-cancel"></a><?php

		// Logo
		set_query_var('holy_church_logo_args', array('type' => 'inverse'));
		get_template_part( 'templates/header-logo' );
		set_query_var('holy_church_logo_args', array());

		// Mobile menu
		$holy_church_menu_mobile = holy_church_get_nav_menu('menu_mobile');
		if (empty($holy_church_menu_mobile)) {
			$holy_church_menu_mobile = apply_filters('holy_church_filter_get_mobile_menu', '');
			if (empty($holy_church_menu_mobile)) $holy_church_menu_mobile = holy_church_get_nav_menu('menu_main');
			if (empty($holy_church_menu_mobile)) $holy_church_menu_mobile = holy_church_get_nav_menu();
		}
		if (!empty($holy_church_menu_mobile)) {
			if (!empty($holy_church_menu_mobile))
				$holy_church_menu_mobile = str_replace(
					array('menu_main', 'id="menu-', 'sc_layouts_menu_nav', 'sc_layouts_hide_on_mobile', 'hide_on_mobile'),
					array('menu_mobile', 'id="menu_mobile-', '', '', ''),
					$holy_church_menu_mobile
					);
			if (strpos($holy_church_menu_mobile, '<nav ')===false)
				$holy_church_menu_mobile = sprintf('<nav class="menu_mobile_nav_area">%s</nav>', $holy_church_menu_mobile);
			holy_church_show_layout(apply_filters('holy_church_filter_menu_mobile_layout', $holy_church_menu_mobile));
		}

		// Search field
		do_action('holy_church_action_search', 'normal', 'search_mobile', false);
		
		// Social icons
		holy_church_show_layout(holy_church_get_socials_links(), '<div class="socials_mobile">', '</div>');
		?>
	</div>
</div>
