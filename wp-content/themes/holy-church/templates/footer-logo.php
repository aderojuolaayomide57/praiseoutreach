<?php
/**
 * The template to display the site logo in the footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */

// Logo
if (holy_church_is_on(holy_church_get_theme_option('logo_in_footer'))) {
	$holy_church_logo_image = '';
	if (holy_church_get_retina_multiplier(2) > 1)
		$holy_church_logo_image = holy_church_get_theme_option( 'logo_footer_retina' );
	if (empty($holy_church_logo_image)) 
		$holy_church_logo_image = holy_church_get_theme_option( 'logo_footer' );
	$holy_church_logo_text   = get_bloginfo( 'name' );
	if (!empty($holy_church_logo_image) || !empty($holy_church_logo_text)) {
		?>
		<div class="footer_logo_wrap">
			<div class="footer_logo_inner">
				<?php
				if (!empty($holy_church_logo_image)) {
					$holy_church_attr = holy_church_getimagesize($holy_church_logo_image);
					echo '<a href="'.esc_url(home_url('/')).'"><img src="'.esc_url($holy_church_logo_image).'" class="logo_footer_image" alt="'.esc_attr__('logo_footer_image', 'holy-church').'"'.(!empty($holy_church_attr[3]) ? sprintf(' %s', $holy_church_attr[3]) : '').'></a>' ;
				} else if (!empty($holy_church_logo_text)) {
					echo '<h1 class="logo_footer_text"><a href="'.esc_url(home_url('/')).'">' . esc_html($holy_church_logo_text) . '</a></h1>';
				}
				?>
			</div>
		</div>
		<?php
	}
}
?>