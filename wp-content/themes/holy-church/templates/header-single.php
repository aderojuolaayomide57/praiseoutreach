<?php
/**
 * The template to display the featured image in the single post
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

if ( get_query_var('holy_church_header_image')=='' && is_singular() && has_post_thumbnail() && in_array(get_post_type(), array('post', 'page')) )  {
	$holy_church_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
	if (!empty($holy_church_src[0])) {
		holy_church_sc_layouts_showed('featured', false);
	}
}
?>