<?php
/**
 * The template to display custom header from the ThemeREX Addons Layouts
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.06
 */

$holy_church_header_id = str_replace('header-custom-', '', holy_church_get_theme_option("header_style"));
if ((int) $holy_church_header_id == 0) {
    $holy_church_header_id = holy_church_get_post_id(array(
                                                'name' => $holy_church_header_id,
                                                'post_type' => defined('TRX_ADDONS_CPT_LAYOUTS_PT') ? TRX_ADDONS_CPT_LAYOUTS_PT : 'cpt_layouts'
                                                )
                                            );
} else {
    $holy_church_header_id = apply_filters('trx_addons_filter_get_translated_layout', $holy_church_header_id);
}
?><header class="top_panel top_panel_custom top_panel_custom_<?php echo esc_attr($holy_church_header_id);
						if (holy_church_is_on(holy_church_get_theme_option('header_fullheight'))) echo ' header_fullheight trx-stretch-height';
						?> scheme_<?php echo esc_attr(holy_church_is_inherit(holy_church_get_theme_option('header_scheme')) 
														? holy_church_get_theme_option('color_scheme') 
														: holy_church_get_theme_option('header_scheme'));
						?>"><?php
		
	// Custom header's layout
	do_action('holy_church_action_show_layout', $holy_church_header_id);

	// Header widgets area
	get_template_part( 'templates/header-widgets' );

?></header>