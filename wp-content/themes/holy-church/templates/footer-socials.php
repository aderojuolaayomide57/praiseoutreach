<?php
/**
 * The template to display the socials in the footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */


// Socials
if ( holy_church_is_on(holy_church_get_theme_option('socials_in_footer')) && ($holy_church_output = holy_church_get_socials_links()) != '') {
	?>
	<div class="footer_socials_wrap socials_wrap">
		<div class="footer_socials_inner">
			<?php holy_church_show_layout($holy_church_output); ?>
		</div>
	</div>
	<?php
}
?>