<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */

$holy_church_footer_scheme =  holy_church_is_inherit(holy_church_get_theme_option('footer_scheme')) ? holy_church_get_theme_option('color_scheme') : holy_church_get_theme_option('footer_scheme');
?>
<footer class="footer_wrap footer_default scheme_<?php echo esc_attr($holy_church_footer_scheme); ?>">
	<?php

	// Footer widgets area
	get_template_part( 'templates/footer-widgets' );

    // Menu
    get_template_part( 'templates/footer-menu' );

    if ((holy_church_is_on(holy_church_get_theme_option('logo_in_footer'))) && ( holy_church_is_on(holy_church_get_theme_option('socials_in_footer')) && ($holy_church_output = holy_church_get_socials_links()) != ''))
    ?><div class="footer_logo_and_socials"><div class="content_wrap"><?php

        // Logo
        get_template_part( 'templates/footer-logo' );

        // Socials
        get_template_part( 'templates/footer-socials' );

        // Copyright area
        get_template_part( 'templates/footer-copyright' );

    if ((holy_church_is_on(holy_church_get_theme_option('logo_in_footer'))) && ( holy_church_is_on(holy_church_get_theme_option('socials_in_footer')) && ($holy_church_output = holy_church_get_socials_links()) != ''))
    ?></div></div><?php

?>
</footer><!-- /.footer_wrap -->
