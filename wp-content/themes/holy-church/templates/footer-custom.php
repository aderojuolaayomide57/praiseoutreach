<?php
/**
 * The template to display default site footer
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0.10
 */

$holy_church_footer_scheme = holy_church_is_inherit( holy_church_get_theme_option( 'footer_scheme' ) ) ? holy_church_get_theme_option( 'color_scheme' ) : holy_church_get_theme_option( 'footer_scheme' );
$holy_church_footer_id     = str_replace( 'footer-custom-', '', holy_church_get_theme_option( "footer_style" ) );
    if ( (int) $holy_church_footer_id == 0 ) {
        $holy_church_footer_id = holy_church_get_post_id( array(
                'name'      => $holy_church_footer_id,
                'post_type' => defined( 'TRX_ADDONS_CPT_LAYOUT_PT' ) ? TRX_ADDONS_CPT_LAYOUT_PT : 'cpt_layouts'
            )
        );
    } else {
        $holy_church_footer_id = apply_filters( 'trx_addons_filter_get_translated_layout', $holy_church_footer_id );
    }

$holy_church_footer_meta = get_post_meta( $holy_church_footer_id, 'trx_addons_options', true );
?>
<footer class="footer_wrap footer_custom footer_custom_<?php echo esc_attr( $holy_church_footer_id );
?> footer_custom_<?php echo esc_attr( sanitize_title( get_the_title( $holy_church_footer_id ) ) );
    if ( ! empty( $holy_church_footer_meta['margin'] ) != '' ) {
        echo ' ' . esc_attr( holy_church_add_inline_css_class( 'margin-top: ' . esc_attr( holy_church_prepare_css_value( $holy_church_footer_meta['margin'] ) ) . ';' ) );
    }
    ?> scheme_<?php echo esc_attr( $holy_church_footer_scheme );
?>">
	<?php
	// Custom footer's layout
	do_action( 'holy_church_action_show_layout', $holy_church_footer_id );
	?>
</footer><!-- /.footer_wrap -->