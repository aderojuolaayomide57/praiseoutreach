<?php
/**
 * The template to display default site header
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_header_video = false;

?><header class="top_panel top_panel_default<?php
					if (holy_church_is_on(holy_church_get_theme_option('header_fullheight'))) echo ' header_fullheight trx-stretch-height';
					?> scheme_<?php echo esc_attr(holy_church_is_inherit(holy_church_get_theme_option('header_scheme')) 
													? holy_church_get_theme_option('color_scheme') 
													: holy_church_get_theme_option('header_scheme'));
					?>"><?php

	// Background video
	if (!empty($holy_church_header_video)) {
		get_template_part( 'templates/header-video' );
	}
	
	// Main menu
    get_template_part( 'templates/header-navi' );

	// Page title and breadcrumbs area
	get_template_part( 'templates/header-title');

	// Header widgets area
	get_template_part( 'templates/header-widgets' );

	// Header for single posts
	get_template_part( 'templates/header-single' );

?></header>