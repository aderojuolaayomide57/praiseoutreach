<?php
/**
 * The template to display blog archive
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

/*
Template Name: Blog archive
*/

/**
 * Make page with this template and put it into menu
 * to display posts as blog archive
 * You can setup output parameters (blog style, posts per page, parent category, etc.)
 * in the Theme Options section (under the page content)
 * You can build this page in the WPBakery Page Builder to make custom page layout:
 * just insert %%CONTENT%% in the desired place of content
 */

// Get template page's content
$holy_church_content = '';
$holy_church_blog_archive_mask = '%%CONTENT%%';
$holy_church_blog_archive_subst = sprintf('<div class="blog_archive">%s</div>', $holy_church_blog_archive_mask);
if ( have_posts() ) {
	the_post(); 
	if (($holy_church_content = apply_filters('the_content', get_the_content())) != '') {
		if (($holy_church_pos = strpos($holy_church_content, $holy_church_blog_archive_mask)) !== false) {
			$holy_church_content = preg_replace('/(\<p\>\s*)?'.$holy_church_blog_archive_mask.'(\s*\<\/p\>)/i', $holy_church_blog_archive_subst, $holy_church_content);
		} else
			$holy_church_content .= $holy_church_blog_archive_subst;
		$holy_church_content = explode($holy_church_blog_archive_mask, $holy_church_content);
	}
}

// Prepare args for a new query
$holy_church_args = array(
	'post_status' => current_user_can('read_private_pages') && current_user_can('read_private_posts') ? array('publish', 'private') : 'publish'
);
$holy_church_args = holy_church_query_add_posts_and_cats($holy_church_args, '', holy_church_get_theme_option('post_type'), holy_church_get_theme_option('parent_cat'));
$holy_church_page_number = get_query_var('paged') ? get_query_var('paged') : (get_query_var('page') ? get_query_var('page') : 1);
if ($holy_church_page_number > 1) {
	$holy_church_args['paged'] = $holy_church_page_number;
	$holy_church_args['ignore_sticky_posts'] = true;
}
$holy_church_ppp = holy_church_get_theme_option('posts_per_page');
if ((int) $holy_church_ppp != 0)
	$holy_church_args['posts_per_page'] = (int) $holy_church_ppp;
// Make a new query
query_posts( $holy_church_args );
// Set a new query as main WP Query
$GLOBALS['wp_the_query'] = $GLOBALS['wp_query'];

// Set query vars in the new query!
if (is_array($holy_church_content) && count($holy_church_content) == 2) {
	set_query_var('blog_archive_start', $holy_church_content[0]);
	set_query_var('blog_archive_end', $holy_church_content[1]);
}

get_template_part('index');
?>