<?php
/**
 * The template for homepage posts with "Portfolio" style
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

holy_church_storage_set('blog_archive', true);

// Load scripts for both 'Gallery' and 'Portfolio' layouts!
wp_enqueue_script( 'classie', holy_church_get_file_url('js/theme.gallery/classie.min.js'), array(), null, true );
wp_enqueue_script( 'imagesloaded', holy_church_get_file_url('js/theme.gallery/imagesloaded.min.js'), array(), null, true );
wp_enqueue_script( 'masonry', holy_church_get_file_url('js/theme.gallery/masonry.min.js'), array(), null, true );
wp_enqueue_script( 'holy-church-gallery-script', holy_church_get_file_url('js/theme.gallery/theme.gallery.js'), array(), null, true );

get_header(); 

if (have_posts()) {

	echo get_query_var('blog_archive_start');

	$holy_church_stickies = is_home() ? get_option( 'sticky_posts' ) : false;
	$holy_church_sticky_out = is_array($holy_church_stickies) && count($holy_church_stickies) > 0 && get_query_var( 'paged' ) < 1;
	
	// Show filters
	$holy_church_cat = holy_church_get_theme_option('parent_cat');
	$holy_church_post_type = holy_church_get_theme_option('post_type');
	$holy_church_taxonomy = holy_church_get_post_type_taxonomy($holy_church_post_type);
	$holy_church_show_filters = holy_church_get_theme_option('show_filters');
	$holy_church_tabs = array();
	if (!holy_church_is_off($holy_church_show_filters)) {
		$holy_church_args = array(
			'type'			=> $holy_church_post_type,
			'child_of'		=> $holy_church_cat,
			'orderby'		=> 'name',
			'order'			=> 'ASC',
			'hide_empty'	=> 1,
			'hierarchical'	=> 0,
			'exclude'		=> '',
			'include'		=> '',
			'number'		=> '',
			'taxonomy'		=> $holy_church_taxonomy,
			'pad_counts'	=> false
		);
		$holy_church_portfolio_list = get_terms($holy_church_args);
		if (is_array($holy_church_portfolio_list) && count($holy_church_portfolio_list) > 0) {
			$holy_church_tabs[$holy_church_cat] = esc_html__('All', 'holy-church');
			foreach ($holy_church_portfolio_list as $holy_church_term) {
				if (isset($holy_church_term->term_id)) $holy_church_tabs[$holy_church_term->term_id] = $holy_church_term->name;
			}
		}
	}
	if (count($holy_church_tabs) > 0) {
		$holy_church_portfolio_filters_ajax = true;
		$holy_church_portfolio_filters_active = $holy_church_cat;
		$holy_church_portfolio_filters_id = 'portfolio_filters';
		if (!is_customize_preview())
			wp_enqueue_script('jquery-ui-tabs', false, array('jquery', 'jquery-ui-core'), null, true);
		?>
		<div class="portfolio_filters holy_church_tabs holy_church_tabs_ajax">
			<ul class="portfolio_titles holy_church_tabs_titles">
				<?php
				foreach ($holy_church_tabs as $holy_church_id=>$holy_church_title) {
					?><li><a href="<?php echo esc_url(holy_church_get_hash_link(sprintf('#%s_%s_content', $holy_church_portfolio_filters_id, $holy_church_id))); ?>" data-tab="<?php echo esc_attr($holy_church_id); ?>"><?php echo esc_html($holy_church_title); ?></a></li><?php
				}
				?>
			</ul>
			<?php
			$holy_church_ppp = holy_church_get_theme_option('posts_per_page');
			if (holy_church_is_inherit($holy_church_ppp)) $holy_church_ppp = '';
			foreach ($holy_church_tabs as $holy_church_id=>$holy_church_title) {
				$holy_church_portfolio_need_content = $holy_church_id==$holy_church_portfolio_filters_active || !$holy_church_portfolio_filters_ajax;
				?>
				<div id="<?php echo esc_attr(sprintf('%s_%s_content', $holy_church_portfolio_filters_id, $holy_church_id)); ?>"
					class="portfolio_content holy_church_tabs_content"
					data-blog-template="<?php echo esc_attr(holy_church_storage_get('blog_template')); ?>"
					data-blog-style="<?php echo esc_attr(holy_church_get_theme_option('blog_style')); ?>"
					data-posts-per-page="<?php echo esc_attr($holy_church_ppp); ?>"
					data-post-type="<?php echo esc_attr($holy_church_post_type); ?>"
					data-taxonomy="<?php echo esc_attr($holy_church_taxonomy); ?>"
					data-cat="<?php echo esc_attr($holy_church_id); ?>"
					data-parent-cat="<?php echo esc_attr($holy_church_cat); ?>"
					data-need-content="<?php echo (false===$holy_church_portfolio_need_content ? 'true' : 'false'); ?>"
				>
					<?php
					if ($holy_church_portfolio_need_content) 
						holy_church_show_portfolio_posts(array(
							'cat' => $holy_church_id,
							'parent_cat' => $holy_church_cat,
							'taxonomy' => $holy_church_taxonomy,
							'post_type' => $holy_church_post_type,
							'page' => 1,
							'sticky' => $holy_church_sticky_out
							)
						);
					?>
				</div>
				<?php
			}
			?>
		</div>
		<?php
	} else {
		holy_church_show_portfolio_posts(array(
			'cat' => $holy_church_cat,
			'parent_cat' => $holy_church_cat,
			'taxonomy' => $holy_church_taxonomy,
			'post_type' => $holy_church_post_type,
			'page' => 1,
			'sticky' => $holy_church_sticky_out
			)
		);
	}

	echo get_query_var('blog_archive_end');

} else {

	if ( is_search() )
		get_template_part( 'content', 'none-search' );
	else
		get_template_part( 'content', 'none-archive' );

}

get_footer();
?>