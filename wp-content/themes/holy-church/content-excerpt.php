<?php
/**
 * The default template to display the content
 *
 * Used for index/archive/search.
 *
 * @package WordPress
 * @subpackage HOLY_CHURCH
 * @since HOLY_CHURCH 1.0
 */

$holy_church_post_format = get_post_format();
$holy_church_post_format = empty($holy_church_post_format) ? 'standard' : str_replace('post-format-', '', $holy_church_post_format);
$holy_church_full_content = holy_church_get_theme_option('blog_content') != 'excerpt' || in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'));
$holy_church_animation = holy_church_get_theme_option('blog_animation');

?><article id="post-<?php the_ID(); ?>" 
	<?php post_class( 'post_item post_layout_excerpt post_format_'.esc_attr($holy_church_post_format) ); ?>
	<?php echo (!holy_church_is_off($holy_church_animation) ? ' data-animation="'.esc_attr(holy_church_get_animation_classes($holy_church_animation)).'"' : ''); ?>
	><?php

    // Post meta
    holy_church_show_post_meta_update(array(
            'categories' => false,
            'date' => true,
            'edit' => false,
            'seo' => false,
            'share' => false,
            'counters' => 'comments,likes',	//comments,likes,views - comma separated in any combination
            'class' => 'post_meta_date',
            'date_orig' => false
        )
    );

	// Title and post meta
	if (get_the_title() != '') {
		?>
		<div class="post_header entry-header">
			<?php
			do_action('holy_church_action_before_post_title'); 

			// Post title
			the_title( sprintf( '<h2 class="post_title entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );

			do_action('holy_church_action_before_post_meta'); 

			// Post meta
			holy_church_show_post_meta_update(array(
                'author' => true,
                'categories' => true,
				'date' => false,
				'edit' => false,
				'seo' => false,
				'share' => false,
				'counters' => ''	//comments,likes,views - comma separated in any combination
				)
			);
			?>
		</div><!-- .post_header --><?php
	}

    // Featured image
    holy_church_show_post_featured(array( 'thumb_size' => holy_church_get_thumb_size( strpos(holy_church_get_theme_option('body_style'), 'full')!==false ? 'full' : 'big' ) ));

	// Post content
	?><div class="post_content entry-content"><?php
		if ($holy_church_full_content) {
			// Post content area
			?><div class="post_content_inner"><?php
				the_content( '' );
			?></div><?php
			// Inner pages
			wp_link_pages( array(
				'before'      => '<div class="page_links"><span class="page_links_title">' . esc_html__( 'Pages:', 'holy-church' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'holy-church' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

		} else {

			$holy_church_show_learn_more = !in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'));

			// Post content area
			?><div class="post_content_inner"><?php
                if (has_excerpt()) {
                    the_excerpt();
                } else if (strpos(get_the_content('!--more'), '!--more')!==false) {
                    the_content( '' );
                } else if (in_array($holy_church_post_format, array('link', 'aside', 'status', 'quote'))) {
                    the_content();
                } else if (in_array($holy_church_post_format, array('audio', 'gallery', 'video'))) {
                    the_excerpt();
                } else if (substr(get_the_content(), 0, 1)!='[') {
                    the_excerpt();
                }
			?></div><?php

            // Post meta
            holy_church_show_post_meta_update(array(
                'author' => false,
                'categories' => false,
                'date' => false,
                'edit' => false,
                'seo' => false,
                'share' => false,
                'tags' => true,
                'counters' => '',	//comments,likes,views - comma separated in any combination
                'class' => 'post_meta_bottom'
            ));
		}
	?></div><!-- .entry-content -->
</article>