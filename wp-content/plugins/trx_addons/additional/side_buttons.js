/* global jQuery:false */
/* global TRX_ADDONS_STORAGE:false */

// Show side button
jQuery(document).ready(function () {

    "use strict";
    if (jQuery('body').hasClass('body_style_boxed')){

        jQuery('body > .side_button_wrapper.left_side_button .caption_button, body > .side_button_wrapper.right_side_button .caption_button').on('click', function(e) {
            var wrap = jQuery(this).parent('.side_button_wrapper');
            if( wrap.hasClass('opened') ){
                jQuery(this).parent('.side_button_wrapper').removeClass('opened');
                jQuery(this).siblings('.mask').removeClass('show');
            } else {
                jQuery(this).parent('.side_button_wrapper').toggleClass('opened');
                jQuery(this).siblings('.mask').toggleClass('show');
            }
            e.preventDefault();
            return false;
        });

        jQuery('body > .side_button_wrapper.left_side_button .mask, body > .side_button_wrapper.right_side_button .mask').on('click', function(e) {
            jQuery(this).parent('.side_button_wrapper').removeClass('opened');
            jQuery(this).removeClass('show');
            e.preventDefault();
            return false;
        });
    } else {

        jQuery('body > .side_button_wrapper.left_side_button, body > .side_button_wrapper.right_side_button').remove();
    }
});