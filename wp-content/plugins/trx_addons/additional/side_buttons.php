<?php
/**
 * ThemeREX Addons Additional block: Side Buttons
 *
 * @package WordPress
 * @subpackage ThemeREX Addons
 * @since v1.6.06
 */

// Don't load directly
if ( ! defined( 'TRX_ADDONS_VERSION' ) ) {
    die( '-1' );
}


// -----------------------------------------------------------------
// -- Functions Side Buttons
// -----------------------------------------------------------------


if (!function_exists('trx_addons_aside_button_left')) {
    add_action('wp_footer', 'trx_addons_aside_button_left');
    function trx_addons_aside_button_left()
    {

        $side_button_layout_left = trx_addons_get_option('side_button_layout_left');
//        $side_button_caption_left = (empty(trx_addons_get_option('side_button_caption_left')) ? esc_html__('Left / Button', 'trx_addons') : trx_addons_get_option('side_button_caption_left'));
        $side_button_caption_left = trx_addons_get_option('side_button_caption_left');
        $side_button_caption_left = (empty($side_button_caption_left) ? esc_html__('Left / Button', 'trx_addons') : $side_button_caption_left);
        $output = '';

        if (($side_button_layout_left != '') && ($side_button_layout_left != 'none')) {
            $output .= '<div class="side_button_wrapper left_side_button">'
                . '<div class="caption_button"><span class="caption_button_item">' . $side_button_caption_left . '</span></div>'
                . '<div class="layouts_wrap">' . do_shortcode('[trx_sc_layouts layout="' . esc_attr($side_button_layout_left) . '"]') . '</div>'
                . '<div class="mask"></div>'
                . '</div>';
            echo $output;
        }
    }
}


if (!function_exists('trx_addons_aside_button_right')) {
    add_action('wp_footer', 'trx_addons_aside_button_right');
    function trx_addons_aside_button_right()
    {
        $side_button_layout_right = trx_addons_get_option('side_button_layout_right');
//        $side_button_caption_right = (empty(trx_addons_get_option('side_button_caption_right')) ? esc_html__('Right / Button', 'trx_addons') : trx_addons_get_option('side_button_caption_right'));
        $side_button_caption_right = trx_addons_get_option('side_button_caption_right');
        $side_button_caption_right = (empty($side_button_caption_right) ? esc_html__('Right / Button', 'trx_addons') : $side_button_caption_right);
        $output = '';

        if (($side_button_layout_right != '') && ($side_button_layout_right != 'none')) {
            $output .= '<div class="side_button_wrapper right_side_button">'
                . '<div class="caption_button"><span class="caption_button_item">' . $side_button_caption_right . '</span></div>'
                . '<div class="layouts_wrap">' . do_shortcode('[trx_sc_layouts layout="' . esc_attr($side_button_layout_right) . '"]') . '</div>'
                . '<div class="mask"></div>'
                . '</div>';
            echo $output;
        }
    }
}

// Add 'Layouts' parameters in the ThemeREX Addons Options
if (!function_exists('trx_addons_button_aside_layouts_options')) {
    add_action( 'trx_addons_filter_options', 'trx_addons_button_aside_layouts_options');
    function trx_addons_button_aside_layouts_options($options) {

        trx_addons_array_insert_after($options, 'theme_specific_section', array(
            // Layouts settings
            'side_button_info' => array(
                "title" => esc_html__('Side Buttons', 'trx_addons'),
                "desc" => wp_kses_data( __('General settings of the side buttons. Displayed only if the body style "Boxed"', 'trx_addons') ),
                "type" => "info"
            ),
            'side_button_caption_left' => array(
                "title" => esc_html__('Caption for left button', 'trx_addons'),
                "desc" => wp_kses_data( __('Caption of the Side Button', 'trx_addons') ),
                "std" => '',
                "type" => "text"
            ),
            'side_button_layout_left' => array(
                "title" => esc_html__('Layout for left button', 'trx_addons'),
                "desc" => wp_kses_data( __('Select any previously created layout to insert to left "Side Button"', 'trx_addons') ),
                "std" => 'none',
                "options" => trx_addons_get_list_posts_custom(false, TRX_ADDONS_CPT_LAYOUTS_PT),
                "type" => "select"
            ),
            'side_button_caption_right' => array(
                "title" => esc_html__('Caption for right button', 'trx_addons'),
                "desc" => wp_kses_data( __('Caption of the Side Button', 'trx_addons') ),
                "std" => '',
                "type" => "text"
            ),
            'side_button_layout_right' => array(
                "title" => esc_html__('Layout for right button', 'trx_addons'),
                "desc" => wp_kses_data( __('Select any previously created layout to insert to right "Side Button"', 'trx_addons') ),
                "std" => 'none',
                "options" => trx_addons_get_list_posts_custom(false, TRX_ADDONS_CPT_LAYOUTS_PT),
                "type" => "select"
            ),
        ));
        return $options;
    }
}


// Return list post items from any post type and taxonomy
if ( !function_exists( 'trx_addons_get_list_posts_custom' ) ) {
    function trx_addons_get_list_posts_custom($prepend_inherit=false, $opt=array()) {
        static $list = array();
        $opt = array_merge(array(
            'post_type'			=> 'post',
            'post_status'		=> 'publish',
            'post_parent'		=> '',
            'taxonomy'			=> 'category',
            'taxonomy_value'	=> '',
            'meta_key'			=> '',
            'meta_value'		=> 'custom',
            'meta_compare'		=> '',
            'posts_per_page'	=> -1,
            'orderby'			=> 'post_date',
            'order'				=> 'desc',
            'not_selected'		=> true,
            'return'			=> 'id'
        ), is_array($opt) ? $opt : array('post_type'=>$opt));

        $hash = 'list_posts'
            . '_' . (is_array($opt['post_type']) ? join('_', $opt['post_type']) : $opt['post_type'])
            . '_' . (is_array($opt['post_parent']) ? join('_', $opt['post_parent']) : $opt['post_parent'])
            . '_' . ($opt['taxonomy'])
            . '_' . (is_array($opt['taxonomy_value']) ? join('_', $opt['taxonomy_value']) : $opt['taxonomy_value'])
            . '_' . ($opt['meta_key'])
            . '_' . ($opt['meta_compare'])
            . '_' . ($opt['meta_value'])
            . '_' . ($opt['orderby'])
            . '_' . ($opt['order'])
            . '_' . ($opt['return'])
            . '_' . ($opt['posts_per_page']);
        if (!isset($list[$hash])) {
            $list[$hash] = array();
            if ($opt['not_selected']!==false) $list[$hash]['none'] = $opt['not_selected']===true
                ? esc_html__("- Not selected -", 'trx_addons')
                : $opt['not_selected'];
            $args = array(
                'post_type' => $opt['post_type'],
                'post_status' => $opt['post_status'],
                'posts_per_page' => $opt['posts_per_page'],
                'ignore_sticky_posts' => true,
                'orderby'	=> $opt['orderby'],
                'order'		=> $opt['order']
            );
            if (!empty($opt['post_parent'])) {
                if (is_array($opt['post_parent']))
                    $args['post_parent__in'] = $opt['post_parent'];
                else
                    $args['post_parent'] = $opt['post_parent'];
            }
            if (!empty($opt['taxonomy_value'])) {
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => $opt['taxonomy'],
                        'field' => is_array($opt['taxonomy_value'])
                            ? ((int) $opt['taxonomy_value'][0] > 0  ? 'term_taxonomy_id' : 'slug')
                            : ((int) $opt['taxonomy_value'] > 0  ? 'term_taxonomy_id' : 'slug'),
                        'terms' => is_array($opt['taxonomy_value'])
                            ? $opt['taxonomy_value']
                            : ((int) $opt['taxonomy_value'] > 0 ? (int) $opt['taxonomy_value'] : $opt['taxonomy_value'] )
                    )
                );
            }
            if (!empty($opt['meta_key'])) {
                $args['meta_key'] = $opt['meta_key'];
            }
            if (!empty($opt['meta_value'])) {
                $args['meta_value'] = $opt['meta_value'];
            }
            if (!empty($opt['meta_compare'])) {
                $args['meta_compare'] = $opt['meta_compare'];
            }
            $posts = get_posts( $args );
            if (is_array($posts) && count($posts) > 0) {
                foreach ($posts as $post) {
                    $list[$hash][$opt['return']=='id' ? $post->ID : $post->post_title] = $post->post_title;
                }
            }
        }
        return $prepend_inherit ? trx_addons_array_merge(array('inherit' => esc_html__("Inherit", 'trx_addons')), $list[$hash]) : $list[$hash];
    }
}


// Load required styles and scripts for the frontend
if ( !function_exists( 'trx_addons_side_button_load_scripts_front' ) ) {
    add_action("wp_enqueue_scripts", 'trx_addons_side_button_load_scripts_front');
    function trx_addons_side_button_load_scripts_front() {
        wp_enqueue_style( 'trx_addons-side_button', trx_addons_get_file_url('additional/side_buttons.css'), array(), null );
        wp_enqueue_script( 'trx_addons-side_button', trx_addons_get_file_url('additional/side_buttons.js'), array(), null );
    }
}
?>